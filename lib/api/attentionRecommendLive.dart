import 'package:flutter_zb/utils/http.dart';
import '../utils/utils.dart';
import 'entitys/attentionRecommendLive.dart';

// 關注 推薦直播
class AttentionRecommendLive {
  static Future<AttentionRecommendLiveResponseData> getRecommandLive() async {
    var response = await HttpUtil().get('/account_room/list');
    return AttentionRecommendLiveResponseData.fromJson(response);
  }
}
