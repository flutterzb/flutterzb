import 'package:flutter_zb/api/entitys/banner.dart';
import 'package:flutter_zb/utils/http.dart';
import '../utils/utils.dart';

// Banner
class AdBanner {
  static Future<GetBannerResponseData> getBanner() async {
    var response = await HttpUtil().get('/getBanner/?index=appindex');
    return GetBannerResponseData.fromJson(response);
  }
}
