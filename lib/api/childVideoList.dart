import 'package:flutter_zb/utils/http.dart';
import 'package:intl/intl.dart';
import '../utils/utils.dart';
import 'entitys/childVideoList.dart';

// ChildVideoList
class ChildVideoList {
  static Future<ChildVideoListResponseData> getChildVideoList(
      {String limit = "99",
      String page,
      String liveTypeID,
      String accountID = ""}) async {
    final now = DateTime.now();
    final DateFormat formatter = DateFormat('yyyy-MM-dd HH:mm:ss');
    final String startAt = formatter.format(now);
    //print(startAt);
    //print('/api/video/?start_at=$startAt&live_type_id=$liveTypeID&page=$page');
    //account_id
    final startAtURL = Uri.encodeComponent(startAt);
    //print(
    //    '/api/video/?limit=$limit&start_at=$startAtURL&live_type_id=$liveTypeID&page=$page&account_id=$accountID');
    var response = await HttpUtil().get(
        '/api/video?limit=$limit&start_at=$startAt&live_type_id=$liveTypeID&page=$page&account_id=$accountID');
    return ChildVideoListResponseData.fromJson(response);
  }
}
