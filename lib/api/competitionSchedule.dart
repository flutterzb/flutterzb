import 'package:flutter_zb/utils/http.dart';
import 'entitys/competitionSchedule.dart';

///賽程表
class CompetitionSchedule {
  static Future<CompetitionScheduleResponesData> getListWithGame(
      {dynamic parentID = "1", dynamic childID = "all"}) async {
    var response = await HttpUtil().get(
        '/program/listwithgame/?parent=$parentID&child=$childID&day_length=7&now=Y');
    return CompetitionScheduleResponesData.fromJson(response);
  }
}
