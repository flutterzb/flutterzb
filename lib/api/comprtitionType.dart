import 'package:flutter_zb/api/entitys/competitionType.dart';
import 'package:flutter_zb/utils/http.dart';

//賽程表上面的分類
class CompetitionType {
  static Future<CompetitionTypeResponseData> getCompetitionType() async {
    var response = await HttpUtil().get('/program/livetype/list');
    return CompetitionTypeResponseData.fromJson(response);
  }
}
