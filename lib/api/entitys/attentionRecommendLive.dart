// To parse this JSON data, do
//
//     final attentionRecommendLiveResponseData = attentionRecommendLiveResponseDataFromJson(jsonString);

import 'dart:convert';

AttentionRecommendLiveResponseData attentionRecommendLiveResponseDataFromJson(
        String str) =>
    AttentionRecommendLiveResponseData.fromJson(json.decode(str));

String attentionRecommendLiveResponseDataToJson(
        AttentionRecommendLiveResponseData data) =>
    json.encode(data.toJson());

class AttentionRecommendLiveResponseData {
  AttentionRecommendLiveResponseData({
    this.webStatus,
    this.code,
    this.message,
    this.data,
    this.result,
    this.paginate,
  });

  int webStatus;
  int code;
  String message;
  dynamic data;
  List<Result> result;
  dynamic paginate;

  factory AttentionRecommendLiveResponseData.fromJson(
          Map<String, dynamic> json) =>
      AttentionRecommendLiveResponseData(
        webStatus: json["web_status"],
        code: json["code"],
        message: json["message"],
        data: json["data"],
        result:
            List<Result>.from(json["result"].map((x) => Result.fromJson(x))),
        paginate: json["paginate"],
      );

  Map<String, dynamic> toJson() => {
        "web_status": webStatus,
        "code": code,
        "message": message,
        "data": data,
        "result": List<dynamic>.from(result.map((x) => x.toJson())),
        "paginate": paginate,
      };
}

class Result {
  Result({
    this.id,
    this.nickname,
    this.icon,
    this.roomNum,
    this.title,
    this.gameName,
    this.resultLiveType,
    this.type,
    this.platform,
    this.liveTypeParent,
    this.liveStatus,
    this.hot,
    this.weight,
    this.liveType,
    this.liveTypesName,
    this.channelNum,
    this.focusTotal,
    this.visitCount,
    this.iframeId,
    this.coverUrl,
    this.iframeUrl,
    this.accountTitle,
    this.kind,
    this.imageUrl,
    this.isFocus,
  });

  int id;
  String nickname;
  String icon;
  String roomNum;
  String title;
  String gameName;
  int resultLiveType;
  int type;
  String platform;
  int liveTypeParent;
  int liveStatus;
  int hot;
  int weight;
  String liveType;
  String liveTypesName;
  String channelNum;
  int focusTotal;
  int visitCount;
  dynamic iframeId;
  dynamic coverUrl;
  dynamic iframeUrl;
  String accountTitle;
  String kind;
  String imageUrl;
  bool isFocus;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"],
        nickname: json["nickname"],
        icon: json["icon"],
        roomNum: json["room_num"],
        title: json["title"],
        gameName: json["game_name"],
        resultLiveType: json["live_type"],
        type: json["type"],
        platform: json["platform"],
        liveTypeParent:
            json["live_type_parent"] == null ? null : json["live_type_parent"],
        liveStatus: json["live_status"],
        hot: json["hot"],
        weight: json["weight"],
        liveType: json["liveType"],
        liveTypesName: json["live_types_name"],
        channelNum: json["channel_num"] == null ? null : json["channel_num"],
        focusTotal: json["focus_total"],
        visitCount: json["visitCount"],
        iframeId: json["iframe_id"],
        coverUrl: json["cover_url"],
        iframeUrl: json["iframe_url"],
        accountTitle: json["accountTitle"],
        kind: json["kind"],
        imageUrl: json["image_url"],
        isFocus: json["isFocus"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "nickname": nickname,
        "icon": icon,
        "room_num": roomNum,
        "title": title,
        "game_name": gameName,
        "live_type": resultLiveType,
        "type": type,
        "platform": platform,
        "live_type_parent": liveTypeParent == null ? null : liveTypeParent,
        "live_status": liveStatus,
        "hot": hot,
        "weight": weight,
        "liveType": liveType,
        "live_types_name": liveTypesName,
        "channel_num": channelNum == null ? null : channelNum,
        "focus_total": focusTotal,
        "visitCount": visitCount,
        "iframe_id": iframeId,
        "cover_url": coverUrl,
        "iframe_url": iframeUrl,
        "accountTitle": accountTitle,
        "kind": kind,
        "image_url": imageUrl,
        "isFocus": isFocus,
      };
}
