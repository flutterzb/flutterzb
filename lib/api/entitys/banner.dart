// To parse this JSON data, do
//
//     final getBannerResponseData = getBannerResponseDataFromJson(jsonString);

import 'dart:convert';

GetBannerResponseData getBannerResponseDataFromJson(String str) =>
    GetBannerResponseData.fromJson(json.decode(str));

String getBannerResponseDataToJson(GetBannerResponseData data) =>
    json.encode(data.toJson());

class GetBannerResponseData {
  GetBannerResponseData({
    this.webStatus,
    this.code,
    this.message,
    this.data,
    this.result,
    this.paginate,
  });

  int webStatus;
  int code;
  String message;
  dynamic data;
  List<Result> result;
  dynamic paginate;

  factory GetBannerResponseData.fromJson(Map<String, dynamic> json) =>
      GetBannerResponseData(
        webStatus: json["web_status"],
        code: json["code"],
        message: json["message"],
        data: json["data"],
        result:
            List<Result>.from(json["result"].map((x) => Result.fromJson(x))),
        paginate: json["paginate"],
      );

  Map<String, dynamic> toJson() => {
        "web_status": webStatus,
        "code": code,
        "message": message,
        "data": data,
        "result": List<dynamic>.from(result.map((x) => x.toJson())),
        "paginate": paginate,
      };
}

class Result {
  Result({
    this.id,
    this.banner,
    this.mBanner,
    this.position,
    this.url,
    this.od,
    this.status,
  });

  int id;
  String banner;
  String mBanner;
  int position;
  String url;
  int od;
  int status;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"],
        banner: json["banner"],
        mBanner: json["m_banner"],
        position: json["position"],
        url: json["url"],
        od: json["od"],
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "banner": banner,
        "m_banner": mBanner,
        "position": position,
        "url": url,
        "od": od,
        "status": status,
      };
}
