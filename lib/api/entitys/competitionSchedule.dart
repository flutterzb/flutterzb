// // To parse this JSON data, do
// //
// //     final competitionScheduleResponesData = competitionScheduleResponesDataFromJson(jsonString);

import 'dart:convert';

CompetitionScheduleResponesData competitionScheduleResponesDataFromJson(
        String str) =>
    CompetitionScheduleResponesData.fromJson(json.decode(str));

String competitionScheduleResponesDataToJson(
        CompetitionScheduleResponesData data) =>
    json.encode(data.toJson());

class CompetitionScheduleResponesData {
  CompetitionScheduleResponesData({
    this.webStatus,
    this.code,
    this.message,
    this.data,
    this.result,
    this.paginate,
  });

  int webStatus;
  int code;
  String message;
  dynamic data;
  List<Result> result;
  dynamic paginate;

  factory CompetitionScheduleResponesData.fromJson(Map<String, dynamic> json) =>
      CompetitionScheduleResponesData(
        webStatus: json["web_status"],
        code: json["code"],
        message: json["message"],
        data: json["data"],
        result:
            List<Result>.from(json["result"].map((x) => Result.fromJson(x))),
        paginate: json["paginate"],
      );

  Map<String, dynamic> toJson() => {
        "web_status": webStatus,
        "code": code,
        "message": message,
        "data": data,
        "result": List<dynamic>.from(result.map((x) => x.toJson())),
        "paginate": paginate,
      };
}

class Result {
  Result({
    this.date,
    this.data,
  });

  DateTime date;
  List<CompeitionDataByDay> data;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        date: DateTime.parse(json["date"]),
        data: List<CompeitionDataByDay>.from(
            json["data"].map((x) => CompeitionDataByDay.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "date":
            "${date.year.toString().padLeft(4, '0')}-${date.month.toString().padLeft(2, '0')}-${date.day.toString().padLeft(2, '0')}",
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class CompeitionDataByDay {
  CompeitionDataByDay({
    this.solarApi,
    this.id,
    this.mid,
    this.lid,
    this.aisportId,
    this.lname,
    this.lnameEn,
    this.hid,
    this.hname,
    this.hnameEn,
    this.aid,
    this.aname,
    this.anameEn,
    this.hicon,
    this.aicon,
    this.time,
    this.endTime,
    this.status,
    this.sport,
    this.hot,
    this.tag,
    this.liveTypesChildId,
    this.videoId,
    this.action,
    this.schId,
    this.liveTypesChildName,
    this.liveTypesName,
    this.sportName,
    this.sportIcon,
    this.coverImgUrl,
    this.coverImgUrlSecond,
    this.schType,
    this.hotTag,
    this.matchDetail,
    this.account,
    this.matchStatus,
  });

  SolarApi solarApi;
  int id;
  int mid;
  int lid;
  String aisportId;
  String lname;
  String lnameEn;
  String hid;
  String hname;
  String hnameEn;
  String aid;
  String aname;
  String anameEn;
  String hicon;
  String aicon;
  DateTime time;
  DateTime endTime;
  int status;
  int sport;
  int hot;
  String tag;
  int liveTypesChildId;
  dynamic videoId;
  int action;
  int schId;
  dynamic liveTypesChildName;
  dynamic liveTypesName;
  String sportName;
  String sportIcon;
  String coverImgUrl;
  String coverImgUrlSecond;
  int schType;
  int hotTag;
  String matchDetail;
  List<Account> account;
  String matchStatus;

  factory CompeitionDataByDay.fromJson(Map<String, dynamic> json) =>
      CompeitionDataByDay(
        solarApi: json["solarApi"] == null
            ? null
            : SolarApi.fromJson(json["solarApi"]),
        id: json["id"],
        mid: json["mid"],
        lid: json["lid"],
        aisportId: json["aisportId"] == null ? null : json["aisportId"],
        lname: json["lname"],
        lnameEn: json["lnameEN"] == null ? null : json["lnameEN"],
        hid: json["hid"] == null ? null : json["hid"],
        hname: json["hname"],
        hnameEn: json["hnameEN"] == null ? null : json["hnameEN"],
        aid: json["aid"] == null ? null : json["aid"],
        aname: json["aname"],
        anameEn: json["anameEN"] == null ? null : json["anameEN"],
        hicon: json["hicon"],
        aicon: json["aicon"],
        time: DateTime.parse(json["time"]),
        endTime: DateTime.parse(json["end_time"]),
        status: json["status"],
        sport: json["sport"],
        hot: json["hot"],
        tag: json["tag"],
        liveTypesChildId: json["live_types_child_id"],
        videoId: json["video_id"],
        action: json["action"],
        schId: json["sch_id"],
        liveTypesChildName: json["live_types_child_name"],
        liveTypesName: json["live_types_name"],
        sportName: json["sportName"],
        sportIcon: json["sport_icon"],
        coverImgUrl: json["cover_img_url"],
        coverImgUrlSecond: json["cover_img_url_second"],
        schType: json["sch_type"],
        hotTag: json["hot_tag"],
        matchDetail: json["match_detail"],
        account:
            List<Account>.from(json["account"].map((x) => Account.fromJson(x))),
        matchStatus: json["matchStatus"],
      );

  Map<String, dynamic> toJson() => {
        "solarApi": solarApi == null ? null : solarApi.toJson(),
        "id": id,
        "mid": mid,
        "lid": lid,
        "aisportId": aisportId == null ? null : aisportId,
        "lname": lname,
        "lnameEN": lnameEn == null ? null : lnameEn,
        "hid": hid == null ? null : hid,
        "hname": hname,
        "hnameEN": hnameEn == null ? null : hnameEn,
        "aid": aid == null ? null : aid,
        "aname": aname,
        "anameEN": anameEn == null ? null : anameEn,
        "hicon": hicon,
        "aicon": aicon,
        "time": time.toIso8601String(),
        "end_time": endTime.toIso8601String(),
        "status": status,
        "sport": sport,
        "hot": hot,
        "tag": tag,
        "live_types_child_id": liveTypesChildId,
        "video_id": videoId,
        "action": action,
        "sch_id": schId,
        "live_types_child_name": liveTypesChildName,
        "live_types_name": liveTypesName,
        "sportName": sportName,
        "sport_icon": sportIcon,
        "cover_img_url": coverImgUrl,
        "cover_img_url_second": coverImgUrlSecond,
        "sch_type": schType,
        "hot_tag": hotTag,
        "match_detail": matchDetail,
        "account": List<dynamic>.from(account.map((x) => x.toJson())),
        "matchStatus": matchStatus,
      };
}

class Account {
  Account({
    this.msrId,
    this.schId,
    this.tags,
    this.streamUrl,
    this.name,
    this.type,
    this.iframeUrl,
    this.iconUrl,
    this.coverUrl,
    this.streamStatus,
    this.scheduleRoomTag,
    this.accountId,
    this.nickname,
    this.icon,
    this.info,
    this.roomNum,
    this.liveStatus,
    this.anchorTags,
    this.uuid,
    this.url,
    this.id,
    this.colorType,
  });

  int msrId;
  int schId;
  String tags;
  String streamUrl;
  String name;
  String type;
  String iframeUrl;
  dynamic iconUrl;
  String coverUrl;
  int streamStatus;
  List<dynamic> scheduleRoomTag;
  String accountId;
  String nickname;
  String icon;
  String info;
  String roomNum;
  int liveStatus;
  List<dynamic> anchorTags;
  String uuid;
  String url;
  String id;
  int colorType;

  factory Account.fromJson(Map<String, dynamic> json) => Account(
        msrId: json["msr_id"],
        schId: json["sch_id"],
        tags: json["tags"],
        streamUrl: json["StreamUrl"],
        name: json["name"],
        type: json["type"],
        iframeUrl: json["iframe_url"],
        iconUrl: json["icon_url"],
        coverUrl: json["cover_url"],
        streamStatus: json["stream_status"],
        scheduleRoomTag:
            List<dynamic>.from(json["scheduleRoomTag"].map((x) => x)),
        accountId: json["account_id"],
        nickname: json["nickname"],
        icon: json["icon"],
        info: json["info"] == null ? null : json["info"],
        roomNum: json["room_num"],
        liveStatus: json["live_status"],
        anchorTags: List<dynamic>.from(json["anchorTags"].map((x) => x)),
        uuid: json["uuid"],
        url: json["url"],
        id: json["id"],
        colorType: json["color_type"],
      );

  Map<String, dynamic> toJson() => {
        "msr_id": msrId,
        "sch_id": schId,
        "tags": tags,
        "StreamUrl": streamUrl,
        "name": name,
        "type": type,
        "iframe_url": iframeUrl,
        "icon_url": iconUrl,
        "cover_url": coverUrl,
        "stream_status": streamStatus,
        "scheduleRoomTag": List<dynamic>.from(scheduleRoomTag.map((x) => x)),
        "account_id": accountId,
        "nickname": nickname,
        "icon": icon,
        "info": info == null ? null : info,
        "room_num": roomNum,
        "live_status": liveStatus,
        "anchorTags": List<dynamic>.from(anchorTags.map((x) => x)),
        "uuid": uuid,
        "url": url,
        "id": id,
        "color_type": colorType,
      };
}

class SolarApi {
  SolarApi({
    this.hTotalScore,
    this.aTotalScore,
    this.timeInfo,
    this.situation,
    //this.extradata,
    this.hid,
    this.aid,
    this.hTeamMatchesUrl,
    this.aTeamMatchesUrl,
  });

  dynamic hTotalScore;
  dynamic aTotalScore;
  String timeInfo;
  List<Situation> situation;
  //Extradata extradata;
  String hid;
  String aid;
  String hTeamMatchesUrl;
  String aTeamMatchesUrl;

  factory SolarApi.fromJson(Map<String, dynamic> json) => SolarApi(
        hTotalScore: json["hTotalScore"] == null ? 0 : json["hTotalScore"],
        aTotalScore: json["aTotalScore"] == null ? 0 : json["aTotalScore"],
        timeInfo: json["time_info"],
        situation: List<Situation>.from(
            json["situation"].map((x) => situationValues.map[x])),
        // extradata: json["extradata"] == null
        //     ? null
        //     : Extradata.fromJson(json["extradata"]),
        hid: json["hid"] == null ? null : json["hid"],
        aid: json["aid"] == null ? null : json["aid"],
        hTeamMatchesUrl:
            json["hTeamMatchesUrl"] == null ? null : json["hTeamMatchesUrl"],
        aTeamMatchesUrl:
            json["aTeamMatchesUrl"] == null ? null : json["aTeamMatchesUrl"],
      );

  Map<String, dynamic> toJson() => {
        "hTotalScore": hTotalScore,
        "aTotalScore": aTotalScore,
        "time_info": timeInfo,
        "situation": List<dynamic>.from(
            situation.map((x) => situationValues.reverse[x])),
        // "extradata": extradata == null ? null : extradata.toJson(),
        "hid": hid == null ? null : hid,
        "aid": aid == null ? null : aid,
        "hTeamMatchesUrl": hTeamMatchesUrl == null ? null : hTeamMatchesUrl,
        "aTeamMatchesUrl": aTeamMatchesUrl == null ? null : aTeamMatchesUrl,
      };
}

class Extradata {
  Extradata({
    this.the1,
    this.the2,
    this.the3,
    this.the4,
    this.the5,
    this.the6,
    this.the7,
    this.the8,
    this.the13,
    this.the15,
    this.the16,
    this.the17,
    this.the19,
    this.the21,
    this.the22,
    this.the23,
    this.the24,
    this.the25,
    this.fats,
    this.fpks,
    this.round,
    this.homePosition,
    this.awayPosition,
    this.groupNum,
    this.hPosition,
    this.aPosition,
    this.bhs,
    this.bas,
  });

  List<dynamic> the1;
  List<dynamic> the2;
  List<dynamic> the3;
  List<dynamic> the4;
  List<dynamic> the5;
  List<dynamic> the6;
  List<dynamic> the7;
  List<dynamic> the8;
  List<dynamic> the13;
  List<dynamic> the15;
  List<dynamic> the16;
  List<dynamic> the17;
  List<dynamic> the19;
  List<dynamic> the21;
  List<dynamic> the22;
  List<dynamic> the23;
  List<dynamic> the24;
  List<dynamic> the25;
  dynamic fats;
  dynamic fpks;
  String round;
  String homePosition;
  String awayPosition;
  String groupNum;
  String hPosition;
  String aPosition;
  dynamic bhs;
  dynamic bas;

  factory Extradata.fromJson(Map<String, dynamic> json) => Extradata(
        the1: List<dynamic>.from(json["1"].map((x) => x)),
        the2: List<dynamic>.from(json["2"].map((x) => x)),
        the3: List<dynamic>.from(json["3"].map((x) => x)),
        the4: List<dynamic>.from(json["4"].map((x) => x)),
        the5: List<dynamic>.from(json["5"].map((x) => x)),
        the6: List<dynamic>.from(json["6"].map((x) => x)),
        the7: json["7"] == null
            ? null
            : List<dynamic>.from(json["7"].map((x) => x)),
        the8: json["8"] == null
            ? null
            : List<dynamic>.from(json["8"].map((x) => x)),
        the13: json["13"] == null
            ? null
            : List<dynamic>.from(json["13"].map((x) => x)),
        the15: json["15"] == null
            ? null
            : List<dynamic>.from(json["15"].map((x) => x)),
        the16: json["16"] == null
            ? null
            : List<dynamic>.from(json["16"].map((x) => x)),
        the17: json["17"] == null
            ? null
            : List<dynamic>.from(json["17"].map((x) => x)),
        the19: json["19"] == null
            ? null
            : List<dynamic>.from(json["19"].map((x) => x)),
        the21: json["21"] == null
            ? null
            : List<dynamic>.from(json["21"].map((x) => x)),
        the22: json["22"] == null
            ? null
            : List<dynamic>.from(json["22"].map((x) => x)),
        the23: json["23"] == null
            ? null
            : List<dynamic>.from(json["23"].map((x) => x)),
        the24: json["24"] == null
            ? null
            : List<dynamic>.from(json["24"].map((x) => x)),
        the25: json["25"] == null
            ? null
            : List<dynamic>.from(json["25"].map((x) => x)),
        fats: json["fats"],
        fpks: json["fpks"],
        round: json["round"] == null ? null : json["round"],
        homePosition:
            json["home_position"] == null ? null : json["home_position"],
        awayPosition:
            json["away_position"] == null ? null : json["away_position"],
        groupNum: json["group_num"] == null ? null : json["group_num"],
        hPosition: json["hPosition"] == null ? null : json["hPosition"],
        aPosition: json["aPosition"] == null ? null : json["aPosition"],
        bhs: json["bhs"] == null ? null : json["bhs"],
        bas: json["bas"] == null ? null : json["bas"],
      );

  Map<String, dynamic> toJson() => {
        "1": List<dynamic>.from(the1.map((x) => x)),
        "2": List<dynamic>.from(the2.map((x) => x)),
        "3": List<dynamic>.from(the3.map((x) => x)),
        "4": List<dynamic>.from(the4.map((x) => x)),
        "5": List<dynamic>.from(the5.map((x) => x)),
        "6": List<dynamic>.from(the6.map((x) => x)),
        "7": the7 == null ? null : List<dynamic>.from(the7.map((x) => x)),
        "8": the8 == null ? null : List<dynamic>.from(the8.map((x) => x)),
        "13": the13 == null ? null : List<dynamic>.from(the13.map((x) => x)),
        "15": the15 == null ? null : List<dynamic>.from(the15.map((x) => x)),
        "16": the16 == null ? null : List<dynamic>.from(the16.map((x) => x)),
        "17": the17 == null ? null : List<dynamic>.from(the17.map((x) => x)),
        "19": the19 == null ? null : List<dynamic>.from(the19.map((x) => x)),
        "21": the21 == null ? null : List<dynamic>.from(the21.map((x) => x)),
        "22": the22 == null ? null : List<dynamic>.from(the22.map((x) => x)),
        "23": the23 == null ? null : List<dynamic>.from(the23.map((x) => x)),
        "24": the24 == null ? null : List<dynamic>.from(the24.map((x) => x)),
        "25": the25 == null ? null : List<dynamic>.from(the25.map((x) => x)),
        "fats": fats,
        "fpks": fpks,
        "round": round == null ? null : round,
        "home_position": homePosition == null ? null : homePosition,
        "away_position": awayPosition == null ? null : awayPosition,
        "group_num": groupNum == null ? null : groupNum,
        "hPosition": hPosition == null ? null : hPosition,
        "aPosition": aPosition == null ? null : aPosition,
        "bhs": bhs == null ? null : bhs,
        "bas": bas == null ? null : bas,
      };
}

enum Situation { THE_00, SITUATION_00, THE_0, SITUATION_0 }

final situationValues = EnumValues({
  "总分 0": Situation.SITUATION_0,
  "角球 0-0": Situation.SITUATION_00,
  "分差 0": Situation.THE_0,
  "半场 0-0": Situation.THE_00
});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
