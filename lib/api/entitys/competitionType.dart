// To parse this JSON data, do
//
//     final competitionTypeResponseData = competitionTypeResponseDataFromJson(jsonString);

import 'dart:convert';

CompetitionTypeResponseData competitionTypeResponseDataFromJson(String str) =>
    CompetitionTypeResponseData.fromJson(json.decode(str));

String competitionTypeResponseDataToJson(CompetitionTypeResponseData data) =>
    json.encode(data.toJson());

class CompetitionTypeResponseData {
  CompetitionTypeResponseData({
    this.webStatus,
    this.code,
    this.message,
    this.data,
    this.result,
    this.paginate,
  });

  int webStatus;
  int code;
  String message;
  Data data;
  bool result;
  dynamic paginate;

  factory CompetitionTypeResponseData.fromJson(Map<String, dynamic> json) =>
      CompetitionTypeResponseData(
        webStatus: json["web_status"],
        code: json["code"],
        message: json["message"],
        data: Data.fromJson(json["data"]),
        result: json["result"],
        paginate: json["paginate"],
      );

  Map<String, dynamic> toJson() => {
        "web_status": webStatus,
        "code": code,
        "message": message,
        "data": data.toJson(),
        "result": result,
        "paginate": paginate,
      };
}

class Data {
  Data({
    this.types,
  });

  List<Type> types;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        types: List<Type>.from(json["types"].map((x) => Type.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "types": List<dynamic>.from(types.map((x) => x.toJson())),
      };
}

class Type {
  Type({
    this.id,
    this.name,
    this.childTypes,
    this.nameEn,
    this.remark,
    this.status,
    this.homeStatus,
    this.parent,
    this.od,
    this.coverImgUrl,
    this.coverImgUrlSecond,
    this.filterSportId,
    this.filterLid,
    this.createdAt,
    this.updatedAt,
    this.url,
  });

  dynamic id;
  String name;
  List<Type> childTypes;
  dynamic nameEn;
  dynamic remark;
  int status;
  int homeStatus;
  int parent;
  int od;
  String coverImgUrl;
  String coverImgUrlSecond;
  String filterSportId;
  String filterLid;
  DateTime createdAt;
  DateTime updatedAt;
  dynamic url;

  factory Type.fromJson(Map<String, dynamic> json) => Type(
        id: json["id"],
        name: json["name"],
        childTypes: json["childTypes"] == null
            ? null
            : List<Type>.from(json["childTypes"].map((x) => Type.fromJson(x))),
        nameEn: json["name_en"],
        remark: json["remark"],
        status: json["status"] == null ? null : json["status"],
        homeStatus: json["home_status"] == null ? null : json["home_status"],
        parent: json["parent"] == null ? null : json["parent"],
        od: json["od"] == null ? null : json["od"],
        coverImgUrl:
            json["cover_img_url"] == null ? null : json["cover_img_url"],
        coverImgUrlSecond: json["cover_img_url_second"] == null
            ? null
            : json["cover_img_url_second"],
        filterSportId:
            json["filter_sport_id"] == null ? null : json["filter_sport_id"],
        filterLid: json["filter_lid"] == null ? null : json["filter_lid"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        url: json["url"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "childTypes": childTypes == null
            ? null
            : List<dynamic>.from(childTypes.map((x) => x.toJson())),
        "name_en": nameEn,
        "remark": remark,
        "status": status == null ? null : status,
        "home_status": homeStatus == null ? null : homeStatus,
        "parent": parent == null ? null : parent,
        "od": od == null ? null : od,
        "cover_img_url": coverImgUrl == null ? null : coverImgUrl,
        "cover_img_url_second":
            coverImgUrlSecond == null ? null : coverImgUrlSecond,
        "filter_sport_id": filterSportId == null ? null : filterSportId,
        "filter_lid": filterLid == null ? null : filterLid,
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
        "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
        "url": url,
      };
}
