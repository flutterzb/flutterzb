// To parse this JSON data, do
//
//     final firstPageMenuResponseData = firstPageMenuResponseDataFromJson(jsonString);

import 'dart:convert';

FirstPageMenuResponseData firstPageMenuResponseDataFromJson(String str) =>
    FirstPageMenuResponseData.fromJson(json.decode(str));

String firstPageMenuResponseDataToJson(FirstPageMenuResponseData data) =>
    json.encode(data.toJson());

class FirstPageMenuResponseData {
  FirstPageMenuResponseData({
    this.webStatus,
    this.code,
    this.message,
    this.data,
    this.result,
    this.paginate,
  });

  int webStatus;
  int code;
  String message;
  dynamic data;
  List<Result> result;
  dynamic paginate;

  factory FirstPageMenuResponseData.fromJson(Map<String, dynamic> json) =>
      FirstPageMenuResponseData(
        webStatus: json["web_status"],
        code: json["code"],
        message: json["message"],
        data: json["data"],
        result:
            List<Result>.from(json["result"].map((x) => Result.fromJson(x))),
        paginate: json["paginate"],
      );

  Map<String, dynamic> toJson() => {
        "web_status": webStatus,
        "code": code,
        "message": message,
        "data": data,
        "result": List<dynamic>.from(result.map((x) => x.toJson())),
        "paginate": paginate,
      };
}

class Result {
  Result({
    this.id,
    this.name,
    this.nameEn,
    this.children,
  });

  int id;
  String name;
  dynamic nameEn;
  List<MenuChild> children;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"],
        name: json["name"],
        nameEn: json["name_en"],
        children: List<MenuChild>.from(
            json["children"].map((x) => MenuChild.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "name_en": nameEn,
        "children": List<dynamic>.from(children.map((x) => x.toJson())),
      };
}

class MenuChild {
  MenuChild({
    this.id,
    this.name,
    this.nameEn,
    this.remark,
    this.status,
    this.homeStatus,
    this.parent,
    this.od,
    this.coverImgUrl,
    this.coverImgUrlSecond,
    this.filterSportId,
    this.filterLid,
    this.createdAt,
    this.updatedAt,
    this.url,
  });

  dynamic id;
  String name;
  dynamic nameEn;
  dynamic remark;
  int status;
  int homeStatus;
  int parent;
  int od;
  String coverImgUrl;
  String coverImgUrlSecond;
  String filterSportId;
  String filterLid;
  DateTime createdAt;
  DateTime updatedAt;
  dynamic url;

  factory MenuChild.fromJson(Map<String, dynamic> json) => MenuChild(
        id: json["id"],
        name: json["name"],
        nameEn: json["name_en"],
        remark: json["remark"],
        status: json["status"] == null ? null : json["status"],
        homeStatus: json["home_status"] == null ? null : json["home_status"],
        parent: json["parent"] == null ? null : json["parent"],
        od: json["od"] == null ? null : json["od"],
        coverImgUrl:
            json["cover_img_url"] == null ? null : json["cover_img_url"],
        coverImgUrlSecond: json["cover_img_url_second"] == null
            ? null
            : json["cover_img_url_second"],
        filterSportId:
            json["filter_sport_id"] == null ? null : json["filter_sport_id"],
        filterLid: json["filter_lid"] == null ? null : json["filter_lid"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        url: json["url"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "name_en": nameEn,
        "remark": remark,
        "status": status == null ? null : status,
        "home_status": homeStatus == null ? null : homeStatus,
        "parent": parent == null ? null : parent,
        "od": od == null ? null : od,
        "cover_img_url": coverImgUrl == null ? null : coverImgUrl,
        "cover_img_url_second":
            coverImgUrlSecond == null ? null : coverImgUrlSecond,
        "filter_sport_id": filterSportId == null ? null : filterSportId,
        "filter_lid": filterLid == null ? null : filterLid,
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
        "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
        "url": url,
      };
}
