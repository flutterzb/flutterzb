// To parse this JSON data, do
//
//     final hotHome = hotHomeFromJson(jsonString);

import 'dart:convert';
import '../entitys/entitys.dart';

class HotHomeRequestData {}

HotHomeResponseData hotHomeFromJson(String str) =>
    HotHomeResponseData.fromJson(json.decode(str));

String hotHomeToJson(HotHomeResponseData data) => json.encode(data.toJson());

class HotHomeResponseData {
  HotHomeResponseData({
    this.webStatus,
    this.code,
    this.message,
    this.data,
    this.result,
    this.paginate,
  });

  int webStatus;
  int code;
  String message;
  dynamic data;
  List<HotHomeResult> result;
  dynamic paginate;

  factory HotHomeResponseData.fromJson(Map<String, dynamic> json) =>
      HotHomeResponseData(
        webStatus: json["web_status"],
        code: json["code"],
        message: json["message"],
        data: json["data"],
        result: List<HotHomeResult>.from(
            json["result"].map((x) => HotHomeResult.fromJson(x))),
        paginate: json["paginate"],
      );

  Map<String, dynamic> toJson() => {
        "web_status": webStatus,
        "code": code,
        "message": message,
        "data": data,
        "result": List<dynamic>.from(result.map((x) => x.toJson())),
        "paginate": paginate,
      };
}

class HotHomeResult {
  HotHomeResult({
    this.liveTypeId,
    this.name,
    this.rooms,
  });

  int liveTypeId;
  String name;
  List<Room> rooms;

  factory HotHomeResult.fromJson(Map<String, dynamic> json) => HotHomeResult(
        liveTypeId: json["live_type_id"],
        name: json["name"],
        rooms: List<Room>.from(json["rooms"].map((x) => Room.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "live_type_id": liveTypeId,
        "name": name,
        "rooms": List<dynamic>.from(rooms.map((x) => x.toJson())),
      };
}

class Room {
  Room({
    this.nickname,
    this.unionid,
    this.icon,
    this.id,
    this.roomNum,
    this.title,
    this.gameName,
    this.roomLiveType,
    this.liveTypeParent,
    this.filterSportId,
    this.filterLid,
    this.sportName,
    this.liveStatus,
    this.hotSort,
    this.total,
    this.accountTitle,
    this.channelNum,
    this.liveType,
    this.kind,
    this.scheduleRoom,
    this.iframeId,
    this.imageUrl,
    this.iframeUrl,
    this.visitCount,
    //this.channel,
    this.updatedAt,
    this.name,
    this.info,
    this.accountsId,
    this.publishSetting,
    this.createdAt,
    this.endAt,
    this.liveTypesId,
    this.urlFhd,
    this.urlHd,
    this.urlSd,
    this.thumbnailUrl,
    this.countInit,
    this.visitCountTotal,
    this.focusCount,
    this.startAt,
    this.types,
    this.linkUrl,
    this.defaultLikeCount,
  });

  String nickname;
  String unionid;
  String icon;
  int id;
  String roomNum;
  String title;
  String gameName;
  int roomLiveType;
  dynamic liveTypeParent;
  int filterSportId;
  int filterLid;
  SportName sportName;
  int liveStatus;
  int hotSort;
  int total;
  //AccountTitle accountTitle;
  String accountTitle;
  dynamic channelNum;
  String liveType;
  Kind kind;
  List<ScheduleRoom> scheduleRoom;
  int iframeId;
  String imageUrl;
  String iframeUrl;
  dynamic visitCount;
  //Channel channel;
  DateTime updatedAt;
  String name;
  Info info;
  int accountsId;
  int publishSetting;
  DateTime createdAt;
  DateTime endAt;
  int liveTypesId;
  String urlFhd;
  String urlHd;
  String urlSd;
  String thumbnailUrl;
  int countInit;
  int visitCountTotal;
  int focusCount;
  DateTime startAt;
  int types;
  String linkUrl;
  int defaultLikeCount;

  factory Room.fromJson(Map<String, dynamic> json) => Room(
        nickname: json["nickname"],
        unionid: json["unionid"] == null ? null : json["unionid"],
        icon: json["icon"],
        id: json["id"],
        roomNum: json["room_num"] == null ? null : json["room_num"],
        title: json["title"] == null ? null : json["title"],
        gameName: json["game_name"] == null ? null : json["game_name"],
        roomLiveType: json["live_type"] == null ? null : json["live_type"],
        liveTypeParent: json["live_type_parent"],
        filterSportId:
            json["filter_sport_id"] == null ? null : json["filter_sport_id"],
        filterLid: json["filter_lid"] == null ? null : json["filter_lid"],
        sportName: json["sport_name"] == null
            ? null
            : sportNameValues.map[json["sport_name"]],
        liveStatus: json["live_status"] == null ? null : json["live_status"],
        hotSort: json["hot_sort"],
        total: json["total"],
        // accountTitle: accountTitleValues.map[json["accountTitle"]],
        accountTitle: json["accountTitle"] == null ? "" : json["accountTitle"],
        channelNum: json["channel_num"],
        liveType: json["liveType"],
        kind: kindValues.map[json["kind"]],
        scheduleRoom: json["schedule_room"] == null
            ? null
            : List<ScheduleRoom>.from(
                json["schedule_room"].map((x) => ScheduleRoom.fromJson(x))),
        iframeId: json["iframe_id"] == null ? null : json["iframe_id"],
        imageUrl: json["image_url"] == null ? null : json["image_url"],
        iframeUrl: json["iframe_url"] == null ? null : json["iframe_url"],
        visitCount: json["visitCount"],
        //channel:
        //    json["channel"] == null ? null : Channel.fromJson(json["channel"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        name: json["name"] == null ? null : json["name"],
        info: json["info"] == null ? null : infoValues.map[json["info"]],
        accountsId: json["accounts_id"] == null ? null : json["accounts_id"],
        publishSetting:
            json["publish_setting"] == null ? null : json["publish_setting"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        endAt: json["end_at"] == null ? null : DateTime.parse(json["end_at"]),
        liveTypesId:
            json["live_types_id"] == null ? null : json["live_types_id"],
        urlFhd: json["url_fhd"] == null ? null : json["url_fhd"],
        urlHd: json["url_hd"] == null ? null : json["url_hd"],
        urlSd: json["url_sd"] == null ? null : json["url_sd"],
        thumbnailUrl:
            json["thumbnail_url"] == null ? null : json["thumbnail_url"],
        countInit: json["count_init"] == null ? null : json["count_init"],
        visitCountTotal: json["visit_count_total"] == null
            ? null
            : json["visit_count_total"],
        focusCount: json["focus_count"] == null ? null : json["focus_count"],
        startAt:
            json["start_at"] == null ? null : DateTime.parse(json["start_at"]),
        types: json["types"] == null ? null : json["types"],
        linkUrl: json["link_url"] == null ? null : json["link_url"],
        defaultLikeCount: json["default_like_count"] == null
            ? null
            : json["default_like_count"],
      );

  Map<String, dynamic> toJson() => {
        "nickname": nickname,
        "unionid": unionid == null ? null : unionid,
        "icon": icon,
        "id": id,
        "room_num": roomNum == null ? null : roomNum,
        "title": title == null ? null : title,
        "game_name": gameName == null ? null : gameName,
        "live_type": roomLiveType == null ? null : roomLiveType,
        "live_type_parent": liveTypeParent,
        "filter_sport_id": filterSportId == null ? null : filterSportId,
        "filter_lid": filterLid == null ? null : filterLid,
        "sport_name":
            sportName == null ? null : sportNameValues.reverse[sportName],
        "live_status": liveStatus == null ? null : liveStatus,
        "hot_sort": hotSort,
        "total": total,
        "accountTitle": accountTitleValues.reverse[accountTitle],
        "channel_num": channelNum,
        "liveType": liveType,
        "kind": kindValues.reverse[kind],
        "schedule_room": scheduleRoom == null
            ? null
            : List<dynamic>.from(scheduleRoom.map((x) => x.toJson())),
        "iframe_id": iframeId == null ? null : iframeId,
        "image_url": imageUrl == null ? null : imageUrl,
        "iframe_url": iframeUrl == null ? null : iframeUrl,
        "visitCount": visitCount,
        //"channel": channel == null ? null : channel.toJson(),
        "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
        "name": name == null ? null : name,
        "info": info == null ? null : infoValues.reverse[info],
        "accounts_id": accountsId == null ? null : accountsId,
        "publish_setting": publishSetting == null ? null : publishSetting,
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
        "end_at": endAt == null ? null : endAt.toIso8601String(),
        "live_types_id": liveTypesId == null ? null : liveTypesId,
        "url_fhd": urlFhd == null ? null : urlFhd,
        "url_hd": urlHd == null ? null : urlHd,
        "url_sd": urlSd == null ? null : urlSd,
        "thumbnail_url": thumbnailUrl == null ? null : thumbnailUrl,
        "count_init": countInit == null ? null : countInit,
        "visit_count_total": visitCountTotal == null ? null : visitCountTotal,
        "focus_count": focusCount == null ? null : focusCount,
        "start_at": startAt == null ? null : startAt.toIso8601String(),
        "types": types == null ? null : types,
        "link_url": linkUrl == null ? null : linkUrl,
        "default_like_count":
            defaultLikeCount == null ? null : defaultLikeCount,
      };
}

enum AccountTitle { EMPTY, ACCOUNT_TITLE, PURPLE }

final accountTitleValues = EnumValues({
  "红单分析": AccountTitle.ACCOUNT_TITLE,
  "红单天后": AccountTitle.EMPTY,
  "": AccountTitle.PURPLE
});

// class Channel {
//   Channel({
//     this.channelNum,
//   });

//   dynamic channelNum;

//   factory Channel.fromJson(Map<String, dynamic> json) => Channel(
//         channelNum: json["channel_num"],
//       );

//   Map<String, dynamic> toJson() => {
//         "channel_num": channelNum,
//       };
// }

enum Info { EMPTY, INFO }

final infoValues =
    EnumValues({"精彩资讯视频公开": Info.EMPTY, "乐动专属资讯影片公开": Info.INFO});

enum Kind { IFRAME, LIVE, VIDEO }

final kindValues =
    EnumValues({"iframe": Kind.IFRAME, "live": Kind.LIVE, "video": Kind.VIDEO});

class ScheduleRoom {
  ScheduleRoom({
    this.id,
    this.mid,
    this.lid,
    this.aisportId,
    this.lname,
    this.lnameEn,
    this.hid,
    this.hname,
    this.hnameEn,
    this.aid,
    this.aname,
    this.anameEn,
    this.hicon,
    this.aicon,
    this.coverImgUrl,
    this.coverImgUrlSecond,
    this.time,
    this.endTime,
    this.status,
    this.sport,
    this.sportName,
    this.hot,
    this.tag,
    this.liveTypesChildId,
    this.videoId,
    this.action,
    this.createdAt,
    this.updatedAt,
    this.relationRoomUpdatedAt,
    this.leisuId,
    this.schId,
    this.roomId,
    this.roomNum,
    this.createSort,
    this.name,
    this.type,
    this.iframeUrl,
    this.iconUrl,
    this.coverUrl,
    this.displayType,
    this.backUrl,
    this.tags,
    this.streamUrl,
    this.streamStatus,
    this.msrTags,
    this.unionid,
  });

  int id;
  int mid;
  int lid;
  String aisportId;
  String lname;
  String lnameEn;
  String hid;
  String hname;
  String hnameEn;
  String aid;
  String aname;
  String anameEn;
  String hicon;
  String aicon;
  String coverImgUrl;
  String coverImgUrlSecond;
  DateTime time;
  DateTime endTime;
  int status;
  int sport;
  SportName sportName;
  int hot;
  Tag tag;
  int liveTypesChildId;
  dynamic videoId;
  int action;
  DateTime createdAt;
  DateTime updatedAt;
  DateTime relationRoomUpdatedAt;
  String leisuId;
  int schId;
  int roomId;
  String roomNum;
  int createSort;
  String name;
  Kind type;
  String iframeUrl;
  dynamic iconUrl;
  String coverUrl;
  dynamic displayType;
  dynamic backUrl;
  String tags;
  String streamUrl;
  int streamStatus;
  String msrTags;
  String unionid;

  factory ScheduleRoom.fromJson(Map<String, dynamic> json) => ScheduleRoom(
        id: json["id"],
        mid: json["mid"],
        lid: json["lid"],
        aisportId: json["aisportId"] == null ? null : json["aisportId"],
        lname: json["lname"],
        lnameEn: json["lnameEN"] == null ? null : json["lnameEN"],
        hid: json["hid"] == null ? null : json["hid"],
        hname: json["hname"],
        hnameEn: json["hnameEN"] == null ? null : json["hnameEN"],
        aid: json["aid"] == null ? null : json["aid"],
        aname: json["aname"],
        anameEn: json["anameEN"] == null ? null : json["anameEN"],
        hicon: json["hicon"],
        aicon: json["aicon"],
        coverImgUrl: json["cover_img_url"],
        coverImgUrlSecond: json["cover_img_url_second"],
        time: DateTime.parse(json["time"]),
        endTime: DateTime.parse(json["end_time"]),
        status: json["status"],
        sport: json["sport"],
        sportName: sportNameValues.map[json["sportName"]],
        hot: json["hot"],
        tag: tagValues.map[json["tag"]],
        liveTypesChildId: json["live_types_child_id"],
        videoId: json["video_id"],
        action: json["action"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        relationRoomUpdatedAt: DateTime.parse(json["relation_room_updated_at"]),
        leisuId: json["leisuId"],
        schId: json["sch_id"],
        roomId: json["room_id"],
        roomNum: json["room_num"],
        createSort: json["create_sort"],
        name: json["name"],
        type: kindValues.map[json["type"]],
        iframeUrl: json["iframe_url"],
        iconUrl: json["icon_url"],
        coverUrl: json["cover_url"],
        displayType: json["display_type"],
        backUrl: json["back_url"],
        tags: json["tags"],
        streamUrl: json["StreamUrl"],
        streamStatus: json["stream_status"],
        msrTags: json["msr_tags"],
        unionid: json["unionid"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "mid": mid,
        "lid": lid,
        "aisportId": aisportId == null ? null : aisportId,
        "lname": lname,
        "lnameEN": lnameEn == null ? null : lnameEn,
        "hid": hid == null ? null : hid,
        "hname": hname,
        "hnameEN": hnameEn == null ? null : hnameEn,
        "aid": aid == null ? null : aid,
        "aname": aname,
        "anameEN": anameEn == null ? null : anameEn,
        "hicon": hicon,
        "aicon": aicon,
        "cover_img_url": coverImgUrl,
        "cover_img_url_second": coverImgUrlSecond,
        "time": time.toIso8601String(),
        "end_time": endTime.toIso8601String(),
        "status": status,
        "sport": sport,
        "sportName": sportNameValues.reverse[sportName],
        "hot": hot,
        "tag": tagValues.reverse[tag],
        "live_types_child_id": liveTypesChildId,
        "video_id": videoId,
        "action": action,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "relation_room_updated_at": relationRoomUpdatedAt.toIso8601String(),
        "leisuId": leisuId,
        "sch_id": schId,
        "room_id": roomId,
        "room_num": roomNum,
        "create_sort": createSort,
        "name": name,
        "type": kindValues.reverse[type],
        "iframe_url": iframeUrl,
        "icon_url": iconUrl,
        "cover_url": coverUrl,
        "display_type": displayType,
        "back_url": backUrl,
        "tags": tags,
        "StreamUrl": streamUrl,
        "stream_status": streamStatus,
        "msr_tags": msrTags,
        "unionid": unionid,
      };
}

enum SportName { EMPTY, SPORT_NAME, PURPLE }

final sportNameValues = EnumValues({
  "足球": SportName.EMPTY,
  "电子竞技": SportName.PURPLE,
  "山猫看点": SportName.SPORT_NAME
});

enum Tag { EMPTY, TAG }

final tagValues = EnumValues({"热门": Tag.EMPTY, "赛事": Tag.TAG});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
