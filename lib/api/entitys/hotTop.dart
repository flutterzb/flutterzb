import 'dart:convert';

class HotTopRequestData {}

HotTopResponseData hotTopResponseDataFromJson(String str) =>
    HotTopResponseData.fromJson(json.decode(str));

String hotTopResponseDataToJson(HotTopResponseData data) =>
    json.encode(data.toJson());

class HotTopResponseData {
  HotTopResponseData({
    this.webStatus,
    this.code,
    this.message,
    this.data,
    this.result,
    this.paginate,
  });

  int webStatus;
  int code;
  String message;
  dynamic data;
  List<HotTopResult> result;
  dynamic paginate;

  factory HotTopResponseData.fromJson(Map<String, dynamic> json) =>
      HotTopResponseData(
        webStatus: json["web_status"],
        code: json["code"],
        message: json["message"],
        data: json["data"],
        result: List<HotTopResult>.from(
            json["result"].map((x) => HotTopResult.fromJson(x))),
        paginate: json["paginate"],
      );

  Map<String, dynamic> toJson() => {
        "web_status": webStatus,
        "code": code,
        "message": message,
        "data": data,
        "result": List<dynamic>.from(result.map((x) => x.toJson())),
        "paginate": paginate,
      };

  printJson() {
    JsonEncoder encoder = new JsonEncoder.withIndent('  ');
    String prettyprint = encoder.convert(this);
    print(prettyprint);
  }
}

class HotTopResult {
  HotTopResult({
    this.nickname,
    this.unionid,
    this.icon,
    this.id,
    this.roomNum,
    this.title,
    this.gameName,
    this.resultLiveType,
    this.liveTypeParent,
    this.filterSportId,
    this.filterLid,
    this.sportName,
    this.liveStatus,
    this.hotSort,
    this.total,
    this.accountTitle,
    this.channelNum,
    this.liveType,
    this.kind,
    this.scheduleRoom,
    this.iframeId,
    this.imageUrl,
    this.iframeUrl,
    this.visitCount,
    this.streamUrlFlv,
    this.streamUrlHls,
    this.streamUrlRtmp,
    this.msrType,
    this.tags,
    this.msrId,
    this.mid,
    this.teamInfo,
    //this.channel,
  });

  String nickname;
  String unionid;
  String icon;
  int id;
  String roomNum;
  String title;
  String gameName;
  int resultLiveType;
  dynamic liveTypeParent;
  int filterSportId;
  int filterLid;
  String sportName;
  int liveStatus;
  int hotSort;
  int total;
  String accountTitle;
  dynamic channelNum;
  String liveType;
  String kind;
  List<ScheduleRoom> scheduleRoom;
  int iframeId;
  String imageUrl;
  String iframeUrl;
  dynamic visitCount;
  Map<String, String> streamUrlFlv;
  Map<String, String> streamUrlHls;
  Map<String, String> streamUrlRtmp;
  String msrType;
  dynamic tags;
  int msrId;
  int mid;
  String teamInfo;
  //Channel channel;

  factory HotTopResult.fromJson(Map<String, dynamic> json) => HotTopResult(
        nickname: json["nickname"],
        unionid: json["unionid"],
        icon: json["icon"],
        id: json["id"],
        roomNum: json["room_num"],
        title: json["title"],
        gameName: json["game_name"],
        resultLiveType: json["live_type"],
        liveTypeParent: json["live_type_parent"],
        filterSportId: json["filter_sport_id"],
        filterLid: json["filter_lid"],
        sportName: json["sport_name"],
        liveStatus: json["live_status"],
        hotSort: json["hot_sort"],
        total: json["total"],
        accountTitle: json["accountTitle"],
        channelNum: json["channel_num"],
        liveType: json["liveType"],
        kind: json["kind"],
        scheduleRoom: json["schedule_room"] != null
            ? List<ScheduleRoom>.from(
                json["schedule_room"].map((x) => ScheduleRoom.fromJson(x)))
            : List<ScheduleRoom>(),
        iframeId: json["iframe_id"],
        imageUrl: json["image_url"],
        iframeUrl: json["iframe_url"],
        visitCount: json["visitCount"],
        streamUrlFlv: Map.from(json["stream_url_Flv"])
            .map((k, v) => MapEntry<String, String>(k, v)),
        streamUrlHls: Map.from(json["stream_url_Hls"])
            .map((k, v) => MapEntry<String, String>(k, v)),
        streamUrlRtmp: Map.from(json["stream_url_Rtmp"])
            .map((k, v) => MapEntry<String, String>(k, v)),
        msrType: json["msr_type"],
        tags: json["tags"],
        msrId: json["msr_id"],
        mid: json["mid"],
        teamInfo: json["team_info"],
        //channel: Channel.fromJson(json["channel"]),
      );

  Map<String, dynamic> toJson() => {
        "nickname": nickname,
        "unionid": unionid,
        "icon": icon,
        "id": id,
        "room_num": roomNum,
        "title": title,
        "game_name": gameName,
        "live_type": resultLiveType,
        "live_type_parent": liveTypeParent,
        "filter_sport_id": filterSportId,
        "filter_lid": filterLid,
        "sport_name": sportName,
        "live_status": liveStatus,
        "hot_sort": hotSort,
        "total": total,
        "accountTitle": accountTitle,
        "channel_num": channelNum,
        "liveType": liveType,
        "kind": kind,
        "schedule_room":
            List<dynamic>.from(scheduleRoom.map((x) => x.toJson())),
        "iframe_id": iframeId,
        "image_url": imageUrl,
        "iframe_url": iframeUrl,
        "visitCount": visitCount,
        "stream_url_Flv": Map.from(streamUrlFlv)
            .map((k, v) => MapEntry<String, dynamic>(k, v)),
        "stream_url_Hls": Map.from(streamUrlHls)
            .map((k, v) => MapEntry<String, dynamic>(k, v)),
        "stream_url_Rtmp": Map.from(streamUrlRtmp)
            .map((k, v) => MapEntry<String, dynamic>(k, v)),
        "msr_type": msrType,
        "tags": tags,
        "msr_id": msrId,
        "mid": mid,
        "team_info": teamInfo,
        //"channel": channel.toJson(),
      };

  printJson() {
    JsonEncoder encoder = new JsonEncoder.withIndent('  ');
    String prettyprint = encoder.convert(this);
    print(prettyprint);
  }
}

class ScheduleRoom {
  ScheduleRoom({
    this.id,
    this.mid,
    this.lid,
    this.aisportId,
    this.lname,
    this.lnameEn,
    this.hid,
    this.hname,
    this.hnameEn,
    this.aid,
    this.aname,
    this.anameEn,
    this.hicon,
    this.aicon,
    this.coverImgUrl,
    this.coverImgUrlSecond,
    this.time,
    this.endTime,
    this.status,
    this.sport,
    this.sportName,
    this.hot,
    this.tag,
    this.liveTypesChildId,
    this.videoId,
    this.action,
    this.createdAt,
    this.updatedAt,
    this.relationRoomUpdatedAt,
    this.leisuId,
    this.schId,
    this.roomId,
    this.roomNum,
    this.createSort,
    this.name,
    this.type,
    this.iframeUrl,
    this.iconUrl,
    this.coverUrl,
    this.displayType,
    this.backUrl,
    this.tags,
    this.streamUrl,
    this.streamStatus,
    this.msrTags,
    this.unionid,
  });

  int id;
  int mid;
  int lid;
  String aisportId;
  String lname;
  String lnameEn;
  String hid;
  String hname;
  String hnameEn;
  String aid;
  String aname;
  String anameEn;
  String hicon;
  String aicon;
  String coverImgUrl;
  String coverImgUrlSecond;
  DateTime time;
  DateTime endTime;
  int status;
  int sport;
  String sportName;
  int hot;
  String tag;
  int liveTypesChildId;
  dynamic videoId;
  int action;
  DateTime createdAt;
  DateTime updatedAt;
  DateTime relationRoomUpdatedAt;
  String leisuId;
  int schId;
  int roomId;
  String roomNum;
  int createSort;
  String name;
  String type;
  String iframeUrl;
  dynamic iconUrl;
  String coverUrl;
  dynamic displayType;
  dynamic backUrl;
  String tags;
  String streamUrl;
  int streamStatus;
  String msrTags;
  String unionid;

  factory ScheduleRoom.fromJson(Map<String, dynamic> json) => ScheduleRoom(
        id: json["id"],
        mid: json["mid"],
        lid: json["lid"],
        aisportId: json["aisportId"],
        lname: json["lname"],
        lnameEn: json["lnameEN"],
        hid: json["hid"],
        hname: json["hname"],
        hnameEn: json["hnameEN"],
        aid: json["aid"],
        aname: json["aname"],
        anameEn: json["anameEN"],
        hicon: json["hicon"],
        aicon: json["aicon"],
        coverImgUrl: json["cover_img_url"],
        coverImgUrlSecond: json["cover_img_url_second"],
        time: DateTime.parse(json["time"]),
        endTime: DateTime.parse(json["end_time"]),
        status: json["status"],
        sport: json["sport"],
        sportName: json["sportName"],
        hot: json["hot"],
        tag: json["tag"],
        liveTypesChildId: json["live_types_child_id"],
        videoId: json["video_id"],
        action: json["action"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        relationRoomUpdatedAt: DateTime.parse(json["relation_room_updated_at"]),
        leisuId: json["leisuId"],
        schId: json["sch_id"],
        roomId: json["room_id"],
        roomNum: json["room_num"],
        createSort: json["create_sort"],
        name: json["name"],
        type: json["type"],
        iframeUrl: json["iframe_url"],
        iconUrl: json["icon_url"],
        coverUrl: json["cover_url"],
        displayType: json["display_type"],
        backUrl: json["back_url"],
        tags: json["tags"],
        streamUrl: json["StreamUrl"],
        streamStatus: json["stream_status"],
        msrTags: json["msr_tags"],
        unionid: json["unionid"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "mid": mid,
        "lid": lid,
        "aisportId": aisportId,
        "lname": lname,
        "lnameEN": lnameEn,
        "hid": hid,
        "hname": hname,
        "hnameEN": hnameEn,
        "aid": aid,
        "aname": aname,
        "anameEN": anameEn,
        "hicon": hicon,
        "aicon": aicon,
        "cover_img_url": coverImgUrl,
        "cover_img_url_second": coverImgUrlSecond,
        "time": time.toIso8601String(),
        "end_time": endTime.toIso8601String(),
        "status": status,
        "sport": sport,
        "sportName": sportName,
        "hot": hot,
        "tag": tag,
        "live_types_child_id": liveTypesChildId,
        "video_id": videoId,
        "action": action,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "relation_room_updated_at": relationRoomUpdatedAt.toIso8601String(),
        "leisuId": leisuId,
        "sch_id": schId,
        "room_id": roomId,
        "room_num": roomNum,
        "create_sort": createSort,
        "name": name,
        "type": type,
        "iframe_url": iframeUrl,
        "icon_url": iconUrl,
        "cover_url": coverUrl,
        "display_type": displayType,
        "back_url": backUrl,
        "tags": tags,
        "StreamUrl": streamUrl,
        "stream_status": streamStatus,
        "msr_tags": msrTags,
        "unionid": unionid,
      };
}
