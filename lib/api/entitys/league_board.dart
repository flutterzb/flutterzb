import 'dart:convert';

LeagueBoardResponesData leagueBoardResponesDataFromJson(String str) =>
    LeagueBoardResponesData.fromJson(json.decode(str));

String leagueBoardResponesDataToJson(LeagueBoardResponesData data) =>
    json.encode(data.toJson());

class LeagueBoardResponesData {
  LeagueBoardResponesData({
    this.code,
    this.message,
    this.data,
  });

  int code;
  String message;
  List<FirstLayerData> data;

  factory LeagueBoardResponesData.fromJson(Map<String, dynamic> json) =>
      LeagueBoardResponesData(
        code: json["code"],
        message: json["message"],
        data: List<FirstLayerData>.from(
            json["data"].map((x) => FirstLayerData.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "code": code,
        "message": message,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class FirstLayerData {
  FirstLayerData({
    this.name,
    this.year,
    this.tag,
    this.sportId,
    this.data,
    this.updatedAt,
  });

  String name;
  String year;
  String tag;
  int sportId;
  List<SecondLayerData> data;
  DateTime updatedAt;

  factory FirstLayerData.fromJson(Map<String, dynamic> json) => FirstLayerData(
        name: json["name"],
        year: json["year"],
        tag: json["tag"],
        sportId: json["sportId"],
        data: List<SecondLayerData>.from(
            json["data"].map((x) => SecondLayerData.fromJson(x))),
        updatedAt: DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "year": year,
        "tag": tag,
        "sportId": sportId,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "updated_at": updatedAt.toIso8601String(),
      };
}

class SecondLayerData {
  SecondLayerData({
    this.tag,
    this.sportId,
    this.scope,
    this.groupId,
    this.group,
    this.rows,
  });

  String tag;
  int sportId;
  int scope;
  int groupId;
  String group;
  List<LeagueGroupRowData> rows;

  factory SecondLayerData.fromJson(Map<String, dynamic> json) =>
      SecondLayerData(
        tag: json["tag"],
        sportId: json["sportId"],
        scope: json["scope"] == null ? null : json["scope"],
        groupId: json["groupId"],
        group: json["group"],
        rows: List<LeagueGroupRowData>.from(
            json["rows"].map((x) => LeagueGroupRowData.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "tag": tag,
        "sportId": sportId,
        "scope": scope == null ? null : scope,
        "groupId": groupId,
        "group": group,
        "rows": List<dynamic>.from(rows.map((x) => x.toJson())),
      };
}

class LeagueGroupRowData {
  LeagueGroupRowData({
    this.teamId,
    this.teamLeisuId,
    this.position,
    this.won,
    this.loss,
    this.gameBack,
    this.pointsAvg,
    this.pointsAgainstAvg,
    this.diffAvg,
    this.streaks,
    this.home,
    this.away,
    this.division,
    this.conference,
    this.last10,
    this.points,
    this.total,
    this.draw,
    this.goals,
    this.goalsAgainst,
    this.teamName,
    this.teamIcon,
    this.deductPoints,
    this.goalDiff,
  });

  String teamId;
  int teamLeisuId;
  int position;
  int won;
  int loss;
  String gameBack;
  String pointsAvg;
  String pointsAgainstAvg;
  String diffAvg;
  int streaks;
  String home;
  String away;
  String division;
  String conference;
  String last10;
  int points;
  int total;
  int draw;
  int goals;
  int goalsAgainst;
  String teamName;
  String teamIcon;
  int deductPoints;
  int goalDiff;

  factory LeagueGroupRowData.fromJson(Map<String, dynamic> json) =>
      LeagueGroupRowData(
        teamId: json["team_id"],
        teamLeisuId: json["team_leisuId"],
        position: json["position"],
        won: json["won"],
        loss: json["loss"],
        gameBack: json["game_back"] == null ? null : json["game_back"],
        pointsAvg: json["points_avg"] == null ? null : json["points_avg"],
        pointsAgainstAvg: json["points_against_avg"] == null
            ? null
            : json["points_against_avg"],
        diffAvg: json["diff_avg"] == null ? null : json["diff_avg"],
        streaks: json["streaks"] == null ? null : json["streaks"],
        home: json["home"] == null ? null : json["home"],
        away: json["away"] == null ? null : json["away"],
        division: json["division"] == null ? null : json["division"],
        conference: json["conference"] == null ? null : json["conference"],
        last10: json["last_10"] == null ? null : json["last_10"],
        points: json["points"],
        total: json["total"],
        draw: json["draw"],
        goals: json["goals"],
        goalsAgainst: json["goals_against"],
        teamName: json["team_name"],
        teamIcon: json["team_icon"],
        deductPoints:
            json["deduct_points"] == null ? null : json["deduct_points"],
        goalDiff: json["goal_diff"] == null ? null : json["goal_diff"],
      );

  Map<String, dynamic> toJson() => {
        "team_id": teamId,
        "team_leisuId": teamLeisuId,
        "position": position,
        "won": won,
        "loss": loss,
        "game_back": gameBack == null ? null : gameBack,
        "points_avg": pointsAvg == null ? null : pointsAvg,
        "points_against_avg":
            pointsAgainstAvg == null ? null : pointsAgainstAvg,
        "diff_avg": diffAvg == null ? null : diffAvg,
        "streaks": streaks == null ? null : streaks,
        "home": home == null ? null : home,
        "away": away == null ? null : away,
        "division": division == null ? null : division,
        "conference": conference == null ? null : conference,
        "last_10": last10 == null ? null : last10,
        "points": points,
        "total": total,
        "draw": draw,
        "goals": goals,
        "goals_against": goalsAgainst,
        "team_name": teamName,
        "team_icon": teamIcon,
        "deduct_points": deductPoints == null ? null : deductPoints,
        "goal_diff": goalDiff == null ? null : goalDiff,
      };
}

// class LeagueBoardResponesData {
//   int code;
//   String message;
//   List<FistLayerData> data;

//   LeagueBoardResponesData({this.code, this.message, this.data});

//   LeagueBoardResponesData.fromJson(Map<String, dynamic> json) {
//     code = json['code'];
//     message = json['message'];
//     if (json['data'] != null) {
//       data = new List<FistLayerData>();
//       json['data'].forEach((v) {
//         data.add(new FistLayerData.fromJson(v));
//       });
//     }
//   }

//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['code'] = this.code;
//     data['message'] = this.message;
//     if (this.data != null) {
//       data['data'] = this.data.map((v) => v.toJson()).toList();
//     }
//     return data;
//   }
// }

// class FistLayerData {
//   String name;
//   String year;
//   String tag;
//   int sportId;
//   List<LeagueBoardData> data;
//   String updatedAt;

//   FistLayerData(
//       {this.name,
//       this.year,
//       this.tag,
//       this.sportId,
//       this.data,
//       this.updatedAt});

//   FistLayerData.fromJson(Map<String, dynamic> json) {
//     name = json['name'];
//     year = json['year'];
//     tag = json['tag'];
//     sportId = json['sportId'];
//     if (json['data'] != null) {
//       data = new List<LeagueBoardData>();
//       json['data'].forEach((v) {
//         data.add(new LeagueBoardData.fromJson(v));
//       });
//     }
//     updatedAt = json['updated_at'];
//   }

//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['name'] = this.name;
//     data['year'] = this.year;
//     data['tag'] = this.tag;
//     data['sportId'] = this.sportId;
//     if (this.data != null) {
//       data['data'] = this.data.map((v) => v.toJson()).toList();
//     }
//     data['updated_at'] = this.updatedAt;
//     return data;
//   }
// }

// class LeagueBoardData {
//   String tag;
//   int sportId;
//   int scope;
//   int groupId;
//   String group;
//   List<Rows> rows;

//   LeagueBoardData(
//       {this.tag,
//       this.sportId,
//       this.scope,
//       this.groupId,
//       this.group,
//       this.rows});

//   LeagueBoardData.fromJson(Map<String, dynamic> json) {
//     tag = json['tag'];
//     sportId = json['sportId'];
//     scope = json['scope'];
//     groupId = json['groupId'];
//     group = json['group'];
//     if (json['rows'] != null) {
//       rows = new List<Rows>();
//       json['rows'].forEach((v) {
//         rows.add(new Rows.fromJson(v));
//       });
//     }
//   }

//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['tag'] = this.tag;
//     data['sportId'] = this.sportId;
//     data['scope'] = this.scope;
//     data['groupId'] = this.groupId;
//     data['group'] = this.group;
//     if (this.rows != null) {
//       data['rows'] = this.rows.map((v) => v.toJson()).toList();
//     }
//     return data;
//   }
// }

// class Rows {
//   String teamId;
//   int teamLeisuId;
//   int position;
//   int won;
//   int loss;
//   String gameBack;
//   String pointsAvg;
//   String pointsAgainstAvg;
//   String diffAvg;
//   int streaks;
//   String home;
//   String away;
//   String division;
//   String conference;
//   String last10;
//   int points;
//   int total;
//   int draw;
//   int goals;
//   int goalsAgainst;
//   String teamName;
//   String teamIcon;
//   int deductPoints;
//   int goalDiff;

//   Rows(
//       {this.teamId,
//       this.teamLeisuId,
//       this.position,
//       this.won,
//       this.loss,
//       this.gameBack,
//       this.pointsAvg,
//       this.pointsAgainstAvg,
//       this.diffAvg,
//       this.streaks,
//       this.home,
//       this.away,
//       this.division,
//       this.conference,
//       this.last10,
//       this.points,
//       this.total,
//       this.draw,
//       this.goals,
//       this.goalsAgainst,
//       this.teamName,
//       this.teamIcon,
//       this.deductPoints,
//       this.goalDiff});

//   Rows.fromJson(Map<String, dynamic> json) {
//     teamId = json['team_id'];
//     teamLeisuId = json['team_leisuId'];
//     position = json['position'];
//     won = json['won'];
//     loss = json['loss'];
//     gameBack = json['game_back'];
//     pointsAvg = json['points_avg'];
//     pointsAgainstAvg = json['points_against_avg'];
//     diffAvg = json['diff_avg'];
//     streaks = json['streaks'];
//     home = json['home'];
//     away = json['away'];
//     division = json['division'];
//     conference = json['conference'];
//     last10 = json['last_10'];
//     points = json['points'];
//     total = json['total'];
//     draw = json['draw'];
//     goals = json['goals'];
//     goalsAgainst = json['goals_against'];
//     teamName = json['team_name'];
//     teamIcon = json['team_icon'];
//     deductPoints = json['deduct_points'];
//     goalDiff = json['goal_diff'];
//   }

//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['team_id'] = this.teamId;
//     data['team_leisuId'] = this.teamLeisuId;
//     data['position'] = this.position;
//     data['won'] = this.won;
//     data['loss'] = this.loss;
//     data['game_back'] = this.gameBack;
//     data['points_avg'] = this.pointsAvg;
//     data['points_against_avg'] = this.pointsAgainstAvg;
//     data['diff_avg'] = this.diffAvg;
//     data['streaks'] = this.streaks;
//     data['home'] = this.home;
//     data['away'] = this.away;
//     data['division'] = this.division;
//     data['conference'] = this.conference;
//     data['last_10'] = this.last10;
//     data['points'] = this.points;
//     data['total'] = this.total;
//     data['draw'] = this.draw;
//     data['goals'] = this.goals;
//     data['goals_against'] = this.goalsAgainst;
//     data['team_name'] = this.teamName;
//     data['team_icon'] = this.teamIcon;
//     data['deduct_points'] = this.deductPoints;
//     data['goal_diff'] = this.goalDiff;
//     return data;
//   }
// }
