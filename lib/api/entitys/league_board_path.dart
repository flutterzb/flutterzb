import 'dart:convert';

class LeagueBoardPathResponesData {
  int webStatus;
  int code;
  String message;
  String data;
  String result;
  Null paginate;

  LeagueBoardPathResponesData(
      {this.webStatus,
      this.code,
      this.message,
      this.data,
      this.result,
      this.paginate});

  LeagueBoardPathResponesData.fromJson(Map<String, dynamic> json) {
    webStatus = json['web_status'];
    code = json['code'];
    message = json['message'];
    data = json['data'];
    result = json['result'];
    paginate = json['paginate'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['web_status'] = this.webStatus;
    data['code'] = this.code;
    data['message'] = this.message;
    data['data'] = this.data;
    data['result'] = this.result;
    data['paginate'] = this.paginate;
    return data;
  }
}
