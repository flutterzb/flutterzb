// To parse this JSON data, do
//
//     final liveRoomResponseData = liveRoomResponseDataFromJson(jsonString);

import 'dart:convert';

LiveRoomResponseData liveRoomResponseDataFromJson(String str) =>
    LiveRoomResponseData.fromJson(json.decode(str));

String liveRoomResponseDataToJson(LiveRoomResponseData data) =>
    json.encode(data.toJson());

class LiveRoomResponseData {
  LiveRoomResponseData({
    this.webStatus,
    this.code,
    this.message,
    this.data,
    this.result,
    this.paginate,
  });

  int webStatus;
  int code;
  String message;
  dynamic data;
  LiveRoomResult result;
  dynamic paginate;

  factory LiveRoomResponseData.fromJson(Map<String, dynamic> json) =>
      LiveRoomResponseData(
        webStatus: json["web_status"],
        code: json["code"],
        message: json["message"],
        data: json["data"],
        result: json["result"] == false
            ? LiveRoomResult()
            : LiveRoomResult.fromJson(json["result"]),
        paginate: json["paginate"],
      );

  Map<String, dynamic> toJson() => {
        "web_status": webStatus,
        "code": code,
        "message": message,
        "data": data,
        "result": result.toJson(),
        "paginate": paginate,
      };
}

class LiveRoomResult {
  LiveRoomResult({
    this.room,
  });

  LiveRoomInfo room;

  factory LiveRoomResult.fromJson(Map<String, dynamic> json) => LiveRoomResult(
        room: LiveRoomInfo.fromJson(json["room"]),
      );

  Map<String, dynamic> toJson() => {
        "room": room.toJson(),
      };
}

class LiveRoomInfo {
  LiveRoomInfo({
    this.id,
    this.roomNum,
    this.title,
    this.gameName,
    this.notice,
    this.noticeUrl,
    this.anchorIcon,
    this.anchorIntro,
    this.liveType,
    this.liveTypeParent,
    this.liveStatus,
    this.voteStatus,
    //this.socialData,
    this.type,
    this.unionid,
    this.nickname,
    this.icon,
    this.selfIntro,
    this.channelNum,
    this.controlTotal,
    this.isChannel,
    this.accountTitle,
    this.imageUrl,
    this.visitCount,
    this.focusCount,
    this.streamUrlFlv,
    this.streamUrlHls,
    this.streamUrlRtmp,
    this.originUrl,
    this.msrType,
    this.tags,
    this.msrId,
    this.mid,
    this.teamInfo,
    this.callSocialApi,
    this.isRobot,
    this.isFocus,
    this.userChatToken,
  });

  int id;
  String roomNum;
  String title;
  String gameName;
  String notice;
  dynamic noticeUrl;
  dynamic anchorIcon;
  dynamic anchorIntro;
  LiveType liveType;
  LiveType liveTypeParent;
  int liveStatus;
  int voteStatus;
  //SocialData socialData;
  int type;
  String unionid;
  String nickname;
  String icon;
  String selfIntro;
  String channelNum;
  int controlTotal;
  String isChannel;
  String accountTitle;
  String imageUrl;
  int visitCount;
  int focusCount;
  Map<String, String> streamUrlFlv;
  Map<String, String> streamUrlHls;
  Map<String, String> streamUrlRtmp;
  String originUrl;
  String msrType;
  dynamic tags;
  int msrId;
  int mid;
  String teamInfo;
  String callSocialApi;
  String isRobot;
  bool isFocus;
  String userChatToken;

  factory LiveRoomInfo.fromJson(Map<String, dynamic> json) => LiveRoomInfo(
        id: json["id"],
        roomNum: json["room_num"],
        title: json["title"],
        gameName: json["game_name"],
        notice: json["notice"],
        noticeUrl: json["notice_url"],
        anchorIcon: json["anchor_icon"],
        anchorIntro: json["anchor_intro"],
        liveType: LiveType.fromJson(json["live_type"]),
        liveTypeParent: LiveType.fromJson(json["live_type_parent"]),
        liveStatus: json["live_status"],
        voteStatus: json["vote_status"],
        //socialData: SocialData.fromJson(json["social_data"]),
        type: json["type"],
        unionid: json["unionid"],
        nickname: json["nickname"],
        icon: json["icon"],
        selfIntro: json["self_intro"],
        channelNum: json["channel_num"],
        controlTotal: json["control_total"],
        isChannel: json["isChannel"],
        accountTitle: json["accountTitle"],
        imageUrl: json["image_url"],
        visitCount: json["visit_count"],
        focusCount: json["focus_count"],
        streamUrlFlv: Map.from(json["stream_url_Flv"])
            .map((k, v) => MapEntry<String, String>(k, v)),
        streamUrlHls: Map.from(json["stream_url_Hls"])
            .map((k, v) => MapEntry<String, String>(k, v)),
        streamUrlRtmp: Map.from(json["stream_url_Rtmp"])
            .map((k, v) => MapEntry<String, String>(k, v)),
        originUrl: json["origin_url"],
        msrType: json["msr_type"],
        tags: json["tags"],
        msrId: json["msr_id"],
        mid: json["mid"],
        teamInfo: json["team_info"],
        callSocialApi: json["call_social_api"],
        isRobot: json["isRobot"],
        isFocus: json["isFocus"],
        userChatToken: json["user_chat_token"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "room_num": roomNum,
        "title": title,
        "game_name": gameName,
        "notice": notice,
        "notice_url": noticeUrl,
        "anchor_icon": anchorIcon,
        "anchor_intro": anchorIntro,
        "live_type": liveType.toJson(),
        "live_type_parent": liveTypeParent.toJson(),
        "live_status": liveStatus,
        "vote_status": voteStatus,
        //"social_data": socialData.toJson(),
        "type": type,
        "unionid": unionid,
        "nickname": nickname,
        "icon": icon,
        "self_intro": selfIntro,
        "channel_num": channelNum,
        "control_total": controlTotal,
        "isChannel": isChannel,
        "accountTitle": accountTitle,
        "image_url": imageUrl,
        "visit_count": visitCount,
        "focus_count": focusCount,
        "stream_url_Flv": Map.from(streamUrlFlv)
            .map((k, v) => MapEntry<String, dynamic>(k, v)),
        "stream_url_Hls": Map.from(streamUrlHls)
            .map((k, v) => MapEntry<String, dynamic>(k, v)),
        "stream_url_Rtmp": Map.from(streamUrlRtmp)
            .map((k, v) => MapEntry<String, dynamic>(k, v)),
        "origin_url": originUrl,
        "msr_type": msrType,
        "tags": tags,
        "msr_id": msrId,
        "mid": mid,
        "team_info": teamInfo,
        "call_social_api": callSocialApi,
        "isRobot": isRobot,
        "isFocus": isFocus,
        "user_chat_token": userChatToken,
      };
}

class LiveType {
  LiveType({
    this.id,
    this.name,
  });

  int id;
  dynamic name;

  factory LiveType.fromJson(Map<String, dynamic> json) => LiveType(
        id: json["id"],
        name: json["name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
      };
}

// class SocialData {
//   SocialData({
//     this.wechat,
//     this.qqGroup,
//     this.weibo,
//   });

//   QqGroup wechat;
//   QqGroup qqGroup;
//   QqGroup weibo;

//   factory SocialData.fromJson(Map<String, dynamic> json) => SocialData(
//         wechat: QqGroup.fromJson(json["wechat"]),
//         qqGroup: QqGroup.fromJson(json["qq_group"]),
//         weibo: QqGroup.fromJson(json["weibo"]),
//       );

//   Map<String, dynamic> toJson() => {
//         "wechat": wechat.toJson(),
//         "qq_group": qqGroup.toJson(),
//         "weibo": weibo.toJson(),
//       };
// }

// class QqGroup {
//   QqGroup({
//     this.name,
//     this.code,
//     this.url,
//     this.status,
//   });

//   String name;
//   String code;
//   dynamic url;
//   String status;

//   factory QqGroup.fromJson(Map<String, dynamic> json) => QqGroup(
//         name: json["name"],
//         code: json["code"],
//         url: json["url"],
//         status: json["status"],
//       );

//   Map<String, dynamic> toJson() => {
//         "name": name,
//         "code": code,
//         "url": url,
//         "status": status,
//       };
// }
