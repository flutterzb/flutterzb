//login succeed
// {
// 	"web_status": 1,
// 	"code": 0,
// 	"message": "\u767b\u5f55\u6210\u529f",
// 	"data": {
// 		"phone": "8616534261708",
// 		"access_token": "jQ4ylfpqShlRw2NBEPwuSqqdCK2Ka-JQZDAUoYUv-0w6XYKpCY5yVlm4EGlBQT-9uXJJeSL2jlLjPEj7NhcplHqJjRxb1QFvXzEDRRAgqP8=",
// 		"single_token": "w7zmvQzR2Nem4qznSiRNKscRo7AdaAoJj8v4oxxvpKMu4SFLLvE5o_3gJpTz3Hyz2kArfQgVRqI_EJOszjRDwvkj6W5R1d6oMPvRomp5Oow=",
// 		"uuid": "5d8c78a2-be7a-8906-8da1-4c78-3d664a8e",
// 		"app_name": "smzb"
// 	},
// 	"result": false,
// 	"paginate": null
// }

//login fail
// {
// 	"web_status": 1,
// 	"code": -1,
// 	"message": "\u5e10\u53f7\u6216\u8005\u5bc6\u7801\u9519\u8bef",
// 	"data": null,
// 	"result": false,
// 	"paginate": null
// }

import 'dart:convert';

LoginResponseData loginResponseDataFromJson(String str) =>
    LoginResponseData.fromJson(json.decode(str));

String loginResponseDataToJson(LoginResponseData data) =>
    json.encode(data.toJson());

class LoginResponseData {
  LoginResponseData({
    this.webStatus,
    this.code,
    this.message,
    this.data,
    this.result,
    this.paginate,
  });

  int webStatus;
  int code;
  String message;
  dynamic data; // data will be null when login fail
  bool result;
  dynamic paginate;

  factory LoginResponseData.fromJson(Map<String, dynamic> json) =>
      LoginResponseData(
        webStatus: json["web_status"],
        code: json["code"],
        message: json["message"],
        // data will be null when login fail
        data: json["data"] == null ? null : LoginData.fromJson(json["data"]),
        result: json["result"],
        paginate: json["paginate"],
      );

  Map<String, dynamic> toJson() => {
        "web_status": webStatus,
        "code": code,
        "message": message,
        "data": data.toJson() == null ? null : data.toJson(),
        "result": result,
        "paginate": paginate,
      };
}

class LoginData {
  LoginData({
    this.phone,
    this.accessToken,
    this.singleToken,
    this.uuid,
    this.appName,
  });

  String phone;
  String accessToken;
  String singleToken;
  String uuid;
  String appName;

  factory LoginData.fromJson(Map<String, dynamic> json) => LoginData(
        phone: json["phone"],
        accessToken: json["access_token"],
        singleToken: json["single_token"],
        uuid: json["uuid"],
        appName: json["app_name"],
      );

  Map<String, dynamic> toJson() => {
        "phone": phone == null ? "" : phone,
        "access_token": accessToken,
        "single_token": singleToken,
        "uuid": uuid,
        "app_name": appName,
      };
}
