// To parse this JSON data, do
//
//     final matchScheduleSportListResponseData = matchScheduleSportListResponseDataFromJson(jsonString);

import 'dart:convert';

MatchScheduleSportListResponseData matchScheduleSportListResponseDataFromJson(
        String str) =>
    MatchScheduleSportListResponseData.fromJson(json.decode(str));

String matchScheduleSportListResponseDataToJson(
        MatchScheduleSportListResponseData data) =>
    json.encode(data.toJson());

class MatchScheduleSportListResponseData {
  MatchScheduleSportListResponseData({
    this.webStatus,
    this.code,
    this.message,
    this.data,
    this.result,
    this.paginate,
  });

  int webStatus;
  int code;
  String message;
  dynamic data;
  List<Result> result;
  dynamic paginate;

  factory MatchScheduleSportListResponseData.fromJson(
          Map<String, dynamic> json) =>
      MatchScheduleSportListResponseData(
        webStatus: json["web_status"],
        code: json["code"],
        message: json["message"],
        data: json["data"],
        result:
            List<Result>.from(json["result"].map((x) => Result.fromJson(x))),
        paginate: json["paginate"],
      );

  Map<String, dynamic> toJson() => {
        "web_status": webStatus,
        "code": code,
        "message": message,
        "data": data,
        "result": List<dynamic>.from(result.map((x) => x.toJson())),
        "paginate": paginate,
      };
}

class Result {
  Result({
    //this.solarApi,
    this.id,
    this.mid,
    this.lid,
    this.aisportId,
    this.lname,
    this.lnameEn,
    this.hid,
    this.hname,
    this.hnameEn,
    this.aid,
    this.aname,
    this.anameEn,
    this.hicon,
    this.aicon,
    this.time,
    this.endTime,
    this.status,
    this.sport,
    this.hot,
    this.tag,
    this.liveTypesChildId,
    this.videoId,
    this.action,
    this.schId,
    this.liveTypesChildName,
    this.liveTypesName,
    this.sportName,
    this.sportIcon,
    this.coverImgUrl,
    this.coverImgUrlSecond,
    this.schType,
    this.hotTag,
    this.matchDetail,
    this.matchStatus,
    this.roomNum,
    this.liveStatus,
    this.tags,
    this.iframeUrl,
  });

  //SolarApi solarApi;
  int id;
  int mid;
  int lid;
  String aisportId;
  String lname;
  String lnameEn;
  String hid;
  String hname;
  String hnameEn;
  String aid;
  String aname;
  String anameEn;
  String hicon;
  String aicon;
  DateTime time;
  DateTime endTime;
  int status;
  int sport;
  int hot;
  Tag tag;
  int liveTypesChildId;
  dynamic videoId;
  int action;
  int schId;
  dynamic liveTypesChildName;
  dynamic liveTypesName;
  SportName sportName;
  String sportIcon;
  String coverImgUrl;
  String coverImgUrlSecond;
  int schType;
  int hotTag;
  MatchDetail matchDetail;
  MatchStatus matchStatus;
  String roomNum;
  int liveStatus;
  Tags tags;
  String iframeUrl;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        //solarApi: SolarApi.fromJson(json["solarApi"]),
        id: json["id"],
        mid: json["mid"],
        lid: json["lid"],
        aisportId: json["aisportId"],
        lname: json["lname"],
        lnameEn: json["lnameEN"] == null ? null : json["lnameEN"],
        hid: json["hid"] == null ? null : json["hid"],
        hname: json["hname"],
        hnameEn: json["hnameEN"] == null ? null : json["hnameEN"],
        aid: json["aid"] == null ? null : json["aid"],
        aname: json["aname"],
        anameEn: json["anameEN"],
        hicon: json["hicon"],
        aicon: json["aicon"],
        time: DateTime.parse(json["time"]),
        endTime: DateTime.parse(json["end_time"]),
        status: json["status"],
        sport: json["sport"],
        hot: json["hot"],
        tag: tagValues.map[json["tag"]],
        liveTypesChildId: json["live_types_child_id"],
        videoId: json["video_id"],
        action: json["action"],
        schId: json["sch_id"],
        liveTypesChildName: json["live_types_child_name"],
        liveTypesName: json["live_types_name"],
        sportName: sportNameValues.map[json["sportName"]],
        sportIcon: json["sport_icon"],
        coverImgUrl: json["cover_img_url"],
        coverImgUrlSecond: json["cover_img_url_second"],
        schType: json["sch_type"],
        hotTag: json["hot_tag"],
        matchDetail: matchDetailValues.map[json["match_detail"]],
        matchStatus: matchStatusValues.map[json["matchStatus"]],
        roomNum: json["room_num"],
        liveStatus: json["live_status"],
        tags: tagsValues.map[json["tags"]],
        iframeUrl: json["iframe_url"],
      );

  Map<String, dynamic> toJson() => {
        //"solarApi": solarApi.toJson(),
        "id": id,
        "mid": mid,
        "lid": lid,
        "aisportId": aisportId,
        "lname": lname,
        "lnameEN": lnameEn == null ? null : lnameEn,
        "hid": hid == null ? null : hid,
        "hname": hname,
        "hnameEN": hnameEn == null ? null : hnameEn,
        "aid": aid == null ? null : aid,
        "aname": aname,
        "anameEN": anameEn,
        "hicon": hicon,
        "aicon": aicon,
        "time": time.toIso8601String(),
        "end_time": endTime.toIso8601String(),
        "status": status,
        "sport": sport,
        "hot": hot,
        "tag": tagValues.reverse[tag],
        "live_types_child_id": liveTypesChildId,
        "video_id": videoId,
        "action": action,
        "sch_id": schId,
        "live_types_child_name": liveTypesChildName,
        "live_types_name": liveTypesName,
        "sportName": sportNameValues.reverse[sportName],
        "sport_icon": sportIcon,
        "cover_img_url": coverImgUrl,
        "cover_img_url_second": coverImgUrlSecond,
        "sch_type": schType,
        "hot_tag": hotTag,
        "match_detail": matchDetailValues.reverse[matchDetail],
        "matchStatus": matchStatusValues.reverse[matchStatus],
        "room_num": roomNum,
        "live_status": liveStatus,
        "tags": tagsValues.reverse[tags],
        "iframe_url": iframeUrl,
      };
}

enum MatchDetail { Y }

final matchDetailValues = EnumValues({"Y": MatchDetail.Y});

enum MatchStatus { EMPTY, MATCH_STATUS }

final matchStatusValues =
    EnumValues({"直播中": MatchStatus.EMPTY, "预定": MatchStatus.MATCH_STATUS});

class SolarApi {
  SolarApi({
    this.hTotalScore,
    this.aTotalScore,
    this.timeInfo,
    // this.situation,
    this.extradata,
    this.hid,
    this.aid,
    this.hTeamMatchesUrl,
    this.aTeamMatchesUrl,
  });

  int hTotalScore;
  int aTotalScore;
  TimeInfo timeInfo;
  // List<Situation> situation;
  Extradata extradata;
  String hid;
  String aid;
  String hTeamMatchesUrl;
  String aTeamMatchesUrl;

  factory SolarApi.fromJson(Map<String, dynamic> json) => SolarApi(
        hTotalScore: json["hTotalScore"] == null ? null : json["hTotalScore"],
        aTotalScore: json["aTotalScore"] == null ? null : json["aTotalScore"],
        timeInfo: timeInfoValues.map[json["time_info"]],
        // situation: List<Situation>.from(
        //     json["situation"].map((x) => situationValues.map[x])),
        extradata: json["extradata"] == null
            ? null
            : Extradata.fromJson(json["extradata"]),
        hid: json["hid"] == null ? null : json["hid"],
        aid: json["aid"] == null ? null : json["aid"],
        hTeamMatchesUrl:
            json["hTeamMatchesUrl"] == null ? null : json["hTeamMatchesUrl"],
        aTeamMatchesUrl:
            json["aTeamMatchesUrl"] == null ? null : json["aTeamMatchesUrl"],
      );

  Map<String, dynamic> toJson() => {
        "hTotalScore": hTotalScore == null ? null : hTotalScore,
        "aTotalScore": aTotalScore == null ? null : aTotalScore,
        "time_info": timeInfoValues.reverse[timeInfo],
        // "situation": List<dynamic>.from(
        //     situation.map((x) => situationValues.reverse[x])),
        "extradata": extradata == null ? null : extradata.toJson(),
        "hid": hid == null ? null : hid,
        "aid": aid == null ? null : aid,
        "hTeamMatchesUrl": hTeamMatchesUrl == null ? null : hTeamMatchesUrl,
        "aTeamMatchesUrl": aTeamMatchesUrl == null ? null : aTeamMatchesUrl,
      };
}

class Extradata {
  Extradata({
    this.the1,
    this.the2,
    this.the3,
    this.the4,
    this.the5,
    this.the6,
    this.the7,
    this.the8,
    this.the13,
    this.the15,
    this.the16,
    this.the17,
    this.the19,
    this.the21,
    this.the22,
    this.the23,
    this.the24,
    this.the25,
    this.round,
    this.homePosition,
    this.awayPosition,
    this.groupNum,
    this.fats,
    this.fpks,
  });

  List<dynamic> the1;
  List<int> the2;
  List<int> the3;
  List<int> the4;
  List<dynamic> the5;
  List<dynamic> the6;
  List<dynamic> the7;
  List<int> the8;
  List<dynamic> the13;
  List<dynamic> the15;
  List<dynamic> the16;
  List<dynamic> the17;
  List<dynamic> the19;
  List<int> the21;
  List<int> the22;
  List<int> the23;
  List<int> the24;
  List<int> the25;
  String round;
  String homePosition;
  String awayPosition;
  String groupNum;
  List<int> fats;
  List<int> fpks;

  factory Extradata.fromJson(Map<String, dynamic> json) => Extradata(
        the1: List<dynamic>.from(json["1"].map((x) => x)),
        the2: List<int>.from(json["2"].map((x) => x == null ? null : x)),
        the3: List<int>.from(json["3"].map((x) => x == null ? null : x)),
        the4: List<int>.from(json["4"].map((x) => x == null ? null : x)),
        the5: List<dynamic>.from(json["5"].map((x) => x)),
        the6: List<dynamic>.from(json["6"].map((x) => x)),
        the7: List<dynamic>.from(json["7"].map((x) => x)),
        the8: json["8"] != null
            ? List<int>.from(json["8"].map((x) => x == null ? null : x))
            : List<int>(),
        the13: json["13"] != null
            ? List<dynamic>.from(json["13"].map((x) => x))
            : List<dynamic>(),
        the15: List<dynamic>.from(json["15"].map((x) => x)),
        the16: List<dynamic>.from(json["16"].map((x) => x)),
        the17: List<dynamic>.from(json["17"].map((x) => x)),
        the19: List<dynamic>.from(json["19"].map((x) => x)),
        the21: List<int>.from(json["21"].map((x) => x == null ? null : x)),
        the22: List<int>.from(json["22"].map((x) => x == null ? null : x)),
        the23: List<int>.from(json["23"].map((x) => x == null ? null : x)),
        the24: List<int>.from(json["24"].map((x) => x == null ? null : x)),
        the25: List<int>.from(json["25"].map((x) => x == null ? null : x)),
        round: json["round"],
        homePosition: json["home_position"],
        awayPosition: json["away_position"],
        groupNum: json["group_num"],
        fats: List<int>.from(json["fats"].map((x) => x == null ? null : x)),
        fpks: List<int>.from(json["fpks"].map((x) => x == null ? null : x)),
      );

  Map<String, dynamic> toJson() => {
        "1": List<dynamic>.from(the1.map((x) => x)),
        "2": List<dynamic>.from(the2.map((x) => x == null ? null : x)),
        "3": List<dynamic>.from(the3.map((x) => x == null ? null : x)),
        "4": List<dynamic>.from(the4.map((x) => x == null ? null : x)),
        "5": List<dynamic>.from(the5.map((x) => x)),
        "6": List<dynamic>.from(the6.map((x) => x)),
        "7": List<dynamic>.from(the7.map((x) => x)),
        "8": List<dynamic>.from(the8.map((x) => x == null ? null : x)),
        "13": List<dynamic>.from(the13.map((x) => x)),
        "15": List<dynamic>.from(the15.map((x) => x)),
        "16": List<dynamic>.from(the16.map((x) => x)),
        "17": List<dynamic>.from(the17.map((x) => x)),
        "19": List<dynamic>.from(the19.map((x) => x)),
        "21": List<dynamic>.from(the21.map((x) => x == null ? null : x)),
        "22": List<dynamic>.from(the22.map((x) => x == null ? null : x)),
        "23": List<dynamic>.from(the23.map((x) => x == null ? null : x)),
        "24": List<dynamic>.from(the24.map((x) => x == null ? null : x)),
        "25": List<dynamic>.from(the25.map((x) => x == null ? null : x)),
        "round": round,
        "home_position": homePosition,
        "away_position": awayPosition,
        "group_num": groupNum,
        "fats": List<dynamic>.from(fats.map((x) => x == null ? null : x)),
        "fpks": List<dynamic>.from(fpks.map((x) => x == null ? null : x)),
      };
}

enum Situation { THE_00, SITUATION_00, THE_0, SITUATION_0 }

final situationValues = EnumValues({
  "总分 0": Situation.SITUATION_0,
  "角球 0-0": Situation.SITUATION_00,
  "分差 0": Situation.THE_0,
  "半场 0-0": Situation.THE_00
});

enum TimeInfo { THE_1, EMPTY }

final timeInfoValues = EnumValues({"未": TimeInfo.EMPTY, "1'": TimeInfo.THE_1});

enum SportName { EMPTY, SPORT_NAME }

final sportNameValues =
    EnumValues({"足球": SportName.EMPTY, "篮球": SportName.SPORT_NAME});

enum Tag { EMPTY, TAG }

final tagValues = EnumValues({"热门": Tag.EMPTY, "赛事": Tag.TAG});

enum Tags { EMPTY, LS_COVERT }

final tagsValues = EnumValues({"": Tags.EMPTY, "LSCovert": Tags.LS_COVERT});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
