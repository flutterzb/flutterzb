// To parse this JSON data, do
//
//     final myInfoResponseData = myInfoResponseDataFromJson(jsonString);

import 'dart:convert';

MyInfoResponseData myInfoResponseDataFromJson(String str) =>
    MyInfoResponseData.fromJson(json.decode(str));

String myInfoResponseDataToJson(MyInfoResponseData data) =>
    json.encode(data.toJson());

class MyInfoResponseData {
  MyInfoResponseData({
    this.webStatus,
    this.code,
    this.message,
    this.data,
    this.result,
    this.paginate,
  });

  int webStatus;
  int code;
  String message;
  dynamic data;
  MyInfoResult result;
  dynamic paginate;

  factory MyInfoResponseData.fromJson(Map<String, dynamic> json) =>
      MyInfoResponseData(
        webStatus: json["web_status"],
        code: json["code"],
        message: json["message"],
        data: json["data"],
        result: MyInfoResult.fromJson(json["result"]),
        paginate: json["paginate"],
      );

  Map<String, dynamic> toJson() => {
        "web_status": webStatus,
        "code": code,
        "message": message,
        "data": data,
        "result": result.toJson(),
        "paginate": paginate,
      };
}

class MyInfoResult {
  MyInfoResult({
    this.id,
    this.nickname,
    this.phone,
    this.icon,
    this.selfIntro,
    this.mCount,
    this.isStreamer,
    this.phoneUpdatedAt,
    this.focus,
    this.brokenStarAmount,
    this.isChannel,
    this.channelNum,
    this.token,
    this.accessToken,
    this.singleToken,
    this.sessionKey,
    this.unionid,
    this.uuid,
    this.createdAt,
  });

  int id;
  String nickname;
  String phone;
  String icon;
  String selfIntro;
  int mCount;
  bool isStreamer;
  dynamic phoneUpdatedAt;
  int focus;
  int brokenStarAmount;
  String isChannel;
  dynamic channelNum;
  String token;
  String accessToken;
  String singleToken;
  String sessionKey;
  String unionid;
  String uuid;
  DateTime createdAt;

  factory MyInfoResult.fromJson(Map<String, dynamic> json) => MyInfoResult(
        id: json["id"],
        nickname: json["nickname"],
        phone: json["phone"],
        icon: json["icon"],
        selfIntro: json["self_intro"],
        mCount: json["m_count"],
        isStreamer: json["isStreamer"],
        phoneUpdatedAt: json["phone_updated_at"],
        focus: json["focus"],
        brokenStarAmount: json["broken_star_amount"],
        isChannel: json["isChannel"],
        channelNum: json["channel_num"],
        token: json["_token"],
        accessToken: json["access_token"],
        singleToken: json["single_token"],
        sessionKey: json["session_key"],
        unionid: json["unionid"],
        uuid: json["uuid"],
        createdAt: DateTime.parse(json["created_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "nickname": nickname,
        "phone": phone,
        "icon": icon,
        "self_intro": selfIntro,
        "m_count": mCount,
        "isStreamer": isStreamer,
        "phone_updated_at": phoneUpdatedAt,
        "focus": focus,
        "broken_star_amount": brokenStarAmount,
        "isChannel": isChannel,
        "channel_num": channelNum,
        "_token": token,
        "access_token": accessToken,
        "single_token": singleToken,
        "session_key": sessionKey,
        "unionid": unionid,
        "uuid": uuid,
        "created_at": createdAt.toIso8601String(),
      };
}
