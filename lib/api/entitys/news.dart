// To parse this JSON data, do
//
//     final getNewsResponseData = getNewsResponseDataFromJson(jsonString);

import 'dart:convert';

GetNewsResponseData getNewsResponseDataFromJson(String str) =>
    GetNewsResponseData.fromJson(json.decode(str));

String getNewsResponseDataToJson(GetNewsResponseData data) =>
    json.encode(data.toJson());

class GetNewsResponseData {
  GetNewsResponseData({
    this.webStatus,
    this.code,
    this.message,
    this.data,
    this.result,
    this.paginate,
  });

  int webStatus;
  int code;
  String message;
  dynamic data;
  List<Result> result;
  dynamic paginate;

  factory GetNewsResponseData.fromJson(Map<String, dynamic> json) =>
      GetNewsResponseData(
        webStatus: json["web_status"],
        code: json["code"],
        message: json["message"],
        data: json["data"],
        result:
            List<Result>.from(json["result"].map((x) => Result.fromJson(x))),
        paginate: json["paginate"],
      );

  Map<String, dynamic> toJson() => {
        "web_status": webStatus,
        "code": code,
        "message": message,
        "data": data,
        "result": List<dynamic>.from(result.map((x) => x.toJson())),
        "paginate": paginate,
      };
}

class Result {
  Result({
    this.id,
    this.type,
    this.top,
    this.title,
    this.time,
    this.url,
    this.content,
    this.status,
    this.createdAt,
    this.updatedAt,
    this.source,
    this.imageUrl,
    this.sportId,
    this.newId,
    this.labels,
    this.leagueName,
    this.leagueFullName,
    this.leagueNameEn,
    this.leagueFullNameEn,
    this.hotRecommend,
    this.topicTop,
    this.focus,
    this.typesName,
    this.typeEn,
    this.target,
  });

  int id;
  int type;
  int top;
  String title;
  DateTime time;
  String url;
  String content;
  int status;
  DateTime createdAt;
  DateTime updatedAt;
  int source;
  String imageUrl;
  dynamic sportId;
  String newId;
  String labels;
  String leagueName;
  String leagueFullName;
  String leagueNameEn;
  String leagueFullNameEn;
  int hotRecommend;
  int topicTop;
  int focus;
  TypesName typesName;
  TypeEn typeEn;
  Target target;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"],
        type: json["type"],
        top: json["top"],
        title: json["title"],
        time: DateTime.parse(json["time"]),
        url: json["url"],
        content: json["content"],
        status: json["status"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        source: json["source"],
        imageUrl: json["image_url"] == null ? null : json["image_url"],
        sportId: json["sport_id"],
        newId: json["new_id"] == null ? null : json["new_id"],
        labels: json["labels"] == null ? null : json["labels"],
        leagueName: json["league_name"] == null ? null : json["league_name"],
        leagueFullName:
            json["league_full_name"] == null ? null : json["league_full_name"],
        leagueNameEn:
            json["league_name_en"] == null ? null : json["league_name_en"],
        leagueFullNameEn: json["league_full_name_en"] == null
            ? null
            : json["league_full_name_en"],
        hotRecommend:
            json["hot_recommend"] == null ? null : json["hot_recommend"],
        topicTop: json["topic_top"],
        focus: json["focus"],
        typesName: typesNameValues.map[json["types_name"]],
        typeEn: typeEnValues.map[json["type_en"]],
        target: targetValues.map[json["target"]],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "type": type,
        "top": top,
        "title": title,
        "time": time.toIso8601String(),
        "url": url,
        "content": content,
        "status": status,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "source": source,
        "image_url": imageUrl == null ? null : imageUrl,
        "sport_id": sportId,
        "new_id": newId == null ? null : newId,
        "labels": labels == null ? null : labels,
        "league_name": leagueName == null ? null : leagueName,
        "league_full_name": leagueFullName == null ? null : leagueFullName,
        "league_name_en": leagueNameEn == null ? null : leagueNameEn,
        "league_full_name_en":
            leagueFullNameEn == null ? null : leagueFullNameEn,
        "hot_recommend": hotRecommend == null ? null : hotRecommend,
        "topic_top": topicTop,
        "focus": focus,
        "types_name": typesNameValues.reverse[typesName],
        "type_en": typeEnValues.reverse[typeEn],
        "target": targetValues.reverse[target],
      };
}

enum Target { SELF, BLANK }

final targetValues = EnumValues({"blank": Target.BLANK, "self": Target.SELF});

enum TypeEn { ACTIVITY, ANNOUNCEMENT, NEWS }

final typeEnValues = EnumValues({
  "activity": TypeEn.ACTIVITY,
  "announcement": TypeEn.ANNOUNCEMENT,
  "news": TypeEn.NEWS
});

enum TypesName { EMPTY, TYPES_NAME, PURPLE }

final typesNameValues = EnumValues({
  "活动": TypesName.EMPTY,
  "新闻": TypesName.PURPLE,
  "公告": TypesName.TYPES_NAME
});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
