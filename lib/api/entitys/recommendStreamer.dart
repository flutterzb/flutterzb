import 'dart:convert';
import '../entitys/entitys.dart';

class RecommendStreamerRequestData {}

RecommendStreamerResponseData recommendStreamerFromJson(String str) =>
    RecommendStreamerResponseData.fromJson(json.decode(str));

String recommendStreamerToJson(RecommendStreamerResponseData data) =>
    json.encode(data.toJson());

class RecommendStreamerResponseData {
  RecommendStreamerResponseData({
    this.webStatus,
    this.code,
    this.message,
    this.data,
    this.result,
    this.paginate,
  });

  int webStatus;
  int code;
  String message;
  dynamic data;
  List<RecommendStreamerResult> result;
  dynamic paginate;

  factory RecommendStreamerResponseData.fromJson(Map<String, dynamic> json) =>
      RecommendStreamerResponseData(
        webStatus: json["web_status"],
        code: json["code"],
        message: json["message"],
        data: json["data"],
        result: List<RecommendStreamerResult>.from(
            json["result"].map((x) => RecommendStreamerResult.fromJson(x))),
        paginate: json["paginate"],
      );

  Map<String, dynamic> toJson() => {
        "web_status": webStatus,
        "code": code,
        "message": message,
        "data": data,
        "result": List<dynamic>.from(result.map((x) => x.toJson())),
        "paginate": paginate,
      };
}

class RecommendStreamerResult {
  RecommendStreamerResult({
    this.id,
    this.nickname,
    this.icon,
    this.selfIntro,
    this.isChannel,
    this.roomNum,
    this.liveStatus,
    this.channelNum,
    this.title,
    this.channelInfo,
    this.accountTitle,
    this.liveType,
    this.kind,
    this.imageUrl,
    this.total,
    this.visitCount,
    this.rank,
    //this.channel,
  });

  int id;
  String nickname;
  String icon;
  String selfIntro;
  IsChannel isChannel;
  String roomNum;
  int liveStatus;
  String channelNum;
  String title;
  String channelInfo;
  String accountTitle;
  dynamic liveType;
  Kind kind;
  String imageUrl;
  int total;
  int visitCount;
  int rank;
  //Channel channel;

  factory RecommendStreamerResult.fromJson(Map<String, dynamic> json) =>
      RecommendStreamerResult(
        id: json["id"],
        nickname: json["nickname"],
        icon: json["icon"],
        selfIntro: json["self_intro"] == null ? null : json["self_intro"],
        isChannel: isChannelValues.map[json["isChannel"]],
        roomNum: json["room_num"],
        liveStatus: json["live_status"],
        channelNum: json["channel_num"] == null ? null : json["channel_num"],
        title: json["title"],
        channelInfo: json["channel_info"] == null ? null : json["channel_info"],
        accountTitle: json["accountTitle"],
        liveType: json["liveType"],
        kind: kindValues.map[json["kind"]],
        imageUrl: json["image_url"],
        total: json["total"],
        visitCount: json["visitCount"],
        rank: json["rank"],
        //channel: Channel.fromJson(json["channel"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "nickname": nickname,
        "icon": icon,
        "self_intro": selfIntro == null ? null : selfIntro,
        "isChannel": isChannelValues.reverse[isChannel],
        "room_num": roomNum,
        "live_status": liveStatus,
        "channel_num": channelNum == null ? null : channelNum,
        "title": title,
        "channel_info": channelInfo == null ? null : channelInfo,
        "accountTitle": accountTitle,
        "liveType": liveType,
        "kind": kindValues.reverse[kind],
        "image_url": imageUrl,
        "total": total,
        "visitCount": visitCount,
        "rank": rank,
        //"channel": channel.toJson(),
      };
}

// class Channel {
//   Channel({
//     this.id,
//     this.channelNum,
//     this.title,
//     this.banner,
//     this.channelInfo,
//     this.channelStatus,
//     this.createdAt,
//     this.updatedAt,
//   });

//   int id;
//   String channelNum;
//   String title;
//   String banner;
//   String channelInfo;
//   int channelStatus;
//   DateTime createdAt;
//   DateTime updatedAt;

//   factory Channel.fromJson(Map<String, dynamic> json) => Channel(
//         id: json["id"] == null ? null : json["id"],
//         channelNum: json["channel_num"] == null ? null : json["channel_num"],
//         title: json["title"] == null ? null : json["title"],
//         banner: json["banner"] == null ? null : json["banner"],
//         channelInfo: json["channel_info"] == null ? null : json["channel_info"],
//         channelStatus:
//             json["channel_status"] == null ? null : json["channel_status"],
//         createdAt: json["created_at"] == null
//             ? null
//             : DateTime.parse(json["created_at"]),
//         updatedAt: json["updated_at"] == null
//             ? null
//             : DateTime.parse(json["updated_at"]),
//       );

//   Map<String, dynamic> toJson() => {
//         "id": id == null ? null : id,
//         "channel_num": channelNum == null ? null : channelNum,
//         "title": title == null ? null : title,
//         "banner": banner == null ? null : banner,
//         "channel_info": channelInfo == null ? null : channelInfo,
//         "channel_status": channelStatus == null ? null : channelStatus,
//         "created_at": createdAt == null ? null : createdAt.toIso8601String(),
//         "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
//       };
// }

enum IsChannel { Y, N }

final isChannelValues = EnumValues({"N": IsChannel.N, "Y": IsChannel.Y});

enum Kind { LIVE }

final kindValues = EnumValues({"live": Kind.LIVE});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
