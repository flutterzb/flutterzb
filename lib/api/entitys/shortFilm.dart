// To parse this JSON data, do
//
//     final shortFilmResponseData = shortFilmResponseDataFromJson(jsonString);

import 'dart:convert';

ShortFilmResponseData shortFilmResponseDataFromJson(String str) =>
    ShortFilmResponseData.fromJson(json.decode(str));

String shortFilmResponseDataToJson(ShortFilmResponseData data) =>
    json.encode(data.toJson());

class ShortFilmResponseData {
  ShortFilmResponseData({
    this.webStatus,
    this.code,
    this.message,
    this.data,
    this.result,
    this.paginate,
  });

  int webStatus;
  int code;
  String message;
  dynamic data;
  List<Result> result;
  dynamic paginate;

  factory ShortFilmResponseData.fromJson(Map<String, dynamic> json) =>
      ShortFilmResponseData(
        webStatus: json["web_status"],
        code: json["code"],
        message: json["message"],
        data: json["data"],
        result:
            List<Result>.from(json["result"].map((x) => Result.fromJson(x))),
        paginate: json["paginate"],
      );

  Map<String, dynamic> toJson() => {
        "web_status": webStatus,
        "code": code,
        "message": message,
        "data": data,
        "result": List<dynamic>.from(result.map((x) => x.toJson())),
        "paginate": paginate,
      };
}

class Result {
  Result({
    this.id,
    this.updatedAt,
    this.name,
    this.info,
    this.accountsId,
    this.publishSetting,
    this.createdAt,
    this.endAt,
    this.liveTypesId,
    this.urlFhd,
    this.urlHd,
    this.urlSd,
    this.thumbnailUrl,
    this.countInit,
    this.visitCountTotal,
    this.focusCount,
    this.startAt,
    this.types,
    this.hotSort,
    this.isChannel,
    this.nickname,
    this.icon,
    this.total,
    this.liveType,
    this.visitCount,
    this.kind,
    this.likeCount,
    this.isLike,
    this.isLiveing,
    this.linkUrl,
    this.channelUrl,
  });

  int id;
  DateTime updatedAt;
  String name;
  Info info;
  int accountsId;
  int publishSetting;
  DateTime createdAt;
  DateTime endAt;
  int liveTypesId;
  String urlFhd;
  String urlHd;
  String urlSd;
  String thumbnailUrl;
  int countInit;
  int visitCountTotal;
  int focusCount;
  DateTime startAt;
  int types;
  int hotSort;
  bool isChannel;
  Nickname nickname;
  String icon;
  int total;
  String liveType;
  int visitCount;
  Kind kind;
  int likeCount;
  bool isLike;
  bool isLiveing;
  String linkUrl;
  ChannelUrl channelUrl;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"],
        updatedAt: DateTime.parse(json["updated_at"]),
        name: json["name"],
        info: infoValues.map[json["info"]],
        accountsId: json["accounts_id"],
        publishSetting: json["publish_setting"],
        createdAt: DateTime.parse(json["created_at"]),
        endAt: DateTime.parse(json["end_at"]),
        liveTypesId: json["live_types_id"],
        urlFhd: json["url_fhd"],
        urlHd: json["url_hd"],
        urlSd: json["url_sd"],
        thumbnailUrl: json["thumbnail_url"],
        countInit: json["count_init"],
        visitCountTotal: json["visit_count_total"],
        focusCount: json["focus_count"],
        startAt: DateTime.parse(json["start_at"]),
        types: json["types"],
        hotSort: json["hot_sort"],
        isChannel: json["isChannel"],
        nickname: nicknameValues.map[json["nickname"]],
        icon: json["icon"],
        total: json["total"],
        liveType: json["liveType"],
        visitCount: json["visitCount"],
        kind: kindValues.map[json["kind"]],
        likeCount: json["likeCount"],
        isLike: json["isLike"],
        isLiveing: json["isLiveing"],
        linkUrl: json["linkUrl"],
        channelUrl: channelUrlValues.map[json["channelUrl"]],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "updated_at": updatedAt.toIso8601String(),
        "name": name,
        "info": infoValues.reverse[info],
        "accounts_id": accountsId,
        "publish_setting": publishSetting,
        "created_at": createdAt.toIso8601String(),
        "end_at": endAt.toIso8601String(),
        "live_types_id": liveTypesId,
        "url_fhd": urlFhd,
        "url_hd": urlHd,
        "url_sd": urlSd,
        "thumbnail_url": thumbnailUrl,
        "count_init": countInit,
        "visit_count_total": visitCountTotal,
        "focus_count": focusCount,
        "start_at": startAt.toIso8601String(),
        "types": types,
        "hot_sort": hotSort,
        "isChannel": isChannel,
        "nickname": nicknameValues.reverse[nickname],
        "icon": icon,
        "total": total,
        "liveType": liveType,
        "visitCount": visitCount,
        "kind": kindValues.reverse[kind],
        "likeCount": likeCount,
        "isLike": isLike,
        "isLiveing": isLiveing,
        "linkUrl": linkUrl,
        "channelUrl": channelUrlValues.reverse[channelUrl],
      };
}

enum ChannelUrl { CHANNEL_5 }

final channelUrlValues = EnumValues({"/channel/5": ChannelUrl.CHANNEL_5});

enum Info { EMPTY }

final infoValues = EnumValues({"小视频资讯": Info.EMPTY});

enum Kind { VIDEO }

final kindValues = EnumValues({"video": Kind.VIDEO});

enum Nickname { SMZB_CN }

final nicknameValues = EnumValues({"山猫直播smzb.cn": Nickname.SMZB_CN});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
