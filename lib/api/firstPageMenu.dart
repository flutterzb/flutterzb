import '../utils/utils.dart';
import 'entitys/firstPageMenu.dart';

// FirstPageMenu
class FirstPageMenu {
  static Future<FirstPageMenuResponseData> getFirstPageMenu() async {
    var response = await HttpUtil().get('/api/nav/menu');
    return FirstPageMenuResponseData.fromJson(response);
  }
}
