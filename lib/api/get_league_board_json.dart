import 'dart:convert';
import '../utils/utils.dart';
import 'entitys/league_board.dart';
import 'entitys/league_board_path.dart';
import 'dart:async';
import 'package:dio/dio.dart';

// HotTop
class LeagueBoard {
  static Future<LeagueBoardResponesData> getLeagueBoardPath() async {
    var response = await HttpUtil().get('/get_League_Board_Json');
    return getLeagueBoardData(
        "https:" + LeagueBoardPathResponesData.fromJson(response).data);
  }

  static Future<LeagueBoardResponesData> getLeagueBoardData(String url) async {
    var response = await HttpUtil().get(
      url,
      options: Options(
        //https://blog.csdn.net/GuanZhong12345/article/details/105701629
        //因為回來的不是JSON 必須先這樣帶
        responseType: ResponseType.plain,
      ),
    );
    //把gameStatus( 和結尾) 暴力洗掉再轉回去
    response = response.replaceAll("gameStatus(", "");
    response = response.replaceAll("})", "}");
    Map responseMap = jsonDecode(response);
    return LeagueBoardResponesData.fromJson(responseMap);
  }
}
