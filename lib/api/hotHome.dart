import 'package:flutter_zb/utils/http.dart';
import 'package:flutter_zb/api/entitys/hotHome.dart';

// HecommendStreamer
class HotHome {
  static Future<HotHomeResponseData> getHotHome() async {
    var response = await HttpUtil().get('/hot/home');
    return HotHomeResponseData.fromJson(response);
  }
}
