import '../api/entitys/entitys.dart';
import '../utils/utils.dart';

// HotTop
class HotTop {
  static Future<HotTopResponseData> getHotTop() async {
    var response = await HttpUtil().get('/hot/top');
    return HotTopResponseData.fromJson(response);
  }
}
