import 'package:flutter_zb/api/entitys/liveRoom.dart';
import 'package:flutter_zb/utils/http.dart';
import '../utils/utils.dart';

/// HecommendStreamer
class LiveRoom {
  static Future<LiveRoomInfo> getLiveRoom(String roomNum,
      {String mid = ""}) async {
    if (mid == null || mid == "") {
      var response = await HttpUtil().get('/live_room/?room_num=$roomNum');
      return LiveRoomResponseData.fromJson(response).result.room;
    } else {
      var response =
          await HttpUtil().get('/live_room/?room_num=$roomNum&mid=$mid');
      return LiveRoomResponseData.fromJson(response).result.room;
    }
  }
}
