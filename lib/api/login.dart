import 'package:flutter_zb/utils/http.dart';
import '../utils/utils.dart';
import 'entitys/login.dart';

//Login
class Login {
  static Future<LoginResponseData> postLogin(
      String phone, String password) async {
    var loginParams = {};
    loginParams["phone"] = phone;
    loginParams["password"] = password;
    loginParams["model"] = "flutter";
    //loginParams["manufacturer"] = "deviceModel"; //iphone Android
    var response =
        await HttpUtil().post('/api/auth/login', params: loginParams);
    return LoginResponseData.fromJson(response);
  }
}
