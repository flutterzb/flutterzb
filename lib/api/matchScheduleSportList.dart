import 'package:flutter_zb/api/entitys/matchScheduleSportList.dart';
import 'package:flutter_zb/utils/http.dart';
import '../utils/utils.dart';

/// HecommendStreamer
class MatchScheduleSportList {
  static Future<MatchScheduleSportListResponseData>
      getMatchScheduleSportList() async {
    var response = await HttpUtil().get('/matchScheduleSportList');
    return MatchScheduleSportListResponseData.fromJson(response);
  }
}
