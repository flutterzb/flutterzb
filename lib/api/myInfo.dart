import 'package:flutter_zb/api/entitys/myInfo.dart';
import 'package:flutter_zb/utils/http.dart';
import '../utils/utils.dart';

//MyInfo
class MyInfo {
  static Future<MyInfoResponseData> getMyInfo() async {
    var response = await HttpUtil().get('/myInfo');
    return MyInfoResponseData.fromJson(response);
  }
}
