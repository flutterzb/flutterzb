import 'package:flutter_zb/api/entitys/news.dart';
import 'package:flutter_zb/utils/http.dart';
import '../utils/utils.dart';

/// HecommendStreamer
class News {
  static Future<GetNewsResponseData> getNews() async {
    var response = await HttpUtil().get('/getNews');
    return GetNewsResponseData.fromJson(response);
  }
}
