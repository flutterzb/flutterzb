import '../api/entitys/entitys.dart';
import '../utils/utils.dart';

/// HecommendStreamer
class RecommendStreamer {
  static Future<RecommendStreamerResponseData> getHecommendStreamer() async {
    var response = await HttpUtil().get('/hot/recommend');
    return RecommendStreamerResponseData.fromJson(response);
  }
}
