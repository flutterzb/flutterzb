import 'package:flutter_zb/utils/http.dart';
import 'entitys/shortFilm.dart';

// ShortFilm 小視頻
class ShortFilm {
  static Future<ShortFilmResponseData> getShortFilmCarousel() async {
    var response = await HttpUtil().get('/short_film_carousel');
    return ShortFilmResponseData.fromJson(response);
  }
}
