import 'package:flutter/material.dart';
import 'package:flutter_zb/api/hotHome.dart';

class HotHomeBloc extends InheritedWidget {
  HotHomeBloc({Key key, this.child}) : super(key: key, child: child);

  final Widget child;
  final HotHome hotHome = HotHome();

  static HotHomeBloc of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<HotHomeBloc>();
  }

  @override
  bool updateShouldNotify(HotHomeBloc oldWidget) {
    return true;
  }
}
