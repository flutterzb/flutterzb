import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_zb/api/entitys/hotHome.dart';
import 'package:flutter_zb/screens/liveRoom/LiveStreamRoom.dart';

import '../screens/liveRoom/LiveStreamRoom.dart';
import '../screens/views/RecommendCard.dart';

class SixBlockView extends StatelessWidget {
  const SixBlockView({Key key, this.imageStr, this.rooms}) : super(key: key);

  final String imageStr;
  final List<Room> rooms;
  final double itemHeight = 125;
  final double itemWidth = 165;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(0, 5, 0, 5),
      color: Colors.white,
      child: getSliverGridWidget(context),
      //getGridWidget(context),
    );
  }

  // getGridWidget(BuildContext context) {
  //   return GridView.count(
  //     childAspectRatio: (itemWidth / itemHeight),
  //     primary: false,
  //     shrinkWrap: false,
  //     padding: EdgeInsets.all(8), // 内边距
  //     scrollDirection: Axis.vertical, // 滚动方向
  //     crossAxisSpacing: 10, // 列间距
  //     crossAxisCount: 2, // 每行的个数（Axis.vertic == 横向三个, Axis.horizontal == 竖方向三个）
  //     mainAxisSpacing: 10,
  //     //dragStartBehavior: DragStartBehavior.start,
  //     children: recommendCardWidgetList(context),
  //     //   <Widget>[
  //     // Container(
  //     //   width: 30,
  //     //   height: 30,
  //     //   color: Colors.lightBlue,
  //     // ),
  //     // Container(
  //     //   width: 30,
  //     //   height: 30,
  //     //   color: Colors.lightGreen,
  //     // ),
  //     // Container(
  //     //   width: 30,
  //     //   height: 30,
  //     //   color: Colors.red,
  //     // ),
  //     // Container(
  //     //   width: 30,
  //     //   height: 30,
  //     //   color: Colors.orange,
  //     // ),
  //   );
  // }

  // List<Widget> recommendCardWidgetList(BuildContext context) {
  //   List<Widget> array = [];
  //   for (final room in rooms) {
  //     if (array.length < 6) {
  //       final isLive = room.liveStatus == 1 ? true : false;
  //       final imageStr =
  //           room.imageUrl == null ? room.thumbnailUrl : room.imageUrl;
  //       final info = room.title == null ? room.name : room.title;
  //       array.add(RecommendCard(
  //         isLive: isLive,
  //         imageStr: imageStr,
  //         userImageStr: room.icon,
  //         visitCount: room.visitCount.toString(),
  //         accountTitle: room.accountTitle,
  //         userName: room.nickname,
  //         info: info,
  //         press: () {
  //           //Navigator.pushNamed(context, LiveStreamRoom.routeName);
  //           Navigator.push(context,
  //               MaterialPageRoute(builder: (BuildContext context) {
  //             return LiveStreamRoom(
  //               roomNum: room.roomNum,
  //               heroImage: room.icon,
  //             );
  //           }));
  //         },
  //       ));
  //     } else {
  //       break;
  //     }
  //   }
  //   //print(array.length);
  //   return array;
  // }

  Widget getSliverGridWidget(BuildContext context) {
    return CustomScrollView(
      //primary: false,
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      slivers: <Widget>[
        SliverGrid(
          //SliverGrid的效能比GridView.count好(應該吧)
          delegate: SliverChildBuilderDelegate(
            (BuildContext context, int index) {
              return recommendCardWidget(context, rooms[index]);
            },
            childCount: 6,
          ),
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              childAspectRatio: (itemWidth / itemHeight),
              crossAxisCount: 2,
              mainAxisSpacing: 10.0,
              crossAxisSpacing: 0),
        ),
      ],
    );
  }

  Widget recommendCardWidget(BuildContext context, Room room) {
    final isLive = room.liveStatus == 1 ? true : false;
    final imageStr = room.imageUrl == null ? room.thumbnailUrl : room.imageUrl;
    final info = room.title == null ? room.name : room.title;
    return Padding(
      padding: EdgeInsets.fromLTRB(6, 0, 6, 0),
      child:
          //Hero(
          //tag: room.roomNum ?? "",
          //child:
          RecommendCard(
        isLive: isLive,
        imageStr: imageStr,
        userImageStr: room.icon,
        visitCount: room.visitCount.toString(),
        accountTitle: room.accountTitle,
        userName: room.nickname,
        info: info,
        press: () {
          if (room.roomNum == null) {
            print("非直播間");
          } else {
            //Navigator.pushNamed(context, LiveStreamRoom.routeName);
            Navigator.push(context,
                MaterialPageRoute(builder: (BuildContext context) {
              return LiveStreamRoom(
                roomNum: room.roomNum,
                heroImage: room.icon,
              );
            }));
          }
        },
      ),
      //),
    );
  }
}
