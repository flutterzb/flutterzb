import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_zb/api/entitys/hotTop.dart';
import 'package:flutter_zb/screens/liveRoom/LiveStreamRoom.dart';
import '../imageCache.dart';

class HotTopCarouselSlider extends StatelessWidget {
  HotTopCarouselSlider({
    Key key,
    this.hotTop,
    this.didSelectedLiveRoom,
  }) : super(key: key);

  final HotTopResponseData hotTop;
  Function(HotTopResult) didSelectedLiveRoom;

  @override
  Widget build(BuildContext context) {
    return CarouselSlider(
      options: CarouselOptions(
        height: MediaQuery.of(context).size.width * 0.86 * 9 / 16,
        aspectRatio: 16 / 9,
        enableInfiniteScroll: true,
        autoPlay: false,
        autoPlayInterval: Duration(seconds: 5),
        scrollDirection: Axis.horizontal,
      ),
      items: hotTop.result.map((hotTopResult) {
        return Builder(
          builder: (BuildContext context) {
            return
                // Hero(
                //   tag: hotTopResult.roomNum,
                //   child:
                Container(
              //width: MediaQuery.of(context).size.width * 0.86,
              margin: EdgeInsets.symmetric(horizontal: 5.0),
              decoration: BoxDecoration(color: Colors.transparent),
              child: GestureDetector(
                onTap: () {
                  print("hotTopTapped");
                  //懸浮窗 not yet done
                  //didSelectedLiveRoom(hotTopResult);
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (BuildContext context) {
                      return LiveStreamRoom(
                        roomNum: hotTopResult.roomNum,
                        mid: hotTopResult.mid.toString(),
                        heroImage: hotTopResult.icon,
                      );
                    }),
                  ).then(
                    (needShowMiniVLC) {
                      print("Navigator callBackValue");
                      if (needShowMiniVLC) {
                        didSelectedLiveRoom(hotTopResult);
                      }
                    },
                  );
                },
                child: //ClipPath()
                    ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: ImageCached(
                    imageUrl: hotTopResult.imageUrl,
                    placeholder: ImageType.apiErrorImage,
                  ),
                ),
              ),
            );
            // );
          },
        );
      }).toList(),
    );
  }
}
