import 'package:flutter/material.dart';
import 'package:flutter_zb/api/entitys/matchScheduleSportList.dart';
import 'package:flutter_zb/screens/views/matchScheduleSportListCard.dart';

class MatchScheduleSport extends StatelessWidget {
  const MatchScheduleSport({
    Key key,
    this.matchScheduleSportList,
  }) : super(key: key);

  final MatchScheduleSportListResponseData matchScheduleSportList;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SizedBox(
      height: size.width * 0.7 * (160 / 256) + 10,
      child: CustomScrollView(
        //primary: false,
        shrinkWrap: true,
        //physics: NeverScrollableScrollPhysics(),
        scrollDirection: Axis.horizontal,
        slivers: <Widget>[
          SliverList(
            delegate: SliverChildBuilderDelegate(
              (BuildContext context, int index) {
                return sportListWidget()[index];
              },
              childCount: sportListWidget().length,
            ),
          ),
        ],
      ),
    );

    // return SingleChildScrollView(
    //   scrollDirection: Axis.horizontal,
    //   child: Row(
    //     children: sportListWidget(),
    //   ),
    // );
  }

  List<Widget> sportListWidget() {
    List<Widget> array = [];
    for (var item in this.matchScheduleSportList.result) {
      array.add(MatchScheduleSportListCard(
        result: item,
      ));
    }
    return array;
  }
}
