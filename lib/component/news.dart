import 'package:flutter/material.dart';
import 'package:flutter_zb/api/entitys/news.dart';
import 'package:url_launcher/url_launcher.dart';
import '../constants.dart';

class NewsView extends StatelessWidget {
  const NewsView({
    Key key,
    this.newsData,
  }) : super(key: key);
  final GetNewsResponseData newsData;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Divider(
          height: 13,
          thickness: 2,
          indent: 20,
          endIndent: 20,
        ),
        Container(
          alignment: Alignment.center,
          color: Colors.white70,
          //width: size.width,
          height: 4.0 * 40,
          child: Column(
            children: newsData.result
                .getRange(0, 4)
                .map(
                  (item) => Container(
                    margin: EdgeInsets.all(5),
                    child: GestureDetector(
                      onTap: () async {
                        if (await canLaunch(item.url)) {
                          await launch(
                            item.url,
                            forceWebView: false,
                            forceSafariVC: false,
                          );
                        } else {
                          print("");
                          throw 'Could not launch ${item.url}';
                        }
                      },
                      child: NewsCard(title: item.title),
                    ),
                  ),
                )
                .toList(),
          ),
        ),
        Divider(
          height: 13,
          thickness: 2,
          indent: 20,
          endIndent: 20,
        ),
      ],
    );
  }
}

class NewsCard extends StatelessWidget {
  const NewsCard({
    Key key,
    this.title,
  }) : super(key: key);
  final String title;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(30, 0, 30, 0),
      //width: size.width * 0.7,
      height: 30,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(
          Radius.circular(5),
        ),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 2,
            blurRadius: 2,
            offset: Offset(2, 3), // changes position of shadow
          ),
        ],
      ),
      child: Container(
          //alignment: Alignment.centerLeft,
          margin: EdgeInsets.symmetric(horizontal: 5),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Image.asset(
                "assets/icons/annoucement.png",
                width: 18,
                height: 18,
              ),
              Expanded(
                child: Text(
                  title,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 1,
                  style: TextStyle(
                    fontSize: 13,
                  ),
                ),
              ),
            ],
          )),
    );
  }
}
