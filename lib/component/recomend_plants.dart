import 'package:flutter/material.dart';
import 'package:flutter_zb/api/entitys/hotTop.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../screens/views/StreamCard.dart';
import '../constants.dart';

class RecomendsPlants extends StatelessWidget {
  const RecomendsPlants({
    Key key,
    this.imageStr,
    this.results,
  }) : super(key: key);

  final String imageStr;
  final List<HotTopResult> results;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        children: streamCardWidget(),
      ),
    );
  }

  List<Widget> streamCardWidget() {
    List<Widget> array = [];
    for (var i in this.results) {
      array.add(StreamCard(
        imageStr: i.imageUrl,
        press: () {
          Fluttertoast.showToast(
              msg: "This is Center Short Toast",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              backgroundColor: Colors.black87,
              textColor: Colors.white,
              fontSize: 16.0);
        },
      ));
    }
    return array;
  }
}

// class RecomendsPlants extends StatefulWidget {
//   RecomendsPlants({Key key, this.imageStr}) : super(key: key);
//   final String imageStr;

//   @override
//   _RecomendsPlantsState createState() => _RecomendsPlantsState();
// }

// class _RecomendsPlantsState extends State<RecomendsPlants> {
//   @override
//   Widget build(BuildContext context) {
//     //hopTop();
//     return SingleChildScrollView(
//       child: Row(
//         children: <Widget>[
//           StreamCard(
//             imageStr: '',
//             press: () {
//               Fluttertoast.showToast(
//                   msg: "This is Center Short Toast",
//                   toastLength: Toast.LENGTH_SHORT,
//                   gravity: ToastGravity.CENTER,
//                   timeInSecForIos: 1,
//                   backgroundColor: Colors.black87,
//                   textColor: Colors.white,
//                   fontSize: 16.0);
//             },
//           ),
//           // UserCard(
//           //   image: "assets/images/bottom_img_2.png",
//           // ),
//           // StreamCard(
//           //   image: "assets/images/apiErrorPlaceholder.jpg",
//           //   press: () {},
//           // ),
//           // UserCard(
//           //   image: "assets/images/bottom_img_2.png",
//           // ),
//           // StreamCard(
//           //   image: "assets/images/apiErrorPlaceholder.jpg",
//           //   press: () {},
//           // ),

//           // UserCard(
//           //   image: "assets/images/bottom_img_2.png",
//           // ),

//           // RecomendPlantCard(
//           //   image: "assets/images/image_1.png",
//           //   title: "Samantha",
//           //   country: "Russia",
//           //   price: 440,
//           //   press: () {
//           //     // Navigator.push(
//           //     //   context,
//           //     //   MaterialPageRoute(
//           //     //     builder: (context) => DetailsScreen(),
//           //     //   ),
//           //     // );
//           //   },
//           // ),
//         ],
//       ),
//     );
//   }
// }

class RecomendPlantCard extends StatelessWidget {
  const RecomendPlantCard({
    Key key,
    this.image,
    this.title,
    this.country,
    this.price,
    this.press,
  }) : super(key: key);

  final String image, title, country;
  final int price;
  final Function press;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.only(
        left: kDefaultPadding,
        top: kDefaultPadding / 2,
        bottom: kDefaultPadding * 2.5,
      ),
      width: size.width * 0.4,
      child: Column(
        children: <Widget>[
          Image.asset(image),
          GestureDetector(
            onTap: press,
            child: Container(
              //padding: EdgeInsets.all(kDefaultPadding / 2),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(10),
                  bottomRight: Radius.circular(10),
                ),
                boxShadow: [
                  BoxShadow(
                    offset: Offset(0, 10),
                    blurRadius: 50,
                    color: kPrimaryColor.withOpacity(0.23),
                  ),
                ],
              ),
              child: Row(
                children: <Widget>[
                  RichText(
                    text: TextSpan(
                      children: [
                        TextSpan(
                            text: "$title\n".toUpperCase(),
                            style: Theme.of(context).textTheme.button),
                        TextSpan(
                          text: "$country".toUpperCase(),
                          style: TextStyle(
                            color: kPrimaryColor.withOpacity(0.5),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Spacer(),
                  Text(
                    '\$$price',
                    style: Theme.of(context)
                        .textTheme
                        .button
                        .copyWith(color: kPrimaryColor),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
