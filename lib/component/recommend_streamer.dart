import 'package:flutter/material.dart';
import 'package:flutter_zb/api/entitys/entitys.dart';

import '../screens/views/UserCard.dart';
import '../screens/liveRoom/LiveStreamRoom.dart';

class RecommendStreamerScrollView extends StatelessWidget {
  const RecommendStreamerScrollView({
    Key key,
    this.imageStr,
    this.results,
  }) : super(key: key);

  final String imageStr;
  final List<RecommendStreamerResult> results;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 132,
      child: CustomScrollView(
        //primary: false,
        shrinkWrap: true,
        //physics: NeverScrollableScrollPhysics(),
        scrollDirection: Axis.horizontal,
        slivers: <Widget>[
          SliverList(
            delegate: SliverChildBuilderDelegate(
              (BuildContext context, int index) {
                return streamCardWidget(context)[index];
              },
              childCount: streamCardWidget(context).length,
            ),
          ),
        ],
      ),
    );

    // return SingleChildScrollView(
    //   scrollDirection: Axis.horizontal,
    //   child: Row(
    //     children: streamCardWidget(context),
    //   ),
    // );
  }

  List<Widget> streamCardWidget(BuildContext context) {
    List<Widget> array = [];
    // for (var item in this.results) {
    //   array.add(
    //     Hero(
    //       tag: item.id,
    //       child: UserCard(
    //         isLive: item.liveStatus == 1 ? true : false,
    //         imageStr: item.icon,
    //         userName: item.nickname,
    //         press: () {
    //           Navigator.push(context,
    //               MaterialPageRoute(builder: (BuildContext context) {
    //             return LiveStreamRoom(roomNum: item.roomNum);
    //           }));

    //           Fluttertoast.showToast(
    //               msg: item.nickname,
    //               toastLength: Toast.LENGTH_SHORT,
    //               gravity: ToastGravity.CENTER,
    //               timeInSecForIos: 1,
    //               backgroundColor: Colors.black87,
    //               textColor: Colors.white,
    //               fontSize: 16.0);
    //         },
    //       ),
    //     ),
    //   );
    // }
    this.results.asMap().forEach((index, item) {
      array.add(
        Hero(
          tag: item.roomNum,
          child: UserCard(
            isLive: item.liveStatus == 1 ? true : false,
            imageStr: item.icon,
            userName: item.nickname,
            press: () async {
              await new Future.delayed(const Duration(milliseconds: 500));
              Navigator.push(context,
                  MaterialPageRoute(builder: (BuildContext context) {
                return LiveStreamRoom(
                  roomNum: item.roomNum,
                  heroImage: item.icon,
                );
              }));

              // Fluttertoast.showToast(
              //     msg: item.nickname,
              //     toastLength: Toast.LENGTH_SHORT,
              //     gravity: ToastGravity.CENTER,
              //     timeInSecForIos: 1,
              //     backgroundColor: Colors.black87,
              //     textColor: Colors.white,
              //     fontSize: 16.0);
            },
          ),
        ),
      );
    });
    return array;
  }
}
