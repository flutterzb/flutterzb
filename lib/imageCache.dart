import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

enum ImageType {
  userImage,
  apiErrorImage,
  soccer,
  basketBall,
}

class ImageCached extends StatelessWidget {
  const ImageCached({
    Key key,
    this.imageUrl,
    this.placeholder,
  }) : super(key: key);

  final String imageUrl;
  final ImageType placeholder;

  @override
  Widget build(BuildContext context) {
    final placeholderTypeEnum = EnumValues({
      "assets/images/apiErrorPlaceholder.jpg": ImageType.apiErrorImage,
      "assets/images/userImage.png": ImageType.userImage,
      "assets/images/soccerDefault.png": ImageType.soccer,
      "assets/images/basketballDefault.png": ImageType.basketBall,
    });
    final placeholderPath = placeholderTypeEnum.reverse[placeholder].toString();

    var imagUrlPrefix = "";
    if (!imageUrl.contains("http"))
      imagUrlPrefix = "https:" + imageUrl;
    else
      imagUrlPrefix = imageUrl;
    return CachedNetworkImage(
      placeholder: (context, url) => Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            fit: BoxFit.fill,
            image: AssetImage(placeholderPath),
          ),
        ),
      ),
      imageUrl: imagUrlPrefix,
      imageBuilder: (context, imageProvider) => Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: imageProvider,
            fit: BoxFit.fill,
          ),
        ),
      ),
      errorWidget: (context, url, error) {
        return Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              fit: BoxFit.fill,
              image: AssetImage(placeholderPath),
            ),
          ),
        );
      },
    );
  }
}

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
