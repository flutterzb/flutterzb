import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class LoadingLottieView extends StatelessWidget {
  const LoadingLottieView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height * 0.28,
      width: MediaQuery.of(context).size.height * 0.28,
      child: FractionallySizedBox(
        heightFactor: 0.9,
        widthFactor: 0.9,
        child: Lottie.asset(
          'assets/lottie/loadingrepeatWhite.json',
          fit: BoxFit.fitHeight,
          animate: true,
        ),
      ),
    );
  }
}
