import 'package:flutter/material.dart';
import 'package:flutter_zb/theme.dart';
import 'package:flutter_zb/utils/AuthManager.dart';
import 'package:flutter_zb/utils/DataManager.dart';
import 'package:flutter_zb/utils/UserDefault.dart';
import 'bloc/hot_home_bloc.dart';
import 'screens/firstPage/FirstPage.dart';
import 'routes.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await UserDefault().init();
  // AuthManager();
  // DataManager();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    return HotHomeBloc(
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: theme(),
        // ThemeData(
        //   primarySwatch: white,
        //   //Colors.blue,
        //   // This makes the visual density adapt to the platform that you run
        //   // the app on. For desktop platforms, the controls will be smaller and
        //   // closer together (more dense) than on mobile platforms.
        //   visualDensity: VisualDensity.adaptivePlatformDensity,
        // ),
        home: FirstPage(),
        initialRoute: FirstPage.routeName,
        routes: routes,
        debugShowCheckedModeBanner: false,
        //showPerformanceOverlay: true,
        //showSemanticsDebugger: true,
      ),
    );
  }
}
