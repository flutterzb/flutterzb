import 'package:flutter/widgets.dart';
import 'package:flutter_zb/screens/firstPage/FirstPage.dart';
import 'package:flutter_zb/screens/liveRoom/LiveStreamRoom.dart';

// We use name route
// All our routes will be available here
final Map<String, WidgetBuilder> routes = {
  FirstPage.routeName: (context) => FirstPage(),
  LiveStreamRoom.routeName: (context) => LiveStreamRoom(),
};
