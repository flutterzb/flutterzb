import 'package:flutter/material.dart';
import 'package:flutter_zb/api/attentionRecommendLive.dart';
import 'package:flutter_zb/api/entitys/attentionRecommendLive.dart';
import 'package:flutter_zb/imageCache.dart';
import 'package:flutter_zb/loadingLottie.dart';
import 'package:flutter_zb/screens/login/loginPage.dart';
import 'package:flutter_zb/utils/AuthManager.dart';

class AttentionLivePage extends StatefulWidget {
  AttentionLivePage({Key key}) : super(key: key);
  AttentionRecommendLiveResponseData recommendLiveData;

  @override
  _AttentionLivePageState createState() => _AttentionLivePageState();
}

class _AttentionLivePageState extends State<AttentionLivePage> {
  bool isLoading = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (isLoading) {
      apiSetup();
      return Center(
        child: LoadingLottieView(),
      );
    } else {
      return Expanded(
        child: CustomScrollView(
          shrinkWrap: true,
          scrollDirection: Axis.vertical,
          slivers: <Widget>[
            SliverList(
              delegate: SliverChildBuilderDelegate(
                (BuildContext context, int index) {
                  return createList()[index];
                },
                childCount: createList().length,
              ),
            ),
          ],
        ),
      );
    }
  }

  List<Widget> createList() {
    return <Widget>[
      recommendRow(),
      needLoginRow(),
    ];
  }

  Widget recommendRow() {
    return Container(
      //color: Colors.amber,
      height: MediaQuery.of(context).size.width * 0.3,
      width: MediaQuery.of(context).size.width,
      child: Column(
        children: [
          Expanded(
            flex: 1,
            child: Center(
              child: Text(" - 為你推薦 - "),
            ),
          ),
          Expanded(
            flex: 4,
            child: recommendStreamer(),
          ),
        ],
      ),
    );
  }

  Widget recommendStreamer() {
    //推薦主播
    return CustomScrollView(
      shrinkWrap: true,
      scrollDirection: Axis.horizontal,
      slivers: <Widget>[
        SliverList(
          delegate: SliverChildBuilderDelegate(
            (BuildContext context, int index) {
              return widget.recommendLiveData.result.map((result) {
                final iconWidth = MediaQuery.of(context).size.width / 5.7;
                return Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8),
                  child: Column(
                    children: [
                      Stack(
                        children: [
                          ClipOval(
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(iconWidth),
                                border: Border.all(
                                  color: result.liveStatus == 1 //直播中 外圈主色
                                      ? Colors.amber
                                      : Colors.transparent,
                                  width: 2,
                                ),
                                color: Colors.white,
                              ),
                              width: iconWidth,
                              height: iconWidth,
                              child: Padding(
                                padding: const EdgeInsets.all(2.0),
                                child: ClipOval(
                                  //主播頭像
                                  child: ImageCached(
                                    imageUrl: result.icon,
                                    placeholder: ImageType.userImage,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                            //加號圖片
                            top: iconWidth - 20,
                            left: iconWidth - 20,
                            child: Image.asset(
                              "assets/icons/addFollow.png",
                              width: 20,
                            ),
                          ),
                        ],
                      ),
                      Container(
                        child: Text(
                          result.nickname, //主播名稱
                          maxLines: 1,
                        ),
                      ),
                    ],
                  ),
                );
              }).toList()[index];
            },
            childCount: widget.recommendLiveData.result.length,
          ),
        ),
      ],
    );
  }

  needLoginRow() {
    return Container(
      //color: Colors.blueGrey,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(18.0),
            child: Image.asset(
              "assets/images/signIn.png",
              width: MediaQuery.of(context).size.width / 3,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: Text(
              "趕緊登錄關注喜歡的主播",
              style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
            ),
          ),
          GestureDetector(
            onTap: didTappedLogin,
            child: Container(
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                  color: Colors.amber, borderRadius: BorderRadius.circular(5)),
              child: Text(
                "立即登錄",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  //backgroundColor: Colors.amber,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  didTappedLogin() {
    print("didTappedLogin");
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => LoginPage()),
    ).then(
      (callBackValue) => setState(() {
        print("Navigator callBackValue $callBackValue");
        //isLogin = AuthManager().isLogin;
      }),
    );
  }

  apiSetup() async {
    widget.recommendLiveData = await AttentionRecommendLive.getRecommandLive();
    if (mounted)
      setState(() {
        isLoading = false;
      });
  }
}
