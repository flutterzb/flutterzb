import 'package:flutter/material.dart';

import 'attentionLivePage.dart';

class AttentionMainPage extends StatefulWidget {
  AttentionMainPage({Key key}) : super(key: key);

  @override
  _AttentionMainScreenState createState() => _AttentionMainScreenState();
}

class _AttentionMainScreenState extends State<AttentionMainPage>
    with SingleTickerProviderStateMixin {
  TabController _tabController;
  var tabBarButtonList = [Tab(text: '主播'), Tab(text: '視頻'), Tab(text: '活動')];
  var tabBarViewList = [
    Container(
      child: AttentionLivePage(),
    ),
    Container(
      child: Center(
        child: Text(
          '視頻',
          style: TextStyle(fontSize: 50),
        ),
      ),
      color: Colors.yellow,
    ),
    Container(
      child: Center(
        child: Text(
          '活動',
          style: TextStyle(fontSize: 50),
        ),
      ),
      color: Colors.red,
    )
  ];

  @override
  void initState() {
    super.initState();
    _tabController =
        TabController(vsync: this, length: tabBarButtonList.length);
    //tab切换的回调
    _tabController.addListener(() {
      var index = _tabController.index;
      print('tab 切换了 ${tabBarButtonList[index]}');
    });
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          //backgroundColor: Colors.amber,
          bottom: TabBar(
            labelColor: Colors.amber,
            unselectedLabelColor: Colors.black87,
            indicatorColor: Colors.amber,
            controller: _tabController,
            tabs: tabBarButtonList,
          ),
          title: Text("關注"),
        ),
        body: TabBarView(
          controller: _tabController,
          children: tabBarViewList,
        ),
      ),
      length: tabBarButtonList.length,
    );
  }
}
