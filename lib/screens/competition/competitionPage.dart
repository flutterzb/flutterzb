import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_zb/api/competitionSchedule.dart';
import 'package:flutter_zb/api/comprtitionType.dart';
import 'package:flutter_zb/api/entitys/competitionSchedule.dart';
import 'package:flutter_zb/api/entitys/competitionType.dart';

import 'package:flutter_zb/imageCache.dart';
import 'package:flutter_zb/loadingLottie.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';

import '../liveRoom/LiveStreamRoom.dart';
import 'competitionTypePage.dart';

class CompetitionPage extends StatefulWidget {
  CompetitionPage({Key key}) : super(key: key);
  CompetitionScheduleResponesData competitionScheduleData;
  CompetitionTypeResponseData competitionTypeData;

  @override
  _CompetitionPageState createState() => _CompetitionPageState();
}

class _CompetitionPageState extends State<CompetitionPage>
    with WidgetsBindingObserver, SingleTickerProviderStateMixin {
  bool isLoading = true;
  String selectedTypeName = "體育-全部";
  String selectedParentTypeName = "";
  String selectedChildTypeName = "";
  bool selectedHotGame = false;
  int selectedDateIndex = 0;
  dynamic selectedParentTypeID;
  dynamic selectedChildTypeID;
  AnimationController _animationController; //animationController;
  Animation<double> _scaleAnimation;
  Duration duration = Duration(milliseconds: 400);
  Tween<Offset> _tween = Tween(begin: Offset(0, 1), end: Offset(0, 0));
  AppLifecycleState _notification;
  bool needShowAnimation = false;
  double topCompotitionCard = 0;
  ScrollController scrollerController = ScrollController();
  bool isScrolling = false;

  @override
  void initState() {
    apiSetup();
    super.initState();
    _animationController = AnimationController(vsync: this, duration: duration);
    _scaleAnimation =
        Tween<double>(begin: 1, end: 0.95).animate(_animationController);

    scrollerController.addListener(() {
      double value = scrollerController.offset / 110;
      // setState(() {
      //   topCompotitionCard = value;
      //topCompotitionCard = scrollerController.offset > 50;
      // });
    });
    WidgetsBinding.instance.addObserver(this);
    //getMatchScheduleSportListResponseData();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    print("didChangeAppLifecycleState: $state");
    //setState(() { _notification = state; });
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    if (isLoading) {
      //return Center(child: CupertinoActivityIndicator());
      return Center(
        child: LoadingLottieView(),
      );
    } else {
      return SafeArea(
        child: Container(
          //選分類的時候有一些些iOS present modal的感覺背景黑
          color: Colors.black,
          child: Stack(
            children: [
              ScaleTransition(
                scale: _scaleAnimation,
                child: ClipRRect(
                  //選分類的時候有一些些iOS present modal的感覺
                  borderRadius: !needShowAnimation
                      ? BorderRadius.circular(0)
                      : BorderRadius.only(
                          topLeft: Radius.circular(12),
                          topRight: Radius.circular(12)),
                  child: Material(
                    animationDuration: duration,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        SizedBox(height: 42, child: topView()),
                        Expanded(flex: 1, child: dateView()),
                        Expanded(flex: 15, child: comptitonCard()),
                      ],
                    ),
                  ),
                ),
              ),
              SlideTransition(
                position: _tween.animate(_animationController),
                child: CompetitionTypePage(
                  competitionTypeData: widget.competitionTypeData,
                  selectedParentTypeID: this.selectedParentTypeID,
                  selectedChildTypeID: this.selectedChildTypeID,
                  didSelectedCancel: () {
                    //print("didSelectedCancel");
                    setState(() {
                      needShowAnimation = false;
                    });
                    _animationController.reverse();
                  },
                  didSelectedType: (String selectedTypeName,
                      dynamic selectedParentTypeID,
                      dynamic selectedChildTypeID) {
                    //print("didSelectedType");
                    setState(() {
                      isLoading = true;
                    });
                    this.selectedTypeName = selectedTypeName;
                    this.selectedParentTypeID = selectedParentTypeID;
                    this.selectedChildTypeID = selectedChildTypeID;
                    apiSetup();
                    needShowAnimation = false;
                    _animationController.reverse();
                  },
                ),
              ),
            ],
          ),
        ),
      );
    }
  }

  Widget topView() {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
      child: Row(
        children: <Widget>[
          //選擇分類
          GestureDetector(
            onTap: () async {
              print("選擇分類");

              if (_animationController.isDismissed)
                _animationController.forward();
              else if (_animationController.isCompleted)
                _animationController.reverse();
              setState(() {
                needShowAnimation = true;
              });
            },
            child: Container(
              margin: EdgeInsets.fromLTRB(5, 0, 5, 0),
              decoration: BoxDecoration(
                border: Border.all(
                    color: Colors.amber, width: 1.0, style: BorderStyle.solid),
                borderRadius: BorderRadius.all(
                  Radius.circular(3),
                ),
              ),
              child: Padding(
                padding: const EdgeInsets.fromLTRB(8, 2, 8, 2),
                child: Row(
                  children: <Widget>[
                    Icon(Icons.filter_list, color: Colors.amber),
                    Text(
                      selectedTypeName,
                      style: TextStyle(
                        color: Colors.amber,
                        fontSize: 17,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Spacer(),
          //熱門
          GestureDetector(
            onTap: () {
              setState(() {
                selectedHotGame = !selectedHotGame;
              });
            },
            child: Container(
                margin: EdgeInsets.fromLTRB(5, 0, 5, 0),
                decoration: BoxDecoration(
                  border: Border.all(
                      color: selectedHotGame ? Colors.amber : Colors.grey,
                      width: 1.0,
                      style: BorderStyle.solid),
                  borderRadius: BorderRadius.all(
                    Radius.circular(3),
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(8, 2, 8, 2),
                  child: Row(
                    children: <Widget>[
                      Image.asset(
                        "assets/icons/hot.png",
                        height: 18,
                        width: 18,
                        color: selectedHotGame ? Colors.amber : Colors.grey,
                      ),
                      Text(
                        "熱門",
                        style: TextStyle(
                            fontSize: 17,
                            color:
                                selectedHotGame ? Colors.amber : Colors.grey),
                      ),
                    ],
                  ),
                )),
          ),
        ],
      ),
    );
  }

  Widget dateView() {
    return CustomScrollView(
      scrollDirection: Axis.horizontal,
      slivers: <Widget>[
        SliverList(
          delegate: SliverChildBuilderDelegate(
            (BuildContext context, int index) {
              final result = widget.competitionScheduleData.result[index];
              String date = DateFormat('MM-dd').format(result.date);
              return GestureDetector(
                onTap: () {
                  setState(() {
                    selectedDateIndex = index;
                  });
                },
                child: Container(
                  margin: EdgeInsets.fromLTRB(5, 2, 5, 2),
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: index == selectedDateIndex
                          ? Colors.amber
                          : Colors.grey,
                      width: 1,
                    ),
                    borderRadius: BorderRadius.all(
                      Radius.circular(3),
                    ),
                  ),
                  child: Center(
                    child: Padding(
                      padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
                      child: Text(
                        date,
                        style: TextStyle(
                          color: index == selectedDateIndex
                              ? Colors.amber
                              : Colors.grey,
                          fontSize: 17,
                        ),
                      ),
                    ),
                  ),
                ),
              );
            },
            // Or, uncomment the following line:
            childCount: widget.competitionScheduleData.result.length,
          ),
        ),
      ],
    );
  }

  Widget comptitonCard() {
    //return SingleChildScrollView(
    //primary: false,
    //child: Column(
    //mainAxisSize: MainAxisSize.max,
    //children: isHotComptition(),
    //),
    //);
    return NotificationListener<ScrollNotification>(
      onNotification: (notification) {
        if (notification is ScrollStartNotification) {
          debugPrint("Scroll start");
          // setState(() {
          //   isScrolling = true;
          // });
        } else if (notification is ScrollEndNotification) {
          debugPrint("Scroll end");
          // setState(() {
          //   isScrolling = false;
          // });
        }
        return true;
      },
      child: RefreshIndicator(
        color: Colors.amber,
        displacement: 70,
        onRefresh: refreshEvent,
        child: CustomScrollView(
          scrollDirection: Axis.vertical,
          controller: scrollerController,
          slivers: <Widget>[
            SliverList(
              delegate: SliverChildBuilderDelegate(
                (BuildContext context, int index) {
                  // double scale = 1.0;
                  // if (topCompotitionCard > 0.5) {
                  //   scale = index + 0.5 - topCompotitionCard;
                  //   if (scale < 0) {
                  //     scale = 0;
                  //   } else if (scale > 1) {
                  //     scale = 1;
                  //   }
                  // }

                  return
                      // Transform(
                      //   transform: Matrix4.identity()..scale(scale, scale),
                      //   alignment: Alignment.bottomCenter,
                      //   child:
                      //Align(
                      //heightFactor: 0.7,
                      //child:
                      //   Transform(
                      // transform: Matrix4.identity()
                      //   ..setEntry(3, 2, 0.01)
                      //   ..rotateX(isScrolling ? -0.2 : 0.0),
                      //   //https://www.youtube.com/watch?v=ogk6X0iuZpw
                      // alignment: Alignment.center,
                      // child:
                      comptitionWidgetList()[index];
                  //)
                  //);
                },
                // Or, uncomment the following line:
                childCount: comptitionWidgetList().length,
              ),
            ),
          ],
        ),
      ),
    );
  }

  List<Widget> comptitionWidgetList() {
    List<CompeitionDataByDay> scheduldByDayList;
    if (selectedHotGame) {
      scheduldByDayList = widget
          .competitionScheduleData.result[selectedDateIndex].data
          .where((scheduldByDay) =>
              (scheduldByDay.tag == "热门")) //熱門不是scheduldByDay.hot == 1 ....
          .toList();
    } else {
      scheduldByDayList =
          widget.competitionScheduleData.result[selectedDateIndex].data;
    }
    return scheduldByDayList.map((scheduldByDay) {
      return Container(
        margin: EdgeInsets.fromLTRB(10, 5, 10, 5),
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 2,
                blurRadius: 2,
                offset: Offset(2, 3) // changes position of shadow
                ),
          ],
          borderRadius: BorderRadius.all(Radius.circular(5)),
        ),
        height: 100,
        width: MediaQuery.of(context).size.width * 0.9,
        child: Column(
          children: <Widget>[
            //(左上角)聯賽名稱 與前面的hot icon
            Padding(
              padding: EdgeInsets.fromLTRB(12, 5, 12, 5),
              child: Container(
                //color: Colors.amber,
                height: 14,
                child: Row(
                  children: <Widget>[
                    scheduldByDay.tag == "热门" //熱門不是scheduldByDay.hot == 1 ....
                        ? Align(
                            alignment: Alignment.bottomCenter,
                            child: Padding(
                              padding: EdgeInsets.fromLTRB(0, 0, 2, 0),
                              child: Image.asset(
                                "assets/icons/hotGame.png",
                                fit: BoxFit.fitHeight,
                                height: 11,
                              ),
                            ),
                          )
                        : SizedBox(),
                    Center(
                      child: Text(
                        scheduldByDay.lname,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 11,
                          //backgroundColor: Colors.blue,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),

            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                //(左)主隊圖 + 隊名
                Container(
                  //color: Colors.amber,
                  width: 105,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 8),
                        height: 26,
                        width: 26,
                        child: ImageCached(
                          imageUrl: scheduldByDay.hicon,
                          placeholder: ImageType.soccer,
                        ),
                      ),
                      Container(
                          child: Text(
                        scheduldByDay.hname,
                        maxLines: 1,
                        textAlign: TextAlign.right,
                      )),
                    ],
                  ),
                ),

                //主隊分數
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 3),
                  child: Text(
                    scheduldByDay.solarApi.hTotalScore.toString(),
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                ),

                Container(
                  margin: EdgeInsets.fromLTRB(13, 0, 13, 0),
                  child: Column(
                    children: <Widget>[
                      Text(
                        DateFormat('HH:mm').format(scheduldByDay.time),
                        style: TextStyle(color: Colors.grey[350]),
                      ), //時間
                      Text(
                        //當有直播時 顯示 solarApi.timeInfo ，再改字
                        scheduldByDay.matchStatus == "直播中"
                            ? (scheduldByDay.solarApi.timeInfo ==
                                    scheduldByDay.matchStatus
                                ? "LIVE"
                                : scheduldByDay.solarApi.timeInfo)
                            : (scheduldByDay.matchStatus.contains("预定")
                                ? "VS"
                                : scheduldByDay.matchStatus.toString()),
                        style: TextStyle(color: Colors.red),
                      ),
                    ],
                  ),
                ),

                //客隊分數
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 3),
                  child: Text(
                    scheduldByDay.solarApi.aTotalScore.toString(),
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                ),

                //(右)客隊圖 + 隊名
                Container(
                  //color: Colors.amberAccent,
                  width: 105,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 8),
                        height: 26,
                        width: 26,
                        child: ImageCached(
                          imageUrl: scheduldByDay.aicon,
                          placeholder: ImageType.soccer,
                        ),
                      ),
                      Container(
                          child: Text(
                        scheduldByDay.aname,
                        maxLines: 1,
                        textAlign: TextAlign.left,
                      )),
                    ],
                  ),
                ),
              ],
            ),

            //最底下 直播主名稱
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: streamerList(scheduldByDay.account),
            ),
          ],
        ),
      );
    }).toList();
  }

  List<Widget> streamerList(List<Account> accountList) {
    var finalList = accountList;
    if (accountList.length > 4) {
      //超過四個 則只顯示四位主播
      finalList = accountList.sublist(0, 4);
    }
    return finalList.map(
      (account) {
        return GestureDetector(
          onTap: () {
            if (account.liveStatus == 1) {
              Navigator.push(context,
                  MaterialPageRoute(builder: (BuildContext context) {
                return LiveStreamRoom(roomNum: account.roomNum);
              }));
            } else {
              Fluttertoast.showToast(
                msg: "尚未開播",
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.CENTER,
                backgroundColor: Colors.black87,
                textColor: Colors.white,
                fontSize: 16.0,
              );
            }
          },
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 3, horizontal: 4),
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 3),
              decoration: BoxDecoration(
                color: account.liveStatus == 1
                    ? accountTypeColor(account.colorType)
                    : Colors.transparent,
                border: Border.all(
                  color: Colors.black,
                  width: 0.5,
                ),
                borderRadius: BorderRadius.all(Radius.circular(5)),
              ),
              child: Text(
                account.name,
                style: TextStyle(fontSize: 14),
              ),
            ),
          ),
        );
      },
    ).toList();
  }

  Color accountTypeColor(int colorType) {
    /* 賽程表顏色分類：color_type 1.主播, 2.機器人, 3.動畫 */
    if (colorType == 1) {
      return Colors.pinkAccent[200];
    } else if (colorType == 2) {
      return Colors.amber[200];
    } else {
      return Colors.lightBlue[200];
    }
  }

  Future refreshEvent() async {
    setState(() {
      isLoading = true;
    });
    await Future.delayed(Duration(milliseconds: 400), () {
      apiSetup();
    });
  }

  apiSetup() async {
    if (widget.competitionTypeData == null) {
      await getCompetitionType();
    }
    await getCompetitionScheduleData();

    setState(() {
      isLoading = false;
    });
  }

  //左上角的分類清單
  getCompetitionType() async {
    widget.competitionTypeData = await CompetitionType.getCompetitionType();
    final parentTypeName = widget.competitionTypeData.data.types[0].name;
    final childeTypeNmae =
        widget.competitionTypeData.data.types[0].childTypes[0].name;
    //預設的選擇
    selectedTypeName = "$parentTypeName-$childeTypeNmae";
  }

  //賽程資訊
  getCompetitionScheduleData() async {
    if (selectedParentTypeID != null && selectedChildTypeID != null) {
      widget.competitionScheduleData =
          await CompetitionSchedule.getListWithGame(
              parentID: selectedParentTypeID, childID: selectedChildTypeID);
    } else {
      widget.competitionScheduleData =
          await CompetitionSchedule.getListWithGame();
    }
  }
}
