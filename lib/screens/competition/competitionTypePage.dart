import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_zb/api/entitys/competitionType.dart';

class CompetitionTypePage extends StatefulWidget {
  CompetitionTypePage({
    Key key,
    this.competitionTypeData,
    this.selectedParentTypeID,
    this.selectedChildTypeID,
    this.didSelectedType,
    this.didSelectedCancel,
  }) : super(key: key);
  //dynamic因為分類的 id 可能是 stinrg "all", 或是 int...
  dynamic selectedParentTypeID;
  dynamic selectedChildTypeID;
  final CompetitionTypeResponseData competitionTypeData;
  final Function(String, dynamic, dynamic) didSelectedType;
  final VoidCallback didSelectedCancel;
  @override
  _CompetitionTypePageState createState() => _CompetitionTypePageState();
}

class _CompetitionTypePageState extends State<CompetitionTypePage> {
  double nameLengthCountByWidth = 12;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.selectedParentTypeID == null) {
      widget.selectedParentTypeID = widget.competitionTypeData.data.types[0].id;
    }
    if (widget.selectedChildTypeID == null) {
      widget.selectedChildTypeID =
          widget.competitionTypeData.data.types[0].childTypes[0].id;
    }
    if (MediaQuery.of(context).size.width < 375) {
      nameLengthCountByWidth = 11;
    }
    return DraggableScrollableSheet(
      initialChildSize: 0.95,
      maxChildSize: 0.95,
      minChildSize: 0.95,
      builder: (BuildContext context, ScrollController scrollController) {
        return ClipRRect(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(12),
            topRight: Radius.circular(12),
          ),
          child: Container(
            color: Colors.white,
            child: ListView.builder(
              dragStartBehavior: DragStartBehavior.start,
              controller: scrollController,
              itemCount: typeView().length,
              itemBuilder: (BuildContext context, int index) {
                return typeView()[index];
              },
            ),
          ),
        );
      },
    );
  }

  //點分類跳出來的view
  List<Widget> typeView() {
    List<Widget> array = [];
    array.add(Container(
      color: Colors.grey[400],
      child: Row(
        //mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0), //IconButton padding default 8
            child: SizedBox(width: 24), //IconButton default 24
            //讓分類文字置中的Box
          ),
          Spacer(),
          Text(
            "請選擇分類",
            style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
          ),
          Spacer(),
          IconButton(
            icon: Icon(Icons.cancel),
            onPressed: widget.didSelectedCancel,
          ),
        ],
      ),
    ));

    for (var types in widget.competitionTypeData.data.types) {
      int index = widget.competitionTypeData.data.types.indexOf(types);
      final title = widget.competitionTypeData.data.types[index].name;

      //大分類 體育 娛樂 電競
      array.add(Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(8, 5, 8, 8),
              child: Text(
                "$title",
                style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
      ));

      //子分類
      List<Widget> tempWidgetRow = [];
      for (var childType in types.childTypes) {
        //是否是選擇到的
        bool isCurrentSelectItem =
            (widget.selectedChildTypeID == childType.id &&
                    widget.selectedParentTypeID == types.id)
                ? true
                : false;
        //選到與沒選到的顏色
        Color colorBySelected =
            isCurrentSelectItem ? Colors.amber : Colors.grey[400];

        tempWidgetRow.add(
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 3.0),
            child: ChoiceChip(
              backgroundColor: Colors.white,
              selectedColor: Colors.white,
              disabledColor: Colors.white,
              label: Text("${childType.name}"),
              labelStyle: TextStyle(
                fontSize: 16,
                color: colorBySelected,
              ),
              selected: isCurrentSelectItem,
              onSelected: (didSelected) {
                widget.selectedParentTypeID = types.id;
                widget.selectedChildTypeID = childType.id;
                setState(() {});
                widget.didSelectedType("${types.name}-${childType.name}",
                    widget.selectedParentTypeID, widget.selectedChildTypeID);
              },
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0),
                side: BorderSide(color: colorBySelected),
              ),
            ),
          ),
        );
      }
      array.add(
        Wrap(
            //spacing: 4.0,
            runSpacing: -5.0,
            alignment: WrapAlignment.start,
            children: tempWidgetRow),
      );
    }
    return array;
  }
}
