import 'package:flutter/material.dart';
import 'package:flutter_zb/screens/attention/attentionMainPage.dart';
import 'package:flutter_zb/screens/competition/competitionPage.dart';
import 'package:flutter_zb/screens/shortFilm/shortFilmPage.dart';
import 'package:flutter_zb/screens/userInfo/UserInfoBody.dart';
//import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../component/BottomTabbar.dart';
import '../../screens/firstPage/FirstPageBody.dart';

class FirstPage extends StatefulWidget {
  FirstPage({Key key}) : super(key: key);
  static String routeName = "/firstPage";

  @override
  _FirstPageState createState() => _FirstPageState();
}

class _FirstPageState extends State<FirstPage> {
  int tabBarIndex = 0;
  List<Widget> tabBarPages = [];
  List<String> tabBarPagesTitle = ["首頁", "賽程", "關注", "小視頻", "個人"];
  // List<Widget> pages = [
  //   FirstPageBody(),
  //   Container(color: Colors.blue),
  //   Container(color: Colors.black54),
  //   Container(color: Colors.amber),
  // ];

  @override
  void initState() {
    super.initState();
    tabBarPages = [
      FirstPageBody(),
      SizedBox(),
      SizedBox(),
      SizedBox(),
      SizedBox()
    ];
  }

  @override
  Widget build(BuildContext context) {
    //print("tabBarIndex:$tabBarIndex");
    return Scaffold(
      //appBar: tabBarIndex == 0 ? null : normalAppBar(),
      body: IndexedStack(
        index: tabBarIndex,
        children: tabBarPages,
      ),
      //bottomNavigationBar: BottomTabbar(),
      bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          currentIndex: tabBarIndex,
          onTap: (int idx) {
            setState(() {
              tabBarIndex = idx;
              if (tabBarPages[tabBarIndex] is SizedBox) {
                //print(tabBarPages);
                switch (tabBarIndex) {
                  case 1:
                    tabBarPages.insert(tabBarIndex, CompetitionPage());
                    tabBarPages.removeAt(tabBarIndex + 1);
                    break;
                  case 2:
                    tabBarPages.insert(tabBarIndex, AttentionMainPage());
                    tabBarPages.removeAt(tabBarIndex + 1);
                    break;
                  case 3:
                    tabBarPages.insert(tabBarIndex, ShortFilmPage());
                    tabBarPages.removeAt(tabBarIndex + 1);
                    break;
                  case 4:
                    tabBarPages.insert(tabBarIndex, UserInfoBody());
                    tabBarPages.removeAt(tabBarIndex + 1);
                    break;
                  default:
                    print("Invalid choice");
                    break;
                }
                //print(pages);
              }
            });
          },
          items: [
            BottomNavigationBarItem(
                //Image.asset("assets/icons/flower.svg")
                activeIcon: Icon(Icons.home, color: Colors.amber),
                icon: Icon(Icons.home, color: Colors.black87),
                title: Text(
                  tabBarPagesTitle[0],
                  style: TextStyle(
                      color: tabBarIndex == 0 ? Colors.amber : Colors.black87),
                )),
            BottomNavigationBarItem(
                activeIcon: Icon(Icons.today, color: Colors.amber),
                icon: Icon(Icons.today, color: Colors.black87),
                title: Text(
                  tabBarPagesTitle[1],
                  style: TextStyle(
                      color: tabBarIndex == 1 ? Colors.amber : Colors.black87),
                )),
            BottomNavigationBarItem(
                activeIcon: Icon(Icons.grade, color: Colors.amber),
                icon: Icon(Icons.grade, color: Colors.black87),
                title: Text(
                  tabBarPagesTitle[2],
                  style: TextStyle(
                      color: tabBarIndex == 2 ? Colors.amber : Colors.black87),
                )),
            BottomNavigationBarItem(
                activeIcon: Icon(Icons.featured_video, color: Colors.amber),
                icon: Icon(Icons.featured_video, color: Colors.black87),
                title: Text(
                  tabBarPagesTitle[3],
                  style: TextStyle(
                      color: tabBarIndex == 3 ? Colors.amber : Colors.black87),
                )),
            BottomNavigationBarItem(
                //Icons.account_box
                activeIcon: Icon(Icons.assignment_ind, color: Colors.amber),
                icon: Icon(Icons.assignment_ind, color: Colors.black87),
                title: Text(
                  tabBarPagesTitle[4],
                  style: TextStyle(
                      color: tabBarIndex == 4 ? Colors.amber : Colors.black87),
                )),
          ]),
    );
  }
}
