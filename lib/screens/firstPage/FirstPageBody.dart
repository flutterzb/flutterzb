import 'package:flutter/gestures.dart';
import 'package:flutter_zb/api/firstPageMenu.dart';
import 'package:flutter_zb/api/get_league_board_json.dart';
import 'package:flutter_zb/component/hotTopCarouselSlider.dart';
import 'package:flutter_zb/screens/firstPage/FirstPageSubTabView.dart';
import 'package:flutter_zb/screens/liveRoom/LiveRoomFloat.dart';
import 'package:flutter_zb/screens/views/bannerPageView.dart';
import 'package:flutter_zb/screens/views/leagueBoardView.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:flutter_zb/api/entitys/firstPageMenu.dart';
import 'package:flutter_zb/api/entitys/league_board.dart';
import 'package:flutter_zb/api/entitys/banner.dart';
import 'package:flutter_zb/api/entitys/news.dart';
import 'package:flutter_zb/api/entitys/entitys.dart';
import 'package:flutter_zb/api/entitys/hotHome.dart';
import 'package:flutter_zb/api/entitys/hotTop.dart';
import 'package:flutter_zb/api/entitys/matchScheduleSportList.dart';
import 'package:flutter_zb/api/entitys/recommendStreamer.dart';

import 'package:flutter_zb/api/banner.dart';
import 'package:flutter_zb/api/hotTop.dart';
import 'package:flutter_zb/api/recommendStreamer.dart';
import 'package:flutter_zb/api/matchScheduleSportList.dart';
import 'package:flutter_zb/api/news.dart';

import 'package:flutter_zb/component/matchScheduleSport.dart';
import 'package:flutter_zb/component/news.dart';
import '../../api/hotHome.dart';
import '../../component/recommend_streamer.dart';
import '../../component/hotHomeSixBlock.dart';
import '../../component/title_with_more_bbtn.dart';
import '../../loadingLottie.dart';
import '../liveRoom/LiveStreamRoom.dart';

class FirstPageBody extends StatefulWidget {
  FirstPageBody({Key key}) : super(key: key);

  @override
  _FirstPageBodyState createState() => _FirstPageBodyState();
}

class _FirstPageBodyState extends State<FirstPageBody>
    with SingleTickerProviderStateMixin {
  var isLoading = true;
  List<Widget> tabBarButtonList = [];
  List<Widget> tabBarViewList = [];
  TabController _tabController;
  FirstPageMenuResponseData pageMenuData;
  GetBannerResponseData bannerData;
  MatchScheduleSportListResponseData matchScheduleSportList;
  HotTopResponseData hotTop;
  RecommendStreamerResponseData recommendStreamer;
  GetNewsResponseData newsData;
  HotHomeResponseData hotHomeData;
  LeagueBoardResponesData leagueBoardData;

  OverlayEntry liveStreamOverlay;
  LiveRoomFloat liveRoom;
  LiveStreamRoom liveStreamRoom;
  Offset position = Offset(0.0, 0.0);
  double screenWidth;
  bool overlayUpdate = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    screenWidth = MediaQuery.of(context).size.width;

    if (isLoading) {
      apiSetup();
      return Center(
        child: LoadingLottieView(),
      );
    } else {
      return SafeArea(
        child: NestedScrollView(
          dragStartBehavior: DragStartBehavior.down,
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              firstPageAppBar(),
            ];
          },
          body: isLoading
              ? Center(
                  child: LoadingLottieView(),
                )
              : DefaultTabController(
                  child: TabBarView(
                    controller: _tabController,
                    children: tabBarViewList,
                  ),
                  length: tabBarButtonList.length,
                ),
        ),
      );
    }
  }

  Widget firstPageScrollView() {
    return RefreshIndicator(
      color: Colors.amber,
      displacement: 70,
      onRefresh: refreshEvent,
      child: CustomScrollView(
        slivers: <Widget>[
          SliverList(
            delegate: SliverChildBuilderDelegate(
              (BuildContext context, int index) {
                // To convert this infinite list to a list with items,
                return setFirstPageScrollWidgets()[index];
              },
              // Or, uncomment the following line:
              childCount: setFirstPageScrollWidgets().length,
            ),
          ),
        ],
      ),
    );
  }

  List<Widget> setFirstPageScrollWidgets() {
    List<Widget> array = [];
    array.add(BannerPageView(bannerData: bannerData));

    array.add(
        TitleWithMoreBtn(title: "焦點賽程", press: () {}, ifNeedMoreButton: false));
    array.add(
        MatchScheduleSport(matchScheduleSportList: matchScheduleSportList));

    array.add(
        TitleWithMoreBtn(title: "最新直播", press: null, ifNeedMoreButton: false));
    array.add(HotTopCarouselSlider(
      hotTop: hotTop,
      didSelectedLiveRoom: (HotTopResult hotTopResult) {
        liveRoomWidgetInit(hotTopResult);
        showliveStreamOverlay();
      },
    ));

    if (leagueBoardData.data.isNotEmpty) {
      array.add(
          TitleWithMoreBtn(title: "數據榜", press: null, ifNeedMoreButton: false));
      array.add(LeagueBoardView(leagueBoardData: leagueBoardData));
    }

    array.add(TitleWithMoreBtn(title: "熱門主播", press: () {}));
    array.add(RecommendStreamerScrollView(
      imageStr: recommendStreamer.result[0].imageUrl,
      results: recommendStreamer.result,
    ));

    array.add(
        TitleWithMoreBtn(title: "最新快訊", press: null, ifNeedMoreButton: false));
    array.add(NewsView(newsData: newsData));

    for (final results in hotHomeData.result) {
      final title = results.name;
      array.add(
        //體育 娛樂 電競
        TitleWithMoreBtn(title: title, press: () {}),
      );
      array.add(
        //下面六個
        Container(
            //final double itemHeight = 124;
            //final double itemWidth = 165;
            height: MediaQuery.of(context).size.width * (165 / 135),
            child: SixBlockView(
              imageStr: results.rooms[0].imageUrl,
              rooms: results.rooms,
            )),
      );
    }
    return array;
  }

  SliverAppBar firstPageAppBar() {
    return SliverAppBar(
      elevation: 1,
      automaticallyImplyLeading: false,
      //forceElevated: innerBoxIsScrolled,
      pinned: true,
      floating: true,
      snap: true,
      primary: true,
      stretchTriggerOffset: 20,
      expandedHeight: 80,
      toolbarHeight: 0,
      flexibleSpace: FlexibleSpaceBar(
        background: Padding(
          padding: EdgeInsets.only(left: 12),
          child: Align(
            alignment: Alignment.topLeft,
            child: Image.asset(
              'assets/icons/logoLargeBlack.png',
              fit: BoxFit.fitWidth,
              width: 100,
            ),
          ),
        ),
      ),
      bottom: PreferredSize(
        preferredSize: Size.fromHeight(42),
        child: Container(
          child: Column(
            children: [
              Align(
                alignment: Alignment.centerLeft,
                child: Padding(
                  padding: EdgeInsets.fromLTRB(10, 0, 10, 3),
                  child: SizedBox(
                    height: 38,
                    child: TabBar(
                        isScrollable: true,
                        labelColor: Colors.amber,
                        unselectedLabelColor: Colors.black87,
                        indicatorColor: Colors.amber,
                        controller: _tabController,
                        tabs: tabBarButtonList),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  // Offset getIndicatorOffset(Offset dragOffset) {
  //   final double x = (dragOffset.dx - (indicatorWidth / 2.0))
  //       .clamp(0.0, slideWidth - indicatorWidth);
  //   final double y = (slideHeight - indicatorHeight) / 2.0;
  //   return Offset(x, y);
  // }

  showliveStreamOverlay() {
    final width = MediaQuery.of(context).size.width * 0.65;
    final playerHeight = width * 9 / 16;
    if (liveStreamOverlay == null) {
      liveStreamOverlay = OverlayEntry(builder: (context) {
        return GestureDetector(
          onPanStart: (details) {
            setState(() {
              overlayUpdate = true;
              if (position.dx == 0) {
                position = Offset(
                    details.localPosition.dx / 2, details.localPosition.dy / 2);
              }
            });
          },
          onPanUpdate: (details) {
            setState(() {
              // position = Offset(position.dx + details.delta.dx,
              //     position.dy + details.delta.dy);
              position += details.delta;
              overlayUpdate = true;
              liveStreamOverlay.markNeedsBuild();
            });
          },
          onPanEnd: (details) {
            setState(() {
              overlayUpdate = false;
              liveStreamOverlay.markNeedsBuild();
            });

            //print(position.dy + playerHeight);
            //print(MediaQuery.of(context).size.height);
            if (position.dy + (playerHeight * 1.3) >
                MediaQuery.of(context).size.height) {
              position = Offset(0, 0);
              liveStreamOverlay.remove();
              liveStreamOverlay = null;
            }
          },
          child: Stack(
            overflow: Overflow.visible,
            children: <Widget>[
              /* 嘗試用Draggable widget 但原因寫在func裡面*/
              //draggableLiveRoomScreen(),
              Positioned(
                top: position.dy,
                left: position.dx,
                child:
                    // Transform.scale(
                    //     scale: 0.8,
                    //     child:
                    Container(color: Colors.transparent, child: liveRoom),
                // ),
              ),
              Positioned(
                bottom: 0,
                right: 0,
                left: 0,
                child: Visibility(
                  visible: overlayUpdate,
                  child: Container(
                    height: playerHeight * 0.5,
                    color: Colors.grey[300].withOpacity(0.5),
                    child: Icon(
                      Icons.cancel,
                      size: 50,
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      });
    }
    Overlay.of(context).insert(liveStreamOverlay);
  }

  liveRoomWidgetInit(HotTopResult hotTopResult) {
    // 懸浮視窗
    // if (liveRoom == null) {
    //   liveRoom = LiveRoomFloat(
    //     roomNum: hotTopResult.roomNum,
    //     mid: hotTopResult.mid.toString(),
    //     heroImage: hotTopResult.icon,
    //   );
    // }
    if (liveStreamRoom == null) {
      liveStreamRoom = LiveStreamRoom(
        roomNum: hotTopResult.roomNum,
        mid: hotTopResult.mid.toString(),
        heroImage: hotTopResult.icon,
      );
    }
  }

  // Widget draggableLiveRoomScreen() {
  //   /* 後來不用Draggable
  //      因為在拖曳的時候feedback 會重新New一個VLC物件
  //      適用在圖片拖曳的時候, 配合DragTarget widget
  //   */
  //   return Draggable(
  //     child: liveRoom,
  //     //childWhenDragging: SizedBox(),
  //     feedback: Container(
  //       height: screenWidth * 9 / 16,
  //       width: screenWidth,
  //       decoration:
  //           BoxDecoration(border: Border.all(width: 2, color: Colors.amber)),
  //     ),
  //     onDraggableCanceled: (velocity, offset) {
  //       setState(() {
  //         position = offset;
  //       });
  //     },
  //   );
  // }

  Future refreshEvent() async {
    await Future.delayed(const Duration(milliseconds: 400), () {
      apiSetup();
    });
  }

  apiSetup() async {
    // Future.wait([
    //   getHopTop(),
    //   getRecommendStreamer(),
    //   getHotHome(),
    //   getMatchScheduleSportList()
    // ]);
    await getPageMenu();
    await getBanner();
    await getHopTop();
    await getLeagueBoardPath();
    await getRecommendStreamer();
    await getHotHome();
    await getMatchScheduleSportList();
    await getNews();

    if (mounted)
      setState(() {
        this.isLoading = false;

        tabBarButtonList = [];
        tabBarViewList = [];
        //加首頁
        tabBarButtonList.add(Tab(text: '首頁'));
        tabBarViewList.add(firstPageScrollView());
        //加體育 娛樂 電競
        pageMenuData.result.forEach((result) {
          tabBarButtonList.add(Tab(text: result.name));
          tabBarViewList.add(FirstPageSubTabView(
            // https://smzb.cn/api/nav/menu
            menuChild: result.children,
            menuChildAll: result.id, //因為api來的子分類 "全部" id 不是int... 先取出來丟
          ));
        });
        if (_tabController == null) {
          _tabController =
              TabController(vsync: this, length: tabBarButtonList.length);
          _tabController.addListener(() {
            var index = _tabController.index;
            final tabTitle = tabBarButtonList[index];
            print('tabbar 選了:$tabTitle');
          });
        }
      });
  }

  getPageMenu() async {
    pageMenuData = await FirstPageMenu.getFirstPageMenu();
  }

  getBanner() async {
    bannerData = await AdBanner.getBanner();
  }

  getHopTop() async {
    hotTop = await HotTop.getHotTop();
  }

  getRecommendStreamer() async {
    recommendStreamer = await RecommendStreamer.getHecommendStreamer();
  }

  getLeagueBoardPath() async {
    leagueBoardData = await LeagueBoard.getLeagueBoardPath();
  }

  getHotHome() async {
    hotHomeData = await HotHome.getHotHome();
  }

  getMatchScheduleSportList() async {
    matchScheduleSportList =
        await MatchScheduleSportList.getMatchScheduleSportList();
  }

  getNews() async {
    newsData = await News.getNews();
  }
}
