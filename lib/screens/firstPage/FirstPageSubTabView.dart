import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_zb/api/childVideoList.dart';
import 'package:flutter_zb/api/entitys/childVideoList.dart';
import 'package:flutter_zb/screens/views/RecommendCard.dart';
import 'package:flutter_zb/api/entitys/firstPageMenu.dart';
import 'package:lottie/lottie.dart';

import '../../loadingLottie.dart';
import '../liveRoom/LiveStreamRoom.dart';

class FirstPageSubTabView extends StatefulWidget {
  FirstPageSubTabView({
    Key key,
    this.menuChild,
    this.menuChildAll, //因為api來的子分類 "全部" id 不是int... 要回上一層json拿
  }) : super(key: key);

  final List<MenuChild> menuChild;
  final int menuChildAll;

  @override
  _FirstPageBodyState createState() => _FirstPageBodyState();
}

class _FirstPageBodyState extends State<FirstPageSubTabView>
    with SingleTickerProviderStateMixin {
  bool isLoading = true;
  static const double itemHeight = 125;
  static const double itemWidth = 165;
  TabController _tabController;
  List<Widget> tabBarButtonList = [];
  List<Widget> tabBarViewList = [];
  int innerTabCurrentIndex = 0;
  ChildVideoListResponseData childVideoList;

  @override
  void initState() {
    super.initState();
    widget.menuChild.forEach(
      (item) => {
        tabBarButtonList.add(Tab(text: item.name)),
      },
    );
    _tabController =
        TabController(vsync: this, length: tabBarButtonList.length);
    _tabController.addListener(() {
      var index = _tabController.index;
      final tabTitle = widget.menuChild[index].name;
      print('inner tabbar 選了:$tabTitle');
      innerTabCurrentIndex = index;
      setState(() {
        isLoading = true;
        apiSetup();
      });
    });

    apiSetup(isFirstTabBarItem: true);
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (isLoading) {
      return Expanded(child: loadStateView());
    } else {
      return Expanded(
        child: Column(
          children: [
            secondLayerTabbar(),
            DefaultTabController(
              length: childVideoList.data.rooms.length,
              child: tabbarViewOrEmptyText(),
            ),
          ],
        ),
      );
    }
  }

  //子分類 tabbar
  SizedBox secondLayerTabbar() {
    return SizedBox(
      height: 38,
      child: Align(
        alignment: Alignment.centerLeft,
        child: Padding(
          padding: EdgeInsets.fromLTRB(10, 2, 10, 2),
          child: TabBar(
              isScrollable: true,
              labelColor: Colors.amber,
              unselectedLabelColor: Colors.black87,
              indicatorColor: Colors.amber,
              controller: _tabController,
              tabs: tabBarButtonList),
        ),
      ),
    );
  }

  //讀取的時候 讓子分類tabbar繼續顯示
  Widget loadStateView() {
    return Column(
      children: [
        secondLayerTabbar(),
        Spacer(),
        LoadingLottieView(),
        Spacer(),
      ],
    );
  }

  Widget tabbarViewOrEmptyText() {
    if (childVideoList.data.rooms.length == 0) {
      return Expanded(
        child: Center(
          child: Text("暫無資料",
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
        ),
      );
    } else {
      return Expanded(
        child: TabBarView(
          controller: _tabController,
          children: tabBarViewList,
        ),
      );
    }
  }

  Widget subChildTabView() {
    return Expanded(
      child: Container(
        margin: EdgeInsets.fromLTRB(0, 5, 0, 5),
        child: CustomScrollView(
          slivers: [
            SliverGrid(
              delegate: SliverChildBuilderDelegate(
                (BuildContext context, int index) {
                  return recommendCardWidget(
                      context, childVideoList.data.rooms[index]);
                },
                childCount: childVideoList.data.rooms.length,
              ),
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  childAspectRatio: (itemWidth / itemHeight),
                  crossAxisCount: 2,
                  mainAxisSpacing: 10.0,
                  crossAxisSpacing: 0),
            ),
          ],
        ),
      ),
    );
  }

  Widget recommendCardWidget(
      BuildContext context, ChildVideoListDataRoom room) {
    bool isLive = room.liveStatus == 1 ? true : false;
    String name;
    String imageStr;
    String info = room.title == null ? room.gameName : room.title;

    if (room.liveStatus != null) {
      //直播中
      isLive = room.liveStatus == 1 ? true : false;
      imageStr = room.imageUrl == null ? room.thumbnailUrl : room.imageUrl;
      info = room.title == null ? room.gameName : room.title;
      name = room.nickname;
    } else {
      //視頻
      isLive = false;
      name = room.nickname;
      imageStr = room.thumbnailUrl;
      info = room.name;
    }

    return Padding(
      padding: EdgeInsets.fromLTRB(6, 0, 6, 0),
      child: RecommendCard(
        isLive: isLive,
        imageStr: imageStr,
        userImageStr: room.icon,
        visitCount: room.visitCount.toString(),
        accountTitle: room.accountTitle,
        userName: name,
        info: info,
        press: () {
          //Navigator.pushNamed(context, LiveStreamRoom.routeName);
          Navigator.push(context,
              MaterialPageRoute(builder: (BuildContext context) {
            return LiveStreamRoom(roomNum: room.roomNum);
          }));
        },
      ),
    );
  }

  apiSetup({bool isFirstTabBarItem = false}) async {
    await getChildVideoList(isFirstTabBarItem);
    if (mounted)
      setState(() {
        isLoading = false;
        tabBarViewList = [];
        widget.menuChild.forEach((element) {
          tabBarViewList.add(subChildTabView());
        });
      });
  }

  getChildVideoList(bool isFirstTabBarItem) async {
    String liveTypeID;
    if (isFirstTabBarItem) {
      //主分類的全部 由上層的json取得
      liveTypeID = widget.menuChildAll.toString();
    } else {
      liveTypeID = widget.menuChild[innerTabCurrentIndex].id.toString();
    }
    childVideoList = await ChildVideoList.getChildVideoList(
        liveTypeID: liveTypeID, page: "1");
    //print(childVideoList.data.rooms.length);
  }
}
