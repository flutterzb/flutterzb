import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_zb/api/liveRoom.dart';
import 'package:flutter_zb/api/entitys/liveRoom.dart';
// import 'package:fluttertoast/fluttertoast.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:flutter_vlc_player/flutter_vlc_player.dart';

import '../../imageCache.dart';
import '../../loadingLottie.dart';

class LiveRoomFloat extends StatefulWidget {
  LiveRoomFloat({
    Key key,
    this.roomNum,
    this.mid,
    this.heroImage,
  }) : super(key: key);

  String roomNum;
  String mid;
  String heroImage;
  @override
  _LiveRoomFloatState createState() => _LiveRoomFloatState();
}

class _LiveRoomFloatState extends State<LiveRoomFloat>
    with AutomaticKeepAliveClientMixin {
  static final VlcPlayerController controller = VlcPlayerController(
      // Start playing as soon as the video is loaded.
      onInit: () {
    controller.play();
  });
  var isLoading = true;

  static double width;
  static double height;
  static double playerHeight;
  LiveRoomInfo liveRoomInfo;

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width * 0.65;
    height = MediaQuery.of(context).size.height * 0.8;
    playerHeight = width * 9 / 16;
    apiSetup();
    //print(liveRoomInfo.streamUrlRtmp["720"]);
    if (isLoading) {
      return Container(
        color: Colors.grey[350],
        width: playerHeight,
        height: playerHeight,
        child: LoadingLottieView(),
      );
    } else {
      //print(liveRoomInfo.streamUrlRtmp["720"]);
      if (liveRoomInfo == null) {
        //errorAndPop();
      } else {
        return playViewWidget(context);
      }
    }
  }

  Widget playViewWidget(BuildContext context) {
    if (liveRoomInfo.liveStatus == 1) {
      return SizedBox(
        height: playerHeight,
        width: width,
        child: VlcPlayer(
          aspectRatio: 16 / 9,
          url: liveRoomInfo.streamUrlRtmp["720"],
          controller: controller,
          placeholder: Center(child: CircularProgressIndicator()),
        ),
      );
    } else {
      return SizedBox(
          height: playerHeight,
          width: width,
          child: Column(
            children: <Widget>[
              GestureDetector(
                child: Align(
                  alignment: Alignment.topLeft,
                  child: IconButton(
                      icon: SvgPicture.asset("assets/icons/back_arrow.svg"),
                      onPressed: () {
                        Navigator.pop(context);
                      }),
                ),
              ),
              Spacer(),
              Image.asset(
                "assets/images/userImage.png",
                width: 56,
                height: 56,
              ),
              Text(
                "開播在即 敬請期待",
                style: TextStyle(
                  fontSize: 18,
                  color: Colors.white70,
                ),
              ),
              Spacer(),
            ],
          ));
    }
  }

  apiSetup() async {
    await liveRoom();

    if (mounted) {
      setState(() {
        isLoading = false;
      });
    }
  }

  liveRoom() async {
    liveRoomInfo = await LiveRoom.getLiveRoom(widget.roomNum, mid: widget.mid);
  }
}
