import 'dart:ui';
import 'dart:math';
import 'dart:convert';
import 'dart:io' show Platform;
import 'package:flutter/services.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_zb/api/liveRoom.dart';
import 'package:flutter_zb/api/entitys/liveRoom.dart';
import 'package:flutter_zb/imageCache.dart';
import 'package:flutter_zb/screens/liveRoom/SettingsPage.dart';
import 'package:flutter_zb/screens/liveRoom/WebViewChatRoom.dart';
import 'package:flutter_zb/utils/AuthManager.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:flutter_vlc_player/flutter_vlc_player.dart';

import '../login/loginPage.dart';

class LiveStreamRoom extends StatefulWidget {
  const LiveStreamRoom({
    Key key,
    this.roomNum,
    this.mid,
    this.heroImage,
  }) : super(key: key);

  static String routeName = "/liveStreamRoom";
  final String roomNum;
  final String mid;
  final String heroImage;

  @override
  _LiveStreamRoomState createState() => _LiveStreamRoomState();
}

class _LiveStreamRoomState extends State<LiveStreamRoom>
    with TickerProviderStateMixin, AutomaticKeepAliveClientMixin {
  List<Widget> tabBarButtonList = [];
  List<Widget> tabBarViewList = [];

  TabController _tabController;
  static final VlcPlayerController controller = VlcPlayerController(
      // Start playing as soon as the video is loaded.
      onInit: () {
    controller.play();
  });

  var isLoading = true;
  var ifReloadChatRoomOnly = false;
  bool isControlPanelShow = false;
  WebViewController webViewController;
  LiveRoomInfo liveRoomInfo;
  int audienceCount = 0;
  static double width;
  static double height;
  static double playerHeight;
  bool settingsPageQulityHight = true;
  EdgeInsets padding;
  bool isPortrait = true;
  OverlayEntry settingsOverlayEntry;
  bool didTappedSetting = false;
  AnimationController _animationController;
  Duration duration = Duration(milliseconds: 550);
  Tween<Offset> _tween = Tween(begin: Offset(0, 1), end: Offset(0, 0));
  bool needShowMiniVLC = true;

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();
    //print("LiveRoom.initState()");
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.landscapeRight,
    ]);
    if (widget.roomNum == null) {
      errorAndPop();
    } else {
      apiSetup();
    }
    _animationController = AnimationController(vsync: this, duration: duration);
  }

  @override
  void dispose() {
    controller.dispose();
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    padding = MediaQuery.of(context).padding;
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;
    playerHeight = width * 9 / 16;

    if (isLoading) {
      return Center(
        child: ClipOval(
          child: SizedBox(
            width: 80,
            height: 80,
            child: ImageCached(
              imageUrl: widget.heroImage,
              placeholder: ImageType.userImage,
            ),
          ),
        ),
        //),
      );
    } else {
      //print(liveRoomInfo.streamUrlRtmp["720"]);
      if (liveRoomInfo == null) {
        errorAndPop();
      } else {
        return Material(
          type: MaterialType.transparency,
          child: OrientationBuilder(
            builder: (context, orientation) {
              isPortrait = orientation == Orientation.portrait ? true : false;
              return SafeArea(
                /* bottom 加入直橫判斷是因為 打橫時 
                 瀏海機SafeArea bottom會有22 px的距離 打橫的時候bottom關掉
              */

                bottom: isPortrait,
                child: Column(
                  children: <Widget>[
                    playViewWidget(context, isPortrait),
                    Expanded(
                      child: Opacity(
                        opacity: isPortrait ? 1 : 0,
                        child: Stack(
                          children: [
                            DefaultTabController(
                              child: Column(
                                children: [
                                  tabbar(),
                                  Expanded(
                                    child: ifReloadChatRoomOnly
                                        ? Container(
                                            //color: Colors.blueAccent,
                                            )
                                        : TabBarView(
                                            controller: _tabController,
                                            children: tabBarViewList,
                                          ),
                                  ),
                                ],
                              ),
                              length: tabBarButtonList.length,
                            ),
                            Visibility(
                              visible: isPortrait && didTappedSetting,
                              child: SlideTransition(
                                position: _tween.animate(_animationController),
                                child: SettingsPage(
                                  currentQualityHigh:
                                      this.settingsPageQulityHight,
                                  needDisappear: (currentQualityHigh) {
                                    this.settingsPageQulityHight =
                                        currentQualityHigh;
                                    settingsControl();
                                  },
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              );
            },
          ),
        );
      }
    }
  }

  Widget playViewWidget(BuildContext context, bool isPortrait) {
    if (liveRoomInfo.liveStatus == 1) {
      return GestureDetector(
        //包在Stack外面 behavior要HitTestBehavior.translucent,否則tap不會觸發
        behavior: HitTestBehavior.translucent,
        onTap: () {
          playViewTapped();
        },
        child: Stack(
          children: [
            SizedBox(
              height: isPortrait
                  ? playerHeight
                  : MediaQuery.of(context).size.height - padding.top,
              width:
                  isPortrait ? width : width - (padding.left + padding.right),
              child: VlcPlayer(
                aspectRatio: 16 / 9,
                url: liveRoomInfo.streamUrlRtmp["720"],
                controller: controller,
                placeholder: Center(child: CircularProgressIndicator()),
              ),
            ),
            Visibility(
              visible: isControlPanelShow,
              child: controlPanelWidget(isPortrait),
            ),
          ],
        ),
      );
    } else {
      return SizedBox(
          height: playerHeight,
          width: width,
          child: Column(
            children: <Widget>[
              GestureDetector(
                child: Align(
                  alignment: Alignment.topLeft,
                  child: IconButton(
                      icon: SvgPicture.asset("assets/icons/back_arrow.svg"),
                      onPressed: () {
                        Navigator.pop(context);
                      }),
                ),
              ),
              Spacer(),
              Image.asset(
                "assets/images/userImage.png",
                width: 56,
                height: 56,
              ),
              Text(
                "開播在即 敬請期待",
                style: TextStyle(
                  fontSize: 18,
                  color: Colors.white70,
                ),
              ),
              Spacer(),
            ],
          ));
    }
  }

  // tabbar
  Container tabbar() {
    return Container(
      color: Colors.white,
      height: 38,
      child: Align(
        alignment: Alignment.centerLeft,
        child: Padding(
          padding: EdgeInsets.fromLTRB(2, 2, 10, 2),
          child: TabBar(
            isScrollable: true,
            labelColor: Colors.amber,
            unselectedLabelColor: Colors.black87,
            indicatorColor: Colors.amber,
            controller: _tabController,
            tabs: tabBarButtonList,
            onTap: (index) => didSelectTab(index),
          ),
        ),
      ),
    );
  }

  didSelectTab(int index) {
    //currentTabIndex = index;
  }

  Container controlPanelWidget(bool isPortrait) {
    return Container(
      height: isPortrait
          ? playerHeight
          : MediaQuery.of(context).size.height - padding.top,
      width: isPortrait ? width : width - (padding.left + padding.right),
      color: Color.fromRGBO(51, 51, 51, 0.4),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: [
                GestureDetector(
                  onTap: () {
                    Navigator.pop(context, needShowMiniVLC);
                  },
                  child: Icon(
                    Icons.arrow_back_ios,
                    color: Colors.grey[300],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: SizedBox(
                    width: MediaQuery.of(context).size.width * 0.75,
                    child: Text(
                      liveRoomInfo.title,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                      style: TextStyle(color: Colors.grey[300], fontSize: 15),
                    ),
                  ),
                ),
                Spacer(),
                GestureDetector(
                  onTap: () {
                    print("did tapped settings");
                    settingsControl();
                  },
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(0, 0, 5, 0),
                    child: Icon(
                      Icons.settings,
                      color: Colors.grey[200],
                    ),
                  ),
                ),
              ],
            ),
          ),
          Spacer(),
          GestureDetector(
              onTap: () {
                controller.setStreamUrl(liveRoomInfo.streamUrlRtmp["720"]);
                controller.play();
              },
              child: Icon(
                Icons.refresh,
                color: Colors.grey[200],
              )),
          Spacer(),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ClipOval(
                    child: Container(
                      height: 10,
                      width: 10,
                      color: Colors.yellow,
                    ),
                  ),
                ),
                Text(
                  "觀看人數 $audienceCount",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 13,
                  ),
                ),
                Spacer(),
                GestureDetector(
                  onTap: () {
                    print("did tapped hd");
                    settingsControl();
                  },
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(8, 0, 8, 2),
                    child: Icon(
                      Icons.hd,
                      color: Colors.grey[200],
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    print("did tapped rotate");
                    if (isPortrait)
                      //直的話改橫
                      SystemChrome.setPreferredOrientations(
                          [DeviceOrientation.landscapeLeft]).then((_) {
                        if (Platform.isIOS) {
                          //Android會撇回去...
                          SystemChrome.setPreferredOrientations([
                            DeviceOrientation.portraitUp,
                            DeviceOrientation.landscapeLeft,
                            DeviceOrientation.landscapeRight,
                          ]);
                        }
                      });
                    else
                      //橫的話改直
                      SystemChrome.setPreferredOrientations(
                          [DeviceOrientation.portraitUp]).then((_) {
                        if (Platform.isIOS) {
                          SystemChrome.setPreferredOrientations([
                            DeviceOrientation.portraitUp,
                            DeviceOrientation.landscapeLeft,
                            DeviceOrientation.landscapeRight,
                          ]);
                        }
                      });
                  },
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(8, 0, 8, 2),
                    child: Icon(
                      Icons.screen_rotation,
                      color: Colors.grey[200],
                    ),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  settingsControl() {
    if (_animationController.isDismissed) {
      setState(() {
        didTappedSetting = true;
      });
      _animationController.forward();
    } else if (_animationController.isCompleted) {
      _animationController.reverse();
      Future.delayed(duration, () {
        setState(() {
          didTappedSetting = false;
        });
      });
    }
  }

  playViewTapped() {
    setState(() {
      isControlPanelShow = !isControlPanelShow;
    });
    Future.delayed(Duration(milliseconds: 5500), () {
      //5.5秒後隱藏
      setState(() {
        isControlPanelShow = false;
      });
    });
  }

  errorAndPop() {
    Fluttertoast.showToast(
        msg: "直播間資訊有誤",
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.CENTER,
        backgroundColor: Colors.black87,
        textColor: Colors.white,
        fontSize: 16.0);
    Future.delayed(const Duration(milliseconds: 500), () {
      Navigator.pop(context);
    });
  }

  settingsOverlay() async {
    settingsOverlayEntry = OverlayEntry(builder: (context) {
      return AnimatedPositioned(
        duration: Duration(milliseconds: 2500),
        //curve: Curves.fastOutSlowIn,
        top: didTappedSetting ? playerHeight : height,
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height - playerHeight,
        child: Material(
          animationDuration: Duration(milliseconds: 2500),
          //color: Color.fromRGBO(128, 128, 128, 0.7),
          color: Colors.white,
          child: SizedBox(
            height: MediaQuery.of(context).size.height - playerHeight,
            child: Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      GestureDetector(
                        onTap: () {
                          setState(() {
                            didTappedSetting = false;
                          });
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Transform.rotate(
                            angle: 270 * pi / 180,
                            child: Icon(Icons.arrow_back_ios),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: SizedBox(
                          child: Text(
                            "設定",
                            style: TextStyle(fontSize: 17),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Text(
                    "解析度",
                    style: TextStyle(fontSize: 17),
                  )
                ],
              ),
            ),
          ),
        ),
      );
    });
    Overlay.of(context).insert(settingsOverlayEntry);
  }

  apiSetup({bool ifLoginSucceed = false}) async {
    setState(() {
      if (ifLoginSucceed) {
        this.ifReloadChatRoomOnly = true;
      }
      tabBarButtonList.clear();
      tabBarViewList.clear();
      _tabController = null;
    });
    await liveRoom();

    if (mounted) {
      setState(() {
        isLoading = false;
        ifReloadChatRoomOnly = false;
        tabBarButtonList.add(Tab(text: '聊天室'));
        tabBarViewList.add(webViewChatRoomInit());

        tabBarButtonList.add(Tab(text: '賽況'));
        tabBarViewList.add(Expanded(
          child: Container(
            color: Colors.white,
            child: Center(
              child: Text("賽況"),
            ),
          ),
        ));
        tabBarButtonList.add(Tab(text: '主播'));
        tabBarViewList.add(Expanded(
          child: Container(
            color: Colors.white,
            child: Center(
              child: Text("主播"),
            ),
          ),
        ));
        if (_tabController == null) {
          _tabController =
              TabController(vsync: this, length: tabBarButtonList.length);
          _tabController.addListener(() {
            var index = _tabController.index;
            final tabTitle = tabBarButtonList[index];
            print('tabbar 選了:$tabTitle');
          });
        }
      });
    }
  }

  liveRoom() async {
    liveRoomInfo = await LiveRoom.getLiveRoom(widget.roomNum, mid: widget.mid);
  }

  WebViewChatRoom webViewChatRoomInit() {
    return WebViewChatRoom(
      liveRoomInfo: liveRoomInfo,
      needLogin: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => LoginPage()),
        ).then(
          (callBackValue) => setState(() {
            print("Navigator callBackValue $callBackValue");
            if (callBackValue) {
              apiSetup(ifLoginSucceed: callBackValue);
            }
          }),
        );
      },
      guestUpdate: (guestCount) {
        audienceCount = guestCount;
      },
      giftNeedShow: () {
        print("顯示禮物");
      },
    );
  }
}
