import 'dart:math';
import 'package:flutter/material.dart';

class SettingsPage extends StatelessWidget {
  SettingsPage({
    Key key,
    this.needDisappear,
    this.currentQualityHigh,
  }) : super(key: key);

  Function(bool) needDisappear;
  bool currentQualityHigh = true;
  var boxDecorationSelected = BoxDecoration(
    borderRadius: BorderRadius.circular(3),
    border: Border.all(
      color: Colors.amber,
      width: 1,
    ),
  );
  var boxDecorationUnselected = BoxDecoration(
    borderRadius: BorderRadius.circular(3),
    border: Border.all(
      color: Colors.grey,
      width: 1,
    ),
  );

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.white,
      child: SizedBox(
        height: MediaQuery.of(context).size.height,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                GestureDetector(
                  onTap: () {
                    needDisappear(currentQualityHigh);
                  },
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Transform.rotate(
                      angle: 270 * pi / 180,
                      child: Icon(Icons.arrow_back_ios),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SizedBox(
                    child: Text(
                      "設定",
                      style: TextStyle(fontSize: 17),
                    ),
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 8),
              child: Text(
                "解析度",
                style: TextStyle(fontSize: 17),
              ),
            ),
            Row(
              children: [
                GestureDetector(
                  onTap: () {
                    currentQualityHigh = true;
                    needDisappear(currentQualityHigh);
                  },
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8),
                    child: Container(
                      child: Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Text(
                          "高清",
                          style: TextStyle(
                              color: currentQualityHigh
                                  ? Colors.amber
                                  : Colors.grey),
                        ),
                      ),
                      decoration: currentQualityHigh
                          ? boxDecorationSelected
                          : boxDecorationUnselected,
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    currentQualityHigh = false;
                    needDisappear(currentQualityHigh);
                  },
                  child: Container(
                    child: Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: Text("流暢",
                          style: TextStyle(
                              color: currentQualityHigh
                                  ? Colors.grey
                                  : Colors.amber)),
                    ),
                    decoration: currentQualityHigh
                        ? boxDecorationUnselected
                        : boxDecorationSelected,
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 8),
              child: Text(
                "頻道選項",
                style: TextStyle(fontSize: 17),
              ),
            ),
            Container(
              color: Colors.grey[200],
              height: 50,
              child: Expanded(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8),
                  child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text("分享", style: TextStyle(fontSize: 17))),
                ),
              ),
            ),
            Divider(height: 3),
            Container(
              color: Colors.grey[200],
              height: 50,
              child: Expanded(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8),
                  child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text("檢舉", style: TextStyle(fontSize: 17))),
                ),
              ),
            ),
            Divider(height: 3),
            Container(
              color: Colors.grey[200],
              height: 50,
              child: Expanded(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8),
                  child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text("幫助中心", style: TextStyle(fontSize: 17))),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
