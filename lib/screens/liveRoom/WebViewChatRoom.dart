import 'dart:convert';
import 'dart:io' show Platform;
import 'package:flutter/material.dart';
import 'package:flutter_zb/api/entitys/liveRoom.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebViewChatRoom extends StatefulWidget {
  WebViewChatRoom({
    Key key,
    this.liveRoomInfo,
    this.needLogin,
    this.guestUpdate,
    this.giftNeedShow,
  }) : super(key: key);

  LiveRoomInfo liveRoomInfo;
  Function needLogin;
  Function(int) guestUpdate;
  Function giftNeedShow;

  @override
  _WebViewChatRoomState createState() => _WebViewChatRoomState();
}

class _WebViewChatRoomState extends State<WebViewChatRoom>
    with AutomaticKeepAliveClientMixin {
  WebViewController webViewController;
  EdgeInsets padding;
  static double width;
  static double height;
  static double playerHeight;
  int audienceCount = 0;

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    padding = MediaQuery.of(context).padding;
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;
    playerHeight = width * 9 / 16;
    return webViewWidget();
  }

  Container webViewWidget() {
    return Container(
      width: width,
      height: height - (playerHeight + padding.bottom + padding.top),
      child: WebView(
        initialUrl: "https://scchat.zanstartv.com",
        javascriptMode: JavascriptMode.unrestricted,
        //userAgent: Platform.isIOS ? null : "Mozilla/5.0 FoxApp",
        onWebViewCreated: (controller) {
          webViewController = controller;
        },
        onPageFinished: (url) {
          //print('Page finished loading: $url');
          var jsContent = {};
          jsContent["origin"] = "setRoom";
          jsContent["appName"] = "solar";
          //unionid
          jsContent["roomName"] = widget.liveRoomInfo.unionid;
          jsContent["accessToken"] = widget.liveRoomInfo.userChatToken;

          jsContent["platform"] = "MM";

          jsContent["openGiftButton"] = true;
          jsContent["appOrigin"] = "smzb";
          jsContent["theme"] = "white"; //black
          jsContent["CDNUrl"] = "//static03.dsoog.cn/bocatsports/";
          jsContent["inApp"] = true;
          String jsContentString = json.encode(jsContent);
          //print(jsContentString);

          final entryString = Platform.isIOS ? "iosEntry" : "androidEntry";
          webViewController
              .evaluateJavascript("$entryString($jsContentString)")
              ?.then((result) {
            print(result);
          });
          //關閉WebView的放大
          webViewController.evaluateJavascript(
              "var meta = document.createElement('meta');" +
                  "meta.name = 'viewport';" +
                  "meta.content = 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no';" +
                  "var head = document.getElementsByTagName('head')[0];" +
                  "head.appendChild(meta);");
        },
        javascriptChannels: <JavascriptChannel>[
          //對應到WKWebView WKScriptMessageHandler
          //let script = WKUserScript(source: source, injectionTime: .atDocumentEnd, forMainFrameOnly: false)
          //config.userContentController.addUserScript(script)
          //config.userContentController.add(self, name: "login")
          needLoginJSChannel(context),
          guestUpdateJSChannel(context),
          giftJSChannel(context),
        ].toSet(),
        // navigationDelegate: (NavigationRequest request) {
        //   print('allowing navigation to $request');
        //   return NavigationDecision.navigate;
        // }
      ),
    );
  }

  JavascriptChannel needLoginJSChannel(BuildContext context) {
    return JavascriptChannel(
        name: 'login',
        onMessageReceived: (JavascriptMessage message) {
          print("didreceive form JS: ${message.message}");
          widget.needLogin();
        });
  }

  JavascriptChannel guestUpdateJSChannel(BuildContext context) {
    return JavascriptChannel(
        name: 'guestUpdate',
        onMessageReceived: (JavascriptMessage message) {
          //print("聊天室人數:${message.message}");
          widget.guestUpdate(int.parse(message.message));
        });
  }

  JavascriptChannel giftJSChannel(BuildContext context) {
    return JavascriptChannel(
        name: 'gift',
        onMessageReceived: (JavascriptMessage message) {
          print("點擊禮物");
        });
  }
}
