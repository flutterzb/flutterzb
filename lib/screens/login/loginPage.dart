import 'package:flutter/material.dart';
import 'package:flutter_zb/screens/login/loginPageBody.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(),
      body: LoginPageBody(),
    );
  }

  AppBar buildAppBar() {
    return AppBar(title: Text("登入"));
  }
}
