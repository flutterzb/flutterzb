import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_zb/api/entitys/login.dart';
import 'package:flutter_zb/api/login.dart';
import 'package:flutter_zb/api/myInfo.dart';
import 'package:flutter_zb/constants.dart';
import 'package:flutter_zb/utils/AuthManager.dart';
import 'package:flutter_zb/utils/DataManager.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'formError.dart';

class LoginPageBody extends StatelessWidget {
  LoginPageBody({Key key}) : super(key: key);
  final scrollViewController = ScrollController();
  Function didChangeLoginState;

  @override
  Widget build(BuildContext context) {
    return Container(
      //color: Colors.amber,
      width: double.infinity,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 25),
        child: SingleChildScrollView(
          /* 這邊用ScrollView 再包起來 鍵盤升起 畫面要推上去
              https://www.youtube.com/watch?v=2E9iZgg5TOY*/
          controller: scrollViewController,
          reverse: true,
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.fromLTRB(0, 15, 0, 15),
                child: Image.asset("assets/images/bottom_img_2.png"),
              ),
              LoginForm(
                didTapTextView: () {
                  // scrollViewController.jumpTo(
                  //   scrollViewController.position.maxScrollExtent,
                  // );
                  scrollViewController.animateTo(
                    scrollViewController.position.maxScrollExtent,
                    duration: Duration(seconds: 1),
                    curve: Curves.easeOut,
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class LoginForm extends StatefulWidget {
  LoginForm({Key key, this.didTapTextView}) : super(key: key);
  Function didTapTextView;

  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final _formKey = GlobalKey<FormState>();
  final List<String> errors = [];
  String phone;
  String password;
  //String conform_password;
  LoginResponseData loginData;

  void addError({String error}) {
    if (!errors.contains(error))
      setState(() {
        errors.add(error);
      });
  }

  void removeError({String error}) {
    if (errors.contains(error))
      setState(() {
        errors.remove(error);
      });
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          buildPhoneFormField(),
          SizedBox(height: 20),
          buildPasswordFormField(),
          SizedBox(height: 20),
          FormError(errors: errors),
          SizedBox(
            height: 55,
            width: double.infinity,
            child: FlatButton(
              onPressed: () {
                if (_formKey.currentState.validate()) {
                  _formKey.currentState.save();
                  executeLogin();
                }
              },
              child: Text("登入"),
              color: Colors.amber,
              textColor: Colors.black,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30),
              ),
            ),
          ),
        ],
      ),
    );
  }

  TextFormField buildPhoneFormField() {
    return TextFormField(
      keyboardType: TextInputType.phone,
      onSaved: (newValue) => phone = newValue,
      onTap: () {
        widget.didTapTextView();
      },
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kPhoneNullError);
        } else if (value.length >= 8) {
          removeError(error: kPhoneNullError);
        }
        password = value;
      },
      validator: (value) {
        if (value.isNotEmpty && errors.contains(kPhoneNullError)) {
          addError(error: kPhoneNullError);
        } else if (value.isEmpty && !errors.contains(kPhoneNullError)) {
          addError(error: kPhoneNullError);
        }
        return null;
      },
      decoration: InputDecoration(
        labelStyle: TextStyle(
          color: Colors.black,
        ),
        labelText: "手機號碼",
        hintText: "輸入手機號碼",
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: Icon(
          Icons.phone,
          color: Colors.black,
        ),
      ),
    );
  }

  TextFormField buildPasswordFormField() {
    return TextFormField(
      obscureText: true, //變成 **
      //keyboardType: TextInputType.number,
      onSaved: (newValue) => password = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kPassNullError);
        } else if (value.length >= 8) {
          removeError(error: kShortPassError);
        }
        password = value;
      },
      validator: (value) {
        if (value.isNotEmpty && errors.contains(kPassNullError)) {
          addError(error: kPassNullError);
        } else if (value.isEmpty && !errors.contains(kPassNullError)) {
          addError(error: kPassNullError);
        }
        return null;
      },
      decoration: InputDecoration(
        labelStyle: TextStyle(
          color: Colors.black,
        ),
        labelText: "輸入密碼",
        hintText: "請輸入密碼",
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: Icon(
          Icons.lock,
          color: Colors.black,
        ),
      ),
    );
  }

  executeLogin() async {
    await postLogin();

    Fluttertoast.showToast(
      msg: loginData.message,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.CENTER,
      backgroundColor: Colors.black87,
      textColor: Colors.white,
      fontSize: 16.0,
    );

    if (loginData.code == 0) {
      AuthManager().setLoginData(phone, password);
      await getMyInfo();
      AuthManager().setToken(DataManager().myInfo.result.token);
      setState(() {
        Navigator.pop(context, true);
      });
    }
  }

  postLogin() async {
    loginData = await Login.postLogin(phone, password);
  }

  getMyInfo() async {
    DataManager().myInfo = await MyInfo.getMyInfo();
  }
}
