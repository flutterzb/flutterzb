import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_vlc_player/flutter_vlc_player.dart';
import 'package:flutter_zb/api/entitys/shortFilm.dart';
import 'package:lottie/lottie.dart';
import '../../api/shortFilm.dart';
import '../../imageCache.dart';

class ShortFilmPage extends StatefulWidget {
  ShortFilmPage({Key key}) : super(key: key);
  ShortFilmResponseData shortFilmData;

  @override
  _ShortFilmPageState createState() => _ShortFilmPageState();
}

class _ShortFilmPageState extends State<ShortFilmPage> {
  List<VlcPlayerController> vlcControllerList = [];
  bool isPlaying = true;
  bool isLoading = true;
  int currentScrollIndex = 0;
  OverlayEntry lottieOverlayEntry;

  @override
  void initState() {
    super.initState();
    apiSetup();
  }

  @override
  void dispose() {
    super.dispose();
    vlcControllerList.forEach((controller) {
      controller.dispose();
    });
  }

  @override
  Widget build(BuildContext context) {
    if (isLoading)
      return Center(
        child: SizedBox(
          height: 30,
          width: 30,
          child: CupertinoActivityIndicator(),
        ),
      );
    else
      return Material(
        child: SafeArea(
          child: PageView.builder(
            scrollDirection: Axis.vertical,
            itemBuilder: (context, index) {
              return playViewWidget(context, index);
            },
            itemCount: widget.shortFilmData.result.length,
            pageSnapping: true, //default true, false滑的時候 前後頁會卡一半
            allowImplicitScrolling: false,
            dragStartBehavior: DragStartBehavior.down,
            onPageChanged: (int index) {
              print("pageview index:$index");
              //前一個暫停
              vlcControllerList[currentScrollIndex].pause();
              //下一個開始
              vlcControllerList[index].play();
              currentScrollIndex = index;
              setState(() {
                isPlaying = true;
              });
            },
          ),
        ),
      );
  }

  Widget playViewWidget(BuildContext context, int index) {
    double screenHeight = MediaQuery.of(context).size.height;
    double screenWidth = MediaQuery.of(context).size.width;
    return Container(
      color: Colors.black,
      child: Stack(
        children: [
          SizedBox(
            height: screenHeight,
            width: screenWidth,
            child: Image.network(
              "https:" + widget.shortFilmData.result[index].thumbnailUrl,
              fit: BoxFit.fitHeight,
            ),
          ),
          Center(
            child: VlcPlayer(
              aspectRatio: 9 / 16,
              url: "https:" + widget.shortFilmData.result[index].urlFhd,
              controller: vlcControllerList[index],
              placeholder: CupertinoActivityIndicator(),
            ),
          ),
          Container(
            height: screenHeight,
            width: screenWidth,
            decoration: BoxDecoration(
                color: isPlaying
                    ? Colors.transparent
                    : Color.fromRGBO(128, 128, 128, 0.7)),
            //color: Colors.transparent,
            child: OutlineButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8.0)),
              child: isPlaying
                  ? Icon(Icons.pause, color: Colors.transparent)
                  : Icon(
                      Icons.play_arrow,
                      color: Colors.white,
                      size: 50,
                    ),
              onPressed: () => {
                setState(() {
                  playOrPause();
                }),
              },
            ),
          ),
        ],
      ),
    );
  }

  lottieSwipeOverlay() async {
    lottieOverlayEntry = OverlayEntry(builder: (context) {
      return GestureDetector(
        onTap: () {
          lottieOverlayEntry.remove();
        },
        child: SafeArea(
          child: Material(
            color: Color.fromRGBO(128, 128, 128, 0.7),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.28,
                  width: MediaQuery.of(context).size.width * 0.28,
                  child: FractionallySizedBox(
                    heightFactor: 0.9,
                    widthFactor: 0.9,
                    child: Lottie.asset(
                      'assets/lottie/shortTechGestureTV.json',
                      fit: BoxFit.fitHeight,
                      animate: true,
                    ),
                  ),
                ),
                Text(
                  "上滑查看下一則視頻",
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.amberAccent,
                  ),
                )
              ],
            ),
          ),
        ),
      );
    });
    Overlay.of(context).insert(lottieOverlayEntry);
    await Future.delayed(const Duration(seconds: 3));
    lottieOverlayEntry.remove();
  }

  playOrPause() {
    String playerState =
        vlcControllerList[currentScrollIndex].playingState.toString();
    //print(playerState);
    if (vlcControllerList[currentScrollIndex].playingState == null) {
      vlcControllerList[currentScrollIndex].play();
      return;
    }
    if (playerState == "PlayingState.STOPPED") {
      isPlaying = true;
      final position = vlcControllerList[currentScrollIndex].position;
      final duration = vlcControllerList[currentScrollIndex].duration;
      final positionMinute = position.inMinutes;
      final positionSecond = position.inSeconds;
      final totalMinute = duration.inMinutes;
      final totalSecond = duration.inSeconds;

      if (positionMinute == totalMinute && positionSecond == totalSecond) {
        /* position和duration對照 是不是播完,如果不是 則是看到一半按暫停
        播完直接執行 .play() 會沒反應...需要再set一次url.. */
        vlcControllerList[currentScrollIndex].setStreamUrl(
            "https:" + widget.shortFilmData.result[currentScrollIndex].urlFhd);
      }
      vlcControllerList[currentScrollIndex].play();
    } else if (playerState == "PlayingState.PLAYING") {
      isPlaying = false;
      vlcControllerList[currentScrollIndex].pause();
    }
  }

  setupPlayerList() {
    for (final item in widget.shortFilmData.result) {
      int index = widget.shortFilmData.result.indexOf(item);
      final VlcPlayerController vlcController = VlcPlayerController(onInit: () {
        if (index == 0) {
          lottieSwipeOverlay();
          Future.delayed(Duration(milliseconds: 300), () {
            //第一個player自動播放
            vlcControllerList[0].play();
          });
        }
      });
      vlcControllerList.add(vlcController);
    }
  }

  apiSetup() async {
    widget.shortFilmData = await ShortFilm.getShortFilmCarousel();
    setupPlayerList();
    setState(() {
      isLoading = false;
    });
  }
}
