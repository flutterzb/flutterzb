import 'package:flutter/material.dart';
import 'package:flutter_zb/api/entitys/myInfo.dart';
import 'package:flutter_zb/screens/login/loginPage.dart';
import 'package:flutter_zb/screens/userInfo/UserInfoDetailPage.dart';
import 'package:flutter_zb/utils/AuthManager.dart';
import 'package:flutter_zb/utils/DataManager.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../../imageCache.dart';

class UserInfoBody extends StatefulWidget {
  const UserInfoBody({Key key}) : super(key: key);

  @override
  _UserInfoBodyState createState() => _UserInfoBodyState();
}

class _UserInfoBodyState extends State<UserInfoBody> {
  List<String> iconNames = ["積分任務", "兌換中心", "消耗紀錄", "我的關注", "觀看紀錄", "複製識別碼"];
  List<String> iconPaths = [
    "assets/icons/userInfo/mission_whiteTheme.png",
    "assets/icons/userInfo/exchange_whiteTheme.png",
    "assets/icons/userInfo/cost_whiteTheme.png",
    "assets/icons/userInfo/collect_whiteTheme.png",
    "assets/icons/userInfo/watchingHistory_whiteTheme.png",
    "assets/icons/userInfo/copy_whiteTheme.png"
  ];
  double width;
  MyInfoResponseData myInfo = DataManager().myInfo;
  bool isLogin = AuthManager().isLogin;

  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    isLogin = AuthManager().isLogin;
    //double height = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: normalAppBar(),
      body: Container(
        color: Colors.grey[200],
        child: Column(
          children: <Widget>[
            topUserInfoWidget(context),
            userCoinWidget(),
            Expanded(
              child: gridViewWidget(width),
            ),
          ],
        ),
      ),
    );
  }

  AppBar normalAppBar() {
    return AppBar(
        automaticallyImplyLeading: false,
        centerTitle: true,
        elevation: 0,
        title: Text("個人"),
        actions: [
          IconButton(
            icon: Icon(
              Icons.settings,
              //color: Theme.of(context).accentColor, //Colors.white,
            ),
            onPressed: () {},
          )
        ]);
  }

  Widget topUserInfoWidget(BuildContext context) {
    return GestureDetector(
      onTap: () => pushToNextPage(userInfoDetail: true),
      child: Container(
        width: width,
        height: 80,
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              offset: Offset(3, 4),
              blurRadius: 2,
              spreadRadius: 2,
              color: Colors.grey.withOpacity(0.4),
            ),
          ],
          borderRadius: BorderRadius.only(
            bottomRight: Radius.circular(50),
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Padding(
                padding: EdgeInsets.fromLTRB(width * 0.05, 3, 10, 3),
                child: userImageWidget()),
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                nameWidget(),
                loginButtonWidget(),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget userImageWidget() {
    if (isLogin)
      return SizedBox(
        height: 60,
        width: 60,
        child: ImageCached(
            imageUrl: myInfo.result.icon, placeholder: ImageType.userImage),
      );
    else
      return Image.asset(
        "assets/icons/userInfo/user_default.png",
        width: 60,
      );
  }

  Widget loginButtonWidget() {
    if (isLogin)
      return SizedBox(width: 90, height: 32);
    else
      return Container(
        width: 90,
        height: 32,
        decoration: BoxDecoration(
          color: Colors.amber,
          borderRadius: BorderRadius.all(Radius.circular(5)),
        ),
        child: Center(
          child: GestureDetector(
            child: Text(
              "立即登入",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 16,
              ),
            ),
            onTap: () => pushToNextPage(),
          ),
        ),
      );
  }

  Text nameWidget() {
    return Text(
      isLogin ? myInfo.result.nickname : "註冊登入即可獲得更多福利!",
      style: TextStyle(
        fontSize: 17,
        fontWeight: FontWeight.bold,
      ),
    );
  }

  Container userCoinWidget() {
    return Container(
      //alignment: Alignment.center,
      margin: EdgeInsets.all(10),
      height: 60,
      width: width * 0.9,
      color: Colors.white,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Image.asset(
            "assets/icons/coin.png",
            width: 40,
            height: 40,
          ),
          SizedBox(width: 5),
          Text(
            "貓幣",
            style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
          ),
          SizedBox(width: 5),
          isLogin
              ? Text("${myInfo.result.brokenStarAmount}",
                  style: TextStyle(
                    fontSize: 17,
                  ))
              : Text("—"),
        ],
      ),
    );
  }

  Container gridViewWidget(double width) {
    return Container(
      //height: height,
      width: width * 0.95,
      //color: Colors.white,
      child: GridView.count(
        //childAspectRatio: (itemWidth / itemHeight),
        primary: false,
        shrinkWrap: false,
        padding: EdgeInsets.all(8), // 内边距
        scrollDirection: Axis.vertical, // 滚动方向
        crossAxisSpacing: 2, // 列间距
        crossAxisCount:
            4, // 每行的个数（Axis.vertic == 横向三个, Axis.horizontal == 竖方向三个）
        mainAxisSpacing: 2,
        //dragStartBehavior: DragStartBehavior.start,
        children: iconNames.map((name) {
          int index = iconNames.indexOf(name);
          return userInfoCard(name, iconPaths[index]);
        }).toList(),
      ),
    );
  }

  Widget userInfoCard(String title, String assetsPath) {
    return GestureDetector(
      onTap: () => pushToNextPage(),
      child: Container(
        //color: Colors.blueGrey,
        //height: 80,
        width: 40,
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 10),
              child: Image.asset(
                assetsPath,
                width: 34,
                height: 34,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(5.0),
              child: Text(title),
            ),
          ],
        ),
      ),
    );
  }

  pushToNextPage({bool userInfoDetail = false}) {
    if (isLogin) {
      if (userInfoDetail) {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => UserInfoDetailPage()),
        ).then(
          (callBackValue) => setState(() {
            print("Navigator callBackValue $callBackValue");
            isLogin = AuthManager().isLogin;
          }),
        );
      } else {
        Fluttertoast.showToast(
          msg: "敬請期待",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.black87,
          textColor: Colors.white,
          fontSize: 16.0,
        );
      }
    } else {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => LoginPage()),
      ).then(
        (callBackValue) => setState(() {
          print("Navigator callBackValue $callBackValue");
          isLogin = AuthManager().isLogin;
          myInfo = DataManager().myInfo;
        }),
      );
    }
  }
}
