import 'package:flutter/material.dart';
import 'package:flutter_zb/api/entitys/myInfo.dart';
import 'package:flutter_zb/imageCache.dart';
import 'package:flutter_zb/utils/AuthManager.dart';
import 'package:flutter_zb/utils/DataManager.dart';

class UserInfoDetailPage extends StatelessWidget {
  UserInfoDetailPage({Key key}) : super(key: key);
  MyInfoResult myInfoResult = DataManager().myInfo.result;

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;

    return Material(
      child: Container(
        //width: width,
        //height: height,
        color: Colors.grey[200],
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            normalAppBar(),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 25),
              child: userImage(screenWidth),
            ),
            detailBody(),
          ],
        ),
      ),
    );
  }

  AppBar normalAppBar() {
    return AppBar(
      centerTitle: true,
      elevation: 0,
      title: Text("個人資料"),
    );
  }

  Container userImage(double screenWidth) {
    return Container(
      height: screenWidth * 0.25,
      width: screenWidth * 0.25,
      child: ImageCached(
        imageUrl: myInfoResult.icon,
        placeholder: ImageType.userImage,
      ),
    );
  }

  Widget detailBody() {
    return Expanded(
      child: CustomScrollView(
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        physics: NeverScrollableScrollPhysics(),
        slivers: <Widget>[
          SliverList(
            delegate: SliverChildBuilderDelegate(
              (BuildContext context, int index) {
                List<String> titleList = ["暱稱", "個性簽名", "修改密碼", "手機號碼", "退出登錄"];
                final title = titleList[index];
                String userData = "";
                if (index == 0) {
                  userData = myInfoResult.nickname;
                } else if (index == 1) {
                  userData = "馬上修改介紹";
                } else if (index == 3) {
                  String phone = AuthManager().phoneNumber;
                  phone = phone.replaceRange(3, 5, "*");
                  userData = phone;
                }
                if (index == 4) {
                  return logoutCell(context, title);
                } else {
                  return userInfoCell(title, userData);
                }
              },
              childCount: 5,
            ),
          ),
        ],
      ),
    );
  }

  Widget userInfoCell(String title, String userInfo) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(12, 3, 12, 3),
      child: Container(
        decoration: BoxDecoration(color: Colors.white, boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 2,
            blurRadius: 2,
            offset: Offset(2, 3), // changes position of shadow
          ),
        ]),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 12),
              child: Text(
                title,
                style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
              ),
            ),
            Spacer(),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 3),
              child: Text(
                userInfo,
                style: TextStyle(fontSize: 15),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(5.0),
              child: Icon(
                Icons.arrow_forward_ios,
                color: Colors.grey[300],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget logoutCell(BuildContext context, String title) {
    return GestureDetector(
      onTap: () {
        print("logout");
        AuthManager().logout();
        Navigator.pop(context, true);
      },
      child: Padding(
        padding: const EdgeInsets.fromLTRB(12, 20, 12, 3),
        child: Container(
          color: Colors.grey[300],
          child: Center(
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 12),
              child: Text(
                title,
                style: TextStyle(
                  color: Colors.red,
                  fontSize: 17,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
