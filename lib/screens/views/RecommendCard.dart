import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import '../../constants.dart';
import '../../imageCache.dart';

class RecommendCard extends StatelessWidget {
  const RecommendCard({
    Key key,
    this.isLive,
    this.imageStr,
    this.userImageStr,
    this.visitCount,
    this.accountTitle,
    this.userName,
    this.info,
    this.press,
  }) : super(key: key);

  final bool isLive;
  final imageStr;
  final userImageStr;
  final String visitCount;
  final accountTitle;
  final userName;
  final info;
  final Function press;

  @override
  Widget build(BuildContext context) {
    Widget yellowBox = DecoratedBox(
      decoration: BoxDecoration(
          color: Colors.yellow, borderRadius: BorderRadius.circular(6)),
    );

    return GestureDetector(
      onTap: press,
      child: Container(
        decoration: BoxDecoration(
          color: Color.fromRGBO(246, 246, 246, 1),
          borderRadius: BorderRadius.circular(5),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 2,
              blurRadius: 5,
              offset: Offset(2, 2), // changes position of shadow
            ),
          ],
        ),
        //height: 138,
        child: Column(
          children: <Widget>[
            Expanded(
              flex: 2,
              child: getStreamImageView(), //整個wiget的上半部底圖 + 左上 右上
            ),
            Expanded(
              flex: 1,
              child: getUserImageWithName(), //整個wiget的下半部 頭像 + 名字 + 比賽資訊
            )
          ],
        ),
      ),
    );
  }

  Container getStreamImageView() {
    return Container(
        height: 100,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
            //只設定左上右上圓角
            topLeft: Radius.circular(5),
            topRight: Radius.circular(5),
          ),
        ),
        child: Stack(
          children: <Widget>[
            ClipRRect(
              borderRadius: BorderRadius.only(
                //只設定左上右上圓角
                topLeft: Radius.circular(5),
                topRight: Radius.circular(5),
              ),
              child: ImageCached(
                  imageUrl: imageStr, placeholder: ImageType.apiErrorImage),
            ),
            //最上面的觀看數 lottie 主播標籤
            Row(
              mainAxisSize: MainAxisSize.max, //表示尽可能多的占用水平方向的空间
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                  child: Container(
                    decoration: BoxDecoration(
                      //由上到下 一個半透明灰色遮罩
                      gradient: LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          //0開始 ～ 1最底, 0.5 只顯示一半
                          stops: [
                            0,
                            0.5
                          ],
                          colors: [
                            Colors.black87.withOpacity(0.4),
                            Colors.transparent
                          ]),
                    ),
                    child: Align(
                      alignment: Alignment.topLeft,
                      child: topLeftWidget(),
                    ),
                  ),
                ),
                topRightAccountTitle(),
              ],
            ),
          ],
        ));
  }

  Widget topLeftWidget() {
    return Container(
      margin: EdgeInsets.only(left: 5),
      child: Row(
        children: <Widget>[
          SizedBox(
            width: 20,
            height: 20,
            child: //yellowBox,
                Lottie.asset(
              'assets/lottie/live_icon.json',
              fit: BoxFit.cover,
            ),
          ),
          Text(
            visitCount,
            style: TextStyle(fontSize: 10, color: Colors.white70),
          ),
        ],
      ),
    );
  }

  Widget topRightAccountTitle() {
    if (this.accountTitle == null || this.accountTitle == "") {
      return Container();
    } else {
      //print(this.accountTitle);
      return Align(
        alignment: Alignment.topRight,
        child: Container(
          child: Align(
            alignment: Alignment.center,
            child: Text(
              accountTitle,
              style: TextStyle(
                color: Colors.white70,
              ),
            ),
          ),
          width: 72,
          height: 20,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              topRight: Radius.circular(5),
              bottomLeft: Radius.circular(5),
            ),
            image: DecorationImage(
              fit: BoxFit.cover,
              image: AssetImage("assets/images/accountTitle.png"),
            ),
          ),
        ),
      );
    }
  }

  Row getUserImageWithName() {
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        //頭像
        Container(
          width: 34,
          height: 34,
          margin: EdgeInsets.only(
            left: 5,
            right: 5,
          ),
          child: ClipOval(
            child: ImageCached(
                imageUrl: userImageStr, placeholder: ImageType.userImage),
          ),
        ),
        Container(
          width: 120,
          //height: 40,
          child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    //color: Colors.red,
                    child: Text(
                      //subtitle
                      info,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    //color: Colors.red,
                    child: Text(
                      //主播名稱
                      userName,
                      //overflow: TextOverflow.ellipsis,
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        color: Colors.black38,
                        fontSize: 13,
                      ),
                    ),
                  ),
                ),
              ]),
        ),
      ],
    );
  }
}
