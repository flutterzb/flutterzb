import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import '../../constants.dart';
import '../../imageCache.dart';

class StreamCard extends StatelessWidget {
  const StreamCard({
    Key key,
    this.imageStr,
    this.press,
  }) : super(key: key);

  final String imageStr;
  final Function press;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    if (imageStr.isEmpty) {
      return GestureDetector(
        onTap: press,
        child: Container(
          margin: EdgeInsets.only(
            left: kDefaultPadding,
            //top: kDefaultPadding / 2,
            //bottom: kDefaultPadding / 2.5,
          ),
          width: size.width * 0.8,
          height: size.width * 0.8 * (184 / 315),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            image: DecorationImage(
              fit: BoxFit.cover,
              image: AssetImage("assets/images/apiErrorPlaceholder.jpg"),
            ),
          ),
        ),
      );
    } else {
      return GestureDetector(
        onTap: press,
        child: Container(
          margin: EdgeInsets.only(
            left: kDefaultPadding,
          ),
          width: size.width * 0.8,
          height: size.width * 0.8 * (184 / 315),
          child:
              // FadeInImage(
              //   placeholder: AssetImage("assets/images/apiErrorPlaceholder.jpg"),
              //   image: NetworkImage("https:" + imageStr),
              // ),
              ImageCached(
            imageUrl: imageStr,
            placeholder: ImageType.apiErrorImage,
          ),
        ),
      );
    }
  }
}
