import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import '../../constants.dart';
import '../../imageCache.dart';

class UserCard extends StatelessWidget {
  const UserCard({
    Key key,
    this.isLive,
    this.imageStr,
    this.userName,
    this.press,
  }) : super(key: key);

  final bool isLive;
  final String imageStr;
  final String userName;
  final Function press;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: press,
      child: Container(
        margin: EdgeInsets.only(
          left: kDefaultPadding / 2,
          top: kDefaultPadding / 2,
          bottom: kDefaultPadding / 2,
        ),
        width: 90, //size.width * 0.3,
        height: 116,
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 2,
              blurRadius: 2,
              offset: Offset(2, 3), // changes position of shadow
            ),
          ],
          color: Color.fromRGBO(246, 246, 246, 1), //Colors.black54,
          borderRadius: BorderRadius.circular(4),
        ),
        child: Center(
          child: Column(
            children: <Widget>[
              Stack(
                alignment: Alignment.bottomRight,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(
                      top: 15,
                    ),
                    width: 70,
                    height: 70,
                    child: ClipOval(
                      child: ImageCached(
                          imageUrl: imageStr, placeholder: ImageType.userImage),
                    ),
                  ),
                  ifNeedLottieAnimate(this.isLive),
                ],
              ),
              Text(
                userName,
                //style: TextStyle(color: Colors.white.withOpacity(0.5)),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  fontSize: 12,
                  color: Colors.black,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget ifNeedLottieAnimate(bool ifNeed) {
    if (ifNeed) {
      return Positioned(
        right: -5,
        bottom: -5,
        child: SizedBox(
          width: 28,
          height: 28,
          child: Lottie.asset(
            'assets/lottie/live_icon.json',
            fit: BoxFit.cover,
          ),
        ),
      );
    } else {
      return SizedBox();
    }
  }
}
