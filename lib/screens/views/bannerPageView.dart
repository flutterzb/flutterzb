import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_zb/api/entitys/banner.dart';
import 'package:flutter_zb/imageCache.dart';

class BannerPageView extends StatefulWidget {
  BannerPageView({
    Key key,
    this.bannerData,
  }) : super(key: key);

  final GetBannerResponseData bannerData;

  @override
  _BannerPageViewState createState() => _BannerPageViewState();
}

class _BannerPageViewState extends State<BannerPageView> {
  int _currentPage = 0;
  PageController _pageController = PageController(
    initialPage: 0,
  );

  @override
  void initState() {
    super.initState();
    Timer.periodic(Duration(seconds: 5), (Timer timer) {
      if (_currentPage < widget.bannerData.result.length - 1) {
        _currentPage++;
      } else {
        _currentPage = 0;
      }
      //print("length ${widget.bannerData.result.length}");
      //print("_currentPage $_currentPage");
      _pageController.animateToPage(
        _currentPage,
        duration: Duration(milliseconds: 350),
        curve: Curves.easeIn,
      );
    });
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      //color: Colors.amber,
      height: 155,
      child: Stack(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 8, 0, 0),
            child: pageView(context),
          ),
          /* Stack 裡面加入Positioned,使翻頁的時候底下的Indicator不會跟著滑掉
             https://medium.com/code4idea/flutter-%E8%B5%B7%E6%AD%A5-day-7-%E5%9C%96%E7%89%87%E5%B9%BB%E7%87%88%E7%89%87-2a6175b34f08
          */
          Positioned(
            left: 0,
            bottom: 0,
            right: 0,
            child: Align(
              alignment: Alignment.bottomCenter,
              child: getIndicator(),
            ),
          ),
        ],
      ),
    );
  }

  Container getIndicator() {
    return Container(
      //color: Colors.amber,
      width: (6.0 * widget.bannerData.result.length) + 20,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: List.generate(
          widget.bannerData.result.length,
          (index) => buildDot(index: index),
        ),
      ),
    );
  }

  PageView pageView(BuildContext context) {
    return PageView(
      controller: _pageController,
      onPageChanged: (index) {
        setState(() {
          _currentPage = index;
        });
      },
      children: widget.bannerData.result.map((bannerResult) {
        return Column(
          children: <Widget>[
            Center(
              child: Container(
                width: MediaQuery.of(context).size.width * 0.85,
                height: 145,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(7.0),
                  child: ImageCached(
                    imageUrl: bannerResult.mBanner,
                    placeholder: ImageType.apiErrorImage,
                  ),
                ),
              ),
            ),
          ],
        );
      }).toList(),
    );
  }

  AnimatedContainer buildDot({int index}) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 350),
      height: 6,
      width: 6,
      margin: EdgeInsets.symmetric(
        vertical: 5,
      ),
      decoration: BoxDecoration(
        color: _currentPage == index ? Colors.amber : Colors.grey,
        borderRadius: BorderRadius.circular(5),
      ),
    );
  }
}
