import 'package:flutter/material.dart';
import 'package:flutter_zb/api/entitys/league_board.dart';
import 'package:flutter_zb/imageCache.dart';

class LeagueBoardView extends StatefulWidget {
  LeagueBoardView({
    Key key,
    this.leagueBoardData,
  }) : super(key: key);
  final LeagueBoardResponesData leagueBoardData;

  @override
  _LeagueBoardViewState createState() => _LeagueBoardViewState();
}

class _LeagueBoardViewState extends State<LeagueBoardView> {
  int firstLayerIndex = 0;
  int secondLayerIndex = 0;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          firstLayerWidget(),
          Align(
            alignment: Alignment.centerLeft,
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                //mainAxisSize: MainAxisSize.max,
                children: secondLayerName(),
              ),
            ),
          ),
          Column(
            children: leagueDataWidget(),
          ),
        ],
      ),
    );
  }

  Widget firstLayerWidget() {
    //print("new firstLayerWidget");
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: widget.leagueBoardData.data.map((leagueData) {
          int index = widget.leagueBoardData.data.indexOf(leagueData);
          //NBA, 英超, 西甲...
          return GestureDetector(
            onTap: () {
              //print("firstLayerIndex: $index");
              setState(() {
                firstLayerIndex = index;
                /*第二層有些很多item 但有些只有一個, 
                  所以第一層改變 第二層選回第一個*/
                secondLayerIndex = 0;
              });
            },
            child: Container(
              alignment: Alignment.center,
              //margin: EdgeInsets.fromLTRB(3, 0, 3, 0),
              color: index == firstLayerIndex
                  ? Colors.white
                  : Color.fromRGBO(240, 240, 240, 1),
              height: 36,
              child: Stack(
                children: <Widget>[
                  Center(
                    child: Container(
                      //margin: EdgeInsets.all(5),
                      width: 58,
                      decoration: BoxDecoration(
                        color: Colors.transparent,
                      ),
                      child: Text(
                        leagueData.name,
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                  Container(
                    width: 58,
                    height: 3,
                    color: index == firstLayerIndex
                        ? Colors.amber
                        : Colors.transparent,
                  ),
                ],
              ),
            ),
          );
        }).toList(),
      ),
    );
  }

  List<Widget> secondLayerName() {
    final leagueData = widget.leagueBoardData.data[firstLayerIndex].data;
    return leagueData.map((secondLayer) {
      int index = leagueData.indexOf(secondLayer);
      return GestureDetector(
        onTap: () {
          setState(() {
            secondLayerIndex = index;
            //print("secondLayerIndex: $index");
          });
        },
        child: Container(
          margin: EdgeInsets.all(5),
          alignment: Alignment.center,
          height: 30,
          width: 64,
          decoration: BoxDecoration(
            color:
                index == secondLayerIndex ? Colors.grey[350] : Colors.grey[200],
            borderRadius: BorderRadius.circular(5),
          ),
          child: Text(
            secondLayer.group,
            textAlign: TextAlign.center,
          ),
        ),
      );
    }).toList();
  }

  List<Widget> leagueDataWidget() {
    List<Widget> array = [];
    array.add(
      Row(
        children: <Widget>[
          Container(
            margin: EdgeInsets.fromLTRB(10, 5, 10, 5),
            child: Text("排名"),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(10, 5, 10, 5),
            child: Text("球隊"),
          ),
          Spacer(),
          Container(
            width: 42,
            margin: EdgeInsets.fromLTRB(5, 5, 5, 5),
            child: Text(
              "出場",
              textAlign: TextAlign.center,
            ),
          ),
          Container(
            width: 42,
            margin: EdgeInsets.fromLTRB(5, 5, 5, 5),
            child: Text(
              "勝/負",
              textAlign: TextAlign.center,
            ),
          ),
          Container(
            width: 42,
            //color: Colors.black,
            margin: EdgeInsets.fromLTRB(5, 5, 5, 5),
            child: Text(
              "勝率",
              textAlign: TextAlign.center,
            ),
          ),
        ],
      ),
    );

    final leagueData = widget.leagueBoardData.data[firstLayerIndex].data;
    //final rowsData = leagueData[secondLayerIndex];
    //array.addAll(rowsData.rows.map((row) {
    //int index = rowsData.rows.indexOf(row);

    // 因為NBA一區15隊會太長 所以只顯示八隊sublist(0, 8)
    final rowsData = leagueData[secondLayerIndex].rows.sublist(0, 8);
    array.addAll(rowsData.map((row) {
      int index = rowsData.indexOf(row);
      int winRate;
      if (row.won != 0) {
        winRate = double.parse(
          (row.won / (row.won + row.loss) * 100).toStringAsExponential(2),
        ).round();
      } else {
        winRate = 0;
      }
      //print(winRate);
      return Row(
        children: <Widget>[
          Container(
            alignment: Alignment.center,
            width: 23,
            height: 23,
            margin: EdgeInsets.fromLTRB(13, 3, 10, 2),
            child: Text(row.position.toString()),
            decoration: BoxDecoration(
              color: index < 3 ? Colors.amber : Colors.grey[350],
              borderRadius: BorderRadius.all(Radius.circular(3)),
            ),
          ),
          Container(
            width: 23,
            height: 23,
            margin: EdgeInsets.fromLTRB(10, 3, 0, 2),
            child: ImageCached(
              imageUrl: row.teamIcon,
              placeholder: ImageType.basketBall,
            ),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(5, 3, 10, 2),
            child: Text(row.teamName),
          ),
          Spacer(),
          Container(
            width: 42,
            margin: EdgeInsets.fromLTRB(5, 3, 5, 2),
            child: Text(
              "${row.won + row.loss}",
              textAlign: TextAlign.center,
            ),
          ),
          Container(
            width: 42,
            margin: EdgeInsets.fromLTRB(5, 3, 5, 2),
            child: Text(
              "${row.won}/${row.loss}",
              textAlign: TextAlign.center,
            ),
          ),
          Container(
            width: 42,
            margin: EdgeInsets.fromLTRB(5, 3, 5, 2),
            child: Text(
              "$winRate%",
              textAlign: TextAlign.center,
            ),
          ),
        ],
      );
    }).toList());
    return array;
  }
}
