import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import '../../api/entitys/matchScheduleSportList.dart';
import 'package:intl/intl.dart';
import '../../constants.dart';
import '../../imageCache.dart';

class MatchScheduleSportListCard extends StatelessWidget {
  const MatchScheduleSportListCard({
    Key key,
    this.result,
  }) : super(key: key);

  final Result result;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Container(
      margin: EdgeInsets.fromLTRB(10.0, 0, 10.0, 10.0),
      width: size.width * 0.7,
      height: size.width * 0.7 * (160 / 256),
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 2,
            blurRadius: 2,
            offset: Offset(2, 3), // changes position of shadow
          ),
        ],
        image: DecorationImage(
          alignment: Alignment.centerRight,
          fit: BoxFit.fitHeight,
          image: AssetImage("assets/images/matchSchdeuleSportListBG.png"),
        ),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Container(
            margin: EdgeInsets.all(5.0),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(DateFormat("MM/dd HH:ss").format(result.time)),
                Text("${result.lname}"),
              ],
            ),
          ),
          Row(
            mainAxisSize: MainAxisSize.max, //表示尽可能多的占用水平方向的空间
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              SizedBox(
                width: 46,
                height: 46,
                child: ImageCached(
                  imageUrl: result.hicon,
                  placeholder: ImageType.soccer,
                ),
              ),
              Text("VS"),
              SizedBox(
                width: 46,
                height: 46,
                child: ImageCached(
                  imageUrl: result.aicon,
                  placeholder: ImageType.soccer,
                ),
              ),
            ],
          ),
          Row(
            mainAxisSize: MainAxisSize.max, //表示尽可能多的占用水平方向的空间
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              SizedBox(
                width: 58,
                child: Text(
                  result.hname,
                  maxLines: 1,
                  textAlign: TextAlign.center,
                ),
              ),
              Container(
                height: 34,
                decoration: BoxDecoration(
                  color: Color.fromRGBO(226, 179, 0, 1),
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                ),
                child: FractionallySizedBox(
                  alignment: Alignment.center,
                  child: Row(
                    children: <Widget>[
                      Text(" ", maxLines: 1),
                      FractionallySizedBox(
                        heightFactor: 0.8,
                        child: Lottie.asset(
                          'assets/lottie/live_icon2.json',
                          fit: BoxFit.fitHeight,
                          animate: true,
                          reverse: true,
                          // frameBuilder: (context, child, composition) {
                          //   return AnimatedOpacity(
                          //     child: child,
                          //     opacity: composition == null ? 0 : 1,
                          //     duration: const Duration(minutes: 1),
                          //     curve: Curves.linear,
                          //   );
                          // },
                        ),
                      ),
                      Text(" 直播中  ", maxLines: 1),
                    ],
                  ),
                ),
              ),
              SizedBox(
                width: 58,
                child: Text(
                  result.aname,
                  maxLines: 1,
                  textAlign: TextAlign.center,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
