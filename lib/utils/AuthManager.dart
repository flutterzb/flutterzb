import 'package:flutter_zb/utils/DataManager.dart';
import 'package:flutter_zb/utils/UserDefault.dart';

class AuthManager {
  static AuthManager instance = AuthManager._internal();
  factory AuthManager() => instance;

  AuthManager._internal() {}

  bool get isLogin {
    if (phoneNumber.isNotEmpty &&
        password.isNotEmpty &&
        DataManager().myInfo != null) {
      return true;
    } else {
      return false;
    }
  }

  String get phoneNumber {
    return UserDefault().phone;
  }

  String get password {
    return UserDefault().password;
  }

  setLoginData(String phone, String password) {
    UserDefault().phone = phone;
    UserDefault().password = password;
  }

  var accessToken;
  setToken(String accessToken) {
    accessToken = accessToken;
  }

  logout() {
    accessToken = null;
    UserDefault().phone = "";
    UserDefault().password = "";
    UserDefault().myInfo = "";
  }
}
