import 'dart:convert';
import 'package:flutter_zb/api/entitys/myInfo.dart';
import 'package:flutter_zb/utils/UserDefault.dart';

class DataManager {
  static DataManager instance = DataManager._internal();
  factory DataManager() => instance;

  DataManager._internal() {
    //SharedPreferences pref = await SharedPreferences.getInstance();
  }

  MyInfoResponseData get myInfo {
    //return MyInfoResponseData.fromJson(json.decode(UserDefault().myInfo));
    if (UserDefault().myInfo != null && UserDefault().myInfo != "") {
      return myInfoResponseDataFromJson(UserDefault().myInfo);
    } else {
      return null;
    }
  }

  set myInfo(MyInfoResponseData myInfo) {
    String myInfoJsonString = json.encode(myInfo);
    //print(myInfoJsonString);
    UserDefault().myInfo = myInfoJsonString;
  }
}
