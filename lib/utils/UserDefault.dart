import 'dart:convert';
import 'package:flutter_zb/api/entitys/myInfo.dart';
import 'package:flutter_zb/values/storage.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserDefault {
  static UserDefault instance = UserDefault._ctor();
  factory UserDefault() => instance;

  UserDefault._ctor();
  SharedPreferences _prefs;

  init() async {
    _prefs = await SharedPreferences.getInstance();
  }

  get phone {
    return _prefs.getString(STORAGE_PHONE) ?? "";
  }

  set phone(String value) {
    _prefs.setString(STORAGE_PHONE, value);
  }

  get password {
    return _prefs.getString(STORAGE_PASSWORD) ?? "";
  }

  set password(String value) {
    _prefs.setString(STORAGE_PASSWORD, value);
  }

  get myInfo {
    final myInfoJsonStr = _prefs.getString('myInfo');
    return myInfoJsonStr;
  }

  set myInfo(String value) {
    _prefs.setString('myInfo', value);
  }
}
